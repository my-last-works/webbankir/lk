import {getDescription, getIconClass} from '@/utils/mappings/loans'

describe('Mapping.Loans', () => {
  it('Should return Error without args', () => {
    // expect(getImgFilename()).to.throw(/Error thrown/)
    // expect(getImgFilename()).to.throw(Error, /Error thrown/)
    expect(getDescription()).to.equals(undefined)
    expect(getIconClass()).to.equals(undefined)
  })

  it('Should return Рассматривается for status 2', () => {
    expect(getDescription(2)).to.equals('Рассматривается')
  })
  it('Should return Займ выдан for status 6', () => {
    expect(getDescription(6, '5422 - 62** - **** - 4103')).to.equals('Займ выдан')
  })
  it('Should return Займ погашен for status 7', () => {
    expect(getDescription(7, '6759 - 64** - **** - 8453')).to.equals('Займ погашен')
  })
  it('Should return correct icon class', () => {
    expect(getIconClass(2)).to.equals('status-whaiting')
  })
  it('Should return correct icon class', () => {
    expect(getIconClass(6)).to.equals('status-sucess')
  })
  it('Should return correct icon class', () => {
    expect(getIconClass(101)).to.equals('status-deny')
  })
})
