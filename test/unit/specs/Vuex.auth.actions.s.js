import vuexAccounts from '@/store/modules/accounts'
// const actionsInjector = require('inject-loader!../../../src/store/modules/accounts')

// const commit = (mutation) => console.log('Here are mutation!!!', mutation)

// const actions = actionsInjector({
//   '@/api': () => console.log('Hello from fake api')
// })
const accounts = [
  {
    id: 100,
    name: 'Зряплатная',
    type: 0,
    number: '5454 - 54** - **** - 5454',
    isprimary: 1,
    activated: 1
  },
  {
    id: 105,
    name: 'Жены',
    type: 1,
    number: '9462 - 83** - **** - 7532',
    isprimary: 0,
    activated: 2,
    news: 999
  },
  {
    id: 125,
    name: 'Моя 2-ая',
    type: 1,
    number: '4129 - 32** - **** - 4567 ',
    isprimary: 0,
    activated: 1
  },
  {
    id: 151,
    name: 'ЙАденьга',
    type: 0,
    card_type: 'yamoney',
    number: 'Пункты выдачи (наличными)',
    isprimary: 0,
    activated: 0
  },
  {
    id: 187,
    type: 1,
    name: 'кЫви',
    card_type: 'qiwi',
    number: '7 926 *** ** 15',
    isprimary: 0
  }
]

const {ACCOUNTS_GET, ACCOUNTS_DELETE_ACCOUNT, ACCOUNTS_SET_PRIMARY, ACCOUNTS_SET_NAME} = vuexAccounts.actions

const testAction = (action, payload, state, expectedMutations, done) => {
  let count = 0

  // mock commit
  const commit = (type, payload) => {
    const mutation = expectedMutations[count]

    try {
      expect(mutation.type).to.equal(type)
      if (payload) {
        expect(mutation.payload).to.deep.equal(payload)
      }
    } catch (error) {
      done(error)
    }

    count++
    if (count >= expectedMutations.length) {
      done()
    }
  }

  // call the action with mocked store and arguments
  action({ commit, state }, payload)

  // check if no mutations should have been dispatched
  if (expectedMutations.length === 0) {
    expect(count).to.equal(0)
    done()
  }
}

describe('actions', () => {
  it('ACCOUNTS_GET', done => {
    testAction(ACCOUNTS_GET, null, {}, [
      { type: 'ACCOUNTS_SET_FIRSTLOAD' },
      { type: 'ACCOUNTS_SET_ACCOUNTS', payload: {accounts} }
    ], done)
  })
  it('ACCOUNTS_DELETE_ACCOUNT', done => {
    testAction(ACCOUNTS_DELETE_ACCOUNT, 100, {}, [
      { type: 'ACCOUNTS_SET_PENDING' },
      { type: 'ACCOUNTS_DELETE_ACCOUNT', payload: 100 },
      { type: 'ACCOUNTS_SET_OK' }
    ], done)
  })
  it('ACCOUNTS_SET_PRIMARY', done => {
    testAction(ACCOUNTS_SET_PRIMARY, 100, {}, [
      { type: 'ACCOUNTS_SET_PENDING' },
      { type: 'ACCOUNTS_SET_PRIMARY', payload: 100 },
      { type: 'ACCOUNTS_SET_OK' }
    ], done)
  })
  it('ACCOUNTS_SET_NAME', done => {
    testAction(ACCOUNTS_SET_NAME, {id: 100, name: 'New name'}, {}, [
      { type: 'ACCOUNTS_SET_NAME', payload: {id: 100, name: 'New name'} }
    ], done)
  })
})
