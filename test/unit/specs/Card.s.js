import Vue from 'vue'
import Hello from '@/pages/MyAccounts/Card'
import store from '@/store'

const card = {
  id: 100,
  name: 'Зряплатная',
  type: 0,
  number: '5454 - 54** - **** - 5454',
  isprimary: 1,
  activated: 1
}

Vue.use(store)
const Constructor = (card) => {
  const InnerConstructor = Vue.extend(Hello)
  return new InnerConstructor({
    store,
    propsData: {card}
  })
}

const vm = new Constructor(card).$mount()

describe('Card.vue', () => {
  it('number is correct', () => {
    expect(vm.$el.querySelector('.card-text').textContent)
      .to.equal(card.number)
  })
  it('Primary card is correct', () => {
    expect(vm.$el.querySelector('.prima-card').textContent)
      .to.equal('Займ будет выдан на эту карту')
    // expect(vm.$el.querySelector('.delete'))
    //   .to.not.exist
  })
  it('Additional card is correct', () => {
    const vm = new Constructor({...card, isprimary: false}).$mount()
    expect(vm.$el.querySelector('.prima-card').textContent)
      .to.equal('Сделать основной')
    // expect(vm.$el.querySelector('.delete'))
    //   .to.exist
  })
})
