import {getImgFilename} from '@/utils/mappings/accounts'

describe('Mapping.Accounts', () => {
  it('Should return Error without args', () => {
    // expect(getImgFilename()).to.throw(/Error thrown/)
    // expect(getImgFilename()).to.throw(Error, /Error thrown/)
    expect(getImgFilename()).to.equals(undefined)
  })
  it('Should return visa.png for 4916 - 62** - **** - 2715', () => {
    expect(getImgFilename(1, '4916 - 62** - **** - 2715')).to.equals('visa.png')
  })
  it('Should return master-card.png for 5422 - 62** - **** - 4103', () => {
    expect(getImgFilename(1, '5422 - 62** - **** - 4103')).to.equals('master-card.png')
  })
  it('Should return maestro.png for 6759 - 64** - **** - 8453', () => {
    expect(getImgFilename(1, '6759 - 64** - **** - 8453')).to.equals('maestro.png')
  })
  it('Should return old-contact.png for type 2', () => {
    expect(getImgFilename(2)).to.equals('contact.png')
  })
  it('Should return yandex.png for type 3', () => {
    expect(getImgFilename(3)).to.equals('yandex.png')
  })
  it('Should return qiwi.png for type 4', () => {
    expect(getImgFilename(4)).to.equals('qiwi.png')
  })
})
