const requiredFields = {
  mobilePhone: {},
  email: {},
  lastName: {},
  firstName: {},
  middleName: {},
  smsCode: {},
  controlQuestionType: {
    1: {
      controlAnswer: {}
    },
    2: {
      controlAnswer: {}
    },
    3: {
      controlQuestion: {},
      controlAnswer: {}
    }
  },
  gender: {},
  bDay: {},
  bPlace: {},
  passport: {},
  passportDivisionCode: {},
  passportDateOfIssue: {},
  passportIssuedBy: {},
  snils: {},
  additionalPhone: {},
  additionalPhoneOwner: {},
  typeOfEmployment: {
    1: {
      workScope: {},
      workType: {},
      workName: {},
      workNumberOfEmployees: {},
      workPhone: {},
      workPeriod: {},
      workPosition: {}
    },
    2: {
      workINN: {}
    },
    3: {
      workINN: {},
      workFullName: {}
    },
    4: {},
    5: {},
    6: {},
    7: {},
    8: {}
  },
  workSalary: {},
  workAddress: {
    alwaysRequired: {
      value: {},
      postalCode: {},
      fiasId: {},
      cityFiasId: {},
      houseFiasId: {},
      regionFiasId: {},
      streetFiasId: {},
      house: {}
    }
  },
  numberOfChildren: {},
  maritalStatus: {},
  educationType: {},
  hasPreviousConviction: {},
  publicity: {
    officialState: {
      true: {
        officialOwnerIsMe: {},
        officialName: {}
      }
    },
    officialRussiaState: {
      true: {
        officialRussiaOwnerIsMe: {},
        officialRussiaName: {}
      }
    },
    internationalOrganizationState: {
      true: {
        internationalOrganizationOwnerIsMe: {},
        internationalOrganizationName: {}
      }
    }
  },
  benefitsOfAnotherPerson: {
    true: {
      beneficial: {
        alwaysRequired: {
          lastName: {},
          firstName: {},
          middleName: {},
          passport: {},
          passportDivisionCode: {},
          passportDateOfIssue: {}
        }
      }
    }
  }
}
