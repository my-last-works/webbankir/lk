import Vue from 'vue'
import Hello from '@/components/Hello'
import store from '@/store'

describe('Hello.vue', () => {
  it('should render correct contents', () => {
    Vue.use(store)
    const Constructor = Vue.extend(Hello)
    const vm = new Constructor({
      store,
      propsData: {value: 'testtext'}
    }).$mount()
    // expect(vm.$el.querySelector('.hello input').value)
    //   .to.equal('testtext')
    expect(vm.$el.querySelector('.hello input')).to.exist
  })
})
