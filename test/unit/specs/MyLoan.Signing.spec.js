import Vue from 'vue'
import Vuex from 'vuex'
import { shallow, mount } from '@vue/test-utils'
import MyLoan from '@/pages/MyLoan/ActiveLoan/index'
import store from '@/store'
const loansInjector = require('inject-loader!../../../src/store/modules/loans')

const isHidden = (wrapper) => wrapper.attributes().style.includes('display: none;')

describe('Signing.vue', () => {
  before(() => {
  })
  beforeEach(() => {
  })

  const inject = (data) => loansInjector({
    '../../utils/backend-api': {
      getData () {
        return new Promise((resolve, reject) => {
          resolve({data})
        })
      }
    }
  }).default

  const loans = inject([
    {
      'id': 341,
      'number': 3,
      'active': true,
      'date_from': '05.11.2017',
      'date_to': '20.11.2017',
      'prolong_to': '20.11.2017',
      'period': 15,
      'summ': 300,
      'penalty': 0,
      'percent_summ': 30.05,
      'status': 6,
      'getting_way': 1,
      'amount': 1000,
      'total_amount': 1300
    }
  ])

  Vue.use(Vuex)

  const mockStore = new Vuex.Store({
    modules: {
      loans
    }
  })

  const wrapper = mount(MyLoan, {
    store: mockStore
  })

  it('should render correct summ', async () => {
    await Vue.nextTick()
    const sum = wrapper.find('.loan-sum span')
    expect(sum.text()).to.equal('300')
  }).timeout(1000)

  it('Detail button works', async () => {
    const loanInfoBlock = wrapper.find('.loan-info')
    expect(isHidden(loanInfoBlock)).to.equal(true) // Не показана
    wrapper.find('[data-test=details]').trigger('click')
    expect(isHidden(loanInfoBlock)).to.equal(false) // Показана после клика
    // wrapper.vm.$destroy()
  }).timeout(1000)

  it('No sign from for status !== 3', async () => {
    await Vue.nextTick()
    expect(wrapper.find('.loan-signing').exists()).to.equal(false)
  })

  it('payment-method-choose don`t show before pay button click', async () => {
    expect(wrapper.find('.payment-method-choose').exists()).to.equal(false)
  })

  it('payment-method-choose show after pay button click', async () => {
    wrapper.find('[data-test=pay]').trigger('click')
    expect(wrapper.find('.payment-method-choose').exists()).to.equal(true)
  })

  // ********************************* Signing block show ****************************** //

  it('should show sign block on status 3', async () => {
    const loans = inject([
      {
        'id': 441,
        'number': 3,
        'active': true,
        'date_from': '05.11.2017',
        'date_to': '20.11.2017',
        'prolong_to': '20.11.2017',
        'period': 15,
        'summ': 300,
        'penalty': 0,
        'percent_summ': 30.05,
        'status': 3,
        'getting_way': 1,
        'amount': 1000,
        'total_amount': 1300
      }
    ])

    const mockStore = new Vuex.Store({
      modules: {
        loans
      }
    })

    const wrapper = mount(MyLoan, {
      store: mockStore
    })

    await Vue.nextTick()
    expect(wrapper.find('.loan-signing').exists()).to.equal(true)
  })
})
