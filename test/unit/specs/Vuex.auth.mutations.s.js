// mutations.spec.js
import find from 'lodash/find'
import vuexAccounts from '@/store/modules/accounts'

const { ACCOUNTS_SET_OK, ACCOUNTS_SET_ACCOUNTS, ACCOUNTS_SET_PRIMARY, ACCOUNTS_SET_NAME, ACCOUNTS_DELETE_ACCOUNT } = vuexAccounts.mutations

const state = {
  accounts: [
  ],
  status: 'start'
}

const response = {
  accounts: [
    {
      id: 100,
      name: 'Зряплатная',
      type: 0,
      number: '5454 - 54** - **** - 5454',
      isprimary: 1,
      activated: 1
    },
    {
      id: 105,
      name: 'Жены',
      type: 1,
      number: '9462 - 83** - **** - 7532',
      isprimary: 0,
      activated: 2,
      news: 999
    },
    {
      id: 125,
      name: 'Моя 2-ая',
      type: 2,
      number: '4129 - 32** - **** - 4567 ',
      isprimary: 0,
      activated: 1
    }
  ]
}

describe('mutations', () => {
  it('ACCOUNTS_SET_OK', () => {
    ACCOUNTS_SET_OK(state)
    expect(state.status).to.equal('ok')
  })
  it('ACCOUNTS_SET_ACCOUNTS', () => {
    ACCOUNTS_SET_ACCOUNTS(state, response)
    expect(state.status).to.equal('ok')
    expect(state.accounts).to.deep.equal(response.accounts)
  })
  it('ACCOUNTS_SET_PRIMARY', () => {
    ACCOUNTS_SET_PRIMARY(state, 105)
    expect(state.status).to.equal('ok')
    expect(find(state.accounts, {id: 105}).isprimary).to.equal(1)
    expect(find(state.accounts, {id: 100}).isprimary).to.equal(0)
  })
  it('ACCOUNTS_SET_NAME', () => {
    ACCOUNTS_SET_NAME(state, {id: 125, name: 'New name'})
    expect(state.status).to.equal('ok')
    expect(find(state.accounts, {id: 125}).name).to.equal('New name')
  })
  it('ACCOUNTS_DELETE_ACCOUNT', () => {
    ACCOUNTS_DELETE_ACCOUNT(state, 125)
    expect(state.status).to.equal('ok')
    expect(find(state.accounts, {id: 125})).to.equal(undefined)
  })
})
