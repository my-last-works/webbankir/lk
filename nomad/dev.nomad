job "frontend-lk" {
  datacenters = [
    "dc1"
  ]
  type = "service"

  group "frontend-lk" {
    count = 1


    task "frontend-lk" {
      driver = "docker"

      config {
        image = "[[ env "CI_REGISTRY_IMAGE"]]:build"
        force_pull = true

        auth {
          server_address = "[[ env "CI_REGISTRY" ]]"
          username = "[[ env "NOMAD_GITLAB_USER" ]]"
          password = "[[ env "NOMAD_GITLAB_PASSWORD" ]]"
        }

        port_map {
          http = 80
        }

        logging {
          type = "gelf"
          config {
            gelf-address = "udp://${attr.unique.network.ip-address}:12201"
            tag          = "${NOMAD_TASK_NAME}"
          }
        }
      }

      resources {
        cpu = 300
        memory = 128
        network {
          port "http" {
          }
        }
      }

      service {
        name = "${NOMAD_TASK_NAME}"
        port = "http"
        tags = [
          "traefik.enable=true",
          "traefik-front.enable=true",
          "traefik-front.frontend.rule=Host:dev.webbankir.com;PathPrefix:/lk"
        ]
      }
    }
  }
}
