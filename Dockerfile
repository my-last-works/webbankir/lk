FROM nginx:alpine
COPY --chown=nginx:nginx ./dist /usr/share/nginx/html/lk
COPY ./nginx /etc/nginx
