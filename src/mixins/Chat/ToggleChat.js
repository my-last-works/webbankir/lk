export default {
  methods: {
    toggleChat () {
      if (document.querySelector('.sender-connect-button') === null) {
        console.log('По каким то причинам со стороны виджита - виджет не подгрузился')
        return false
      }
      if (document.querySelector('.sender-connect-button').classList.contains('sender-connect-hidden')) {
        document.querySelector('.sender-connect-button-close').classList.add('sender-connect-hidden')
        document.querySelector('.sender-connect-list-wrapper').classList.add('sender-connect-hidden')
        document.querySelector('.sender-connect-button').classList.remove('sender-connect-hidden')
      } else {
        document.querySelector('.sender-connect-button-close').classList.remove('sender-connect-hidden')
        document.querySelector('.sender-connect-list-wrapper').classList.remove('sender-connect-hidden')
        document.querySelector('.sender-connect-button').classList.add('sender-connect-hidden')
      }
    }
  }
}
