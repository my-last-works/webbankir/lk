import forEach from 'lodash/forEach'

export default {
  computed: {
    vuexErrors () {
      return this.$store.getters.getErrors
    },
    hasErrors () {
      return this.vErrors.all().length > 0
    }
  },
  watch: {
    vuexErrors (errors) {
      this.vErrors.clear()
      forEach(this.fields, (noneed, field) => {
        forEach(errors, (error) => {
          if (field === error.field) {
            this.vErrors.add(error.field, error.message)
          }
        })
      })
    }
  }
}
