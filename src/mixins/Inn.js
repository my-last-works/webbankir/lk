export default {
  methods: {
    sendInnCheck () {
      const [passportSerial, passportNumber] = this.$user.passport.split(' ')
      const [day, month, year] = this.$user.bDay.split('.')
      const bDay = `${year}-${month}-${day}T00:00:00.000Z`
      return this.$backend.postData('tkb/inn', {
        F: this.$user.lastName,
        I: this.$user.firstName,
        O: this.$user.middleName,
        BD: bDay,
        PS: passportSerial,
        PN: passportNumber
      }, 'https://new.webbankir.com/api/').then((response) => {
        if(response.data.code) {
          this.$user.snilsOrInn = response.data.inn;
          this.autoINN = true;
        }
      }).catch((e) => {
        console.warn(e)
      })
    }
  }
}