export default {
  computed: {
    user: {
      get: function () {
        return this.$store.getters.getUser
      },
      set: function () {

      }
    },
    loading: {
      get: function () {
        return this.$store.getters.getLoading
      },
      set: function () {

      }
    }
  }
}
