import smsErrors from '@/utils/error_mapper/smsErrors';
import ToggleChat from '@/mixins/Chat/ToggleChat';
export default {
	mixins: [
		ToggleChat,
	],
	methods: {
		async sendDataOfUser() {
			/*если вызван не из компонента "первая страница"*/
			if(!this.isResendCodeModal) {
				let access = this.$refs.form;
				// валидация имени пользоваеля
				await access.$refs.InitialsOfUser.$validator.validateAll()
					.then(val => this.$user.validate.step1.firstLastMiddleName = val)
				await access.$refs.ContactMobilePhone.$validator.validateAll()
				// валидация мобильного телефона
					.then(val => this.$user.validate.step1.contactMobilePhone = val)
				// валидация почты
				await access.$refs.ContactEmail.$validator.validateAll()
					.then(val => this.$user.validate.step1.contactEmail = val)

				if (!this.$validate.step1) {
					this.allFields = true
					this.$nextTick(() => {
						focus();
					});
					this.$toastr.error('Не заполнены обязательные поля', 'Ошибка')
					return;
				}
			}


			let error = []
			if (!this.compliance && this.confirmation) {
				error.push('Вы должны дать согласие на обработку персональных данных')
				this.notAgree = true
				// this.$refs.agreeLabel.scrollIntoView()
			}
			if (this.confirmation && (!this.$user.smsCode || this.$user.smsCode.toString().length !== 4)) {
				this.allFields = true
				this.$refs.smsForm.$refs.smsCodeInput.focus()
				this.$refs.smsForm.$refs.smsCodeInput.classList.add('error')
				error.push('Не введен код подтверждения')
			}
			if (error.length) {
				this.$toastr.error(error.join('<br>'), error.length > 1 ? 'Ошибки' : 'Ошибка')
				return
			}

			/*если вызван не из компонента "первая страница"*/
			if(!this.isResendCodeModal) {
				this.BUTTON_LOADING(true);
				this.allFields = false
			}


			await this.$store.dispatch('CREATE_USER', [
				'lastName',
				'firstName',
				'middleName',
				'mobilePhone',
				'email',
				'smsCode'
			])
				.then(() => {
					/*если смс-код введен, то отправки смс-нет. соответсвенно ни уведомлений, ни счетчиков - не нужно.*/
					if (!this.$user.smsCode) {
						this.$toastr.success('СМС-код подтверждения отправлен на ваш номер телефона', 'Успешно')
						// обновим счетчик отправленных смс
						this.$user.smsSend.total++;
						this.$user.smsSend.phone++;
					}

					/*если вызван не из компонента "первая страница"*/
					if(!this.isResendCodeModal) {
						/*показываем форму для ввода кода смс*/
						this.accessForm();
						window.wbEvent.send('USER_REGISTER_ATTEMPT');
						this.BUTTON_LOADING(false);
						const smsFormRef = this.$refs.smsForm;
						if(smsFormRef) {
							this.$nextTick(() => {
								this.$helpers.scroll(smsFormRef?.$el);
								const smsCodeInputRef = smsFormRef.$refs.smsCodeInput;
								if(smsCodeInputRef) {
									smsCodeInputRef.focus();
								}
							});
						}

					}

					this.isSuccessResendSmsCode = true;
					this.$modal.hide('resend-code');
				})
				.catch((e) => {
					/*передать родительскому компоненту ошибку*/
					this.$emit('errorResponse', e);
					if (Array.isArray(e.errors)) {
						switch(this.$getErrorCode(e)) {
							case 'spamProtectionRestriction':
								this.$toastr.error('Превышен лимит отправки сообщений на указанный номер телефона.', 'Ошибка', {
									buttons: [{
										title: 'Обратиться в техподдержку',
										type: 'link',
										click: () => {
											this.toggleChat();
										}
									}]
									
								})
								break;
							case 'mobilePhoneNotUnique':
								this.$modal.show('modal-lk-exists');
								break;
							case 'mobilePhoneDoNotHaveActiveSmsCode':
								this.$user.smsCode = '';
								this.confirmation = true;
								this.hasBlock = true;
								this.againSms();
								break;
							default:
								this.$error(e, smsErrors, 'Ошибка при обработки данных');
								break;
							
						}
						
					// 	e.errors.forEach((code) => {
					// 		this.$errorField(code);
					// 	});
					//
					// 	if (e.errors[0].code === 'spamProtectionRestriction') {
					// 		this.showVuexError = 'none'
					// 		this.$nextTick(() => {
					// 			this.$store.commit('CALL_WARNING_WINDOW', this.$store.state.errorMessagesWithChatHelp.smsLimit)
					// 			this.$toastr.error('Превышен лимит отправки сообщений на указанный номер телефона.', 'Ошибка')
					// 		})
					// 		this.changePhoneBlocked = true
					// 	} else if (e.errors[0].code === 'mobilePhoneNotUnique') {
					// 		this.$modal.show('modal-lk-exists')
					// // 	} else if (e.errors[0].code === 'smsCodeInvalid') {
					// // 		this.$toastr.error('Неверно введен код')
					// // 	} else if (e.errors[0].code === 'Sms code verification failed: smsCodeInvalid') {
					// // 		this.$toastr.error('Неверно введен код')
					// // 	} else if (e.errors[0].code === 'mobilePhoneBlocked') {
					// // 		this.$toastr.error('Вы сделали слишком много попыток ввода. Телефон временно заблокирован.', 'Ошибка')
					// 	} else if (e.errors[0].code === 'mobilePhoneDoNotHaveActiveSmsCode') {
					// 		this.$user.smsCode = '';
					// 		this.confirmation = true;
					// 		this.hasBlock = true;
					// 		this.againSms();
					// 	}else {
					// 		this.showVuexError = 'block'
					// 	}
						
						this.BUTTON_LOADING(false);
					} else {
						this.BUTTON_LOADING(false);
						this.$toastr.warning('Произошла ошибка приложения: ' + e.message, 'Ошибка');
						throw e
					}

				});

			this.BUTTON_LOADING(false);
		},
		againSms() {
			return this.$store.dispatch('CREATE_USER', [
				'lastName',
				'firstName',
				'middleName',
				'mobilePhone',
				'email'
			]).then(() => {
				this.$user.smsSend.total++;
				this.$user.smsSend.phone++;
				this.$toastr.success('СМС-код подтверждения отправлен на ваш номер телефона', 'Успешно')
			})
				.catch((e) => {
					if (e.errors[0].code === 'spamProtectionRestriction') {
						this.$toastr.error('Превышен лимит отправки сообщений на указанный номер телефона.')
						this.$store.commit('CALL_WARNING_WINDOW', this.$store.state.errorMessagesWithChatHelp.smsLimit)
					} else {
						this.$toastr.error('Ошибка отправки сообщения с кодом. Повторите попытку через некоторое время.')
					}
				})
				.finally(() => {
					const smsFormRef = this.$refs.smsForm;
					if(!smsFormRef) {
						return;
					}
					smsFormRef.$refs.smsCodeInput.focus();
				})
			;
		},
	},
}
