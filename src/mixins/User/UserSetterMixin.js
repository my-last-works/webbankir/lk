import {SET_USER} from '@/store/mutation-types'

export default {
  methods: {
    setValue: function (key) {
      this.errorText[key] = ''
      this.vErrors.remove(key)
      this.$store.commit(SET_USER, this.user)
    },
    updateUser: function (user) {
      this.$store.commit(SET_USER, user)
    }
  }
}
