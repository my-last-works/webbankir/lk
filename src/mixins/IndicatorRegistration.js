export default {
  computed: {
    indicatorRegistration: {
      get () {
        return this.$store.getters.getIndicatorRegistration
      }
    },
    validated_forms: {
      get () {
        return this.$store.getters.getValidatedForms
      }
    },
    button_continue: {
      get () {
        return this.$store.getters.getButtonContinue
      }
    },
    registrationStep: {
      get () {
        return this.$store.getters.getRegistrationStep
      }
    }
  }
}
