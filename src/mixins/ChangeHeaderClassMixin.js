import {SET_HEADER_CLASS, CLEAR_HEADER_CLASS} from '@/store/mutation-types'

export default {
  beforeMount: function () {
    this.$store.commit(SET_HEADER_CLASS, this.headerClassName)
  },
  beforeDestroy: function () {
    this.$store.commit(CLEAR_HEADER_CLASS)
  }
}
