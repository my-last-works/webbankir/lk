export default {
  computed: {
    isActivated () {
      return this.$pdl.id && ![6, 7, 100, 101, 102].includes(this.$pdl.status);
    },
    unfiled: {
      get () {
		  if (this.$user.blankFields.length && (this.$pdl.status === 6)) { // проверяем, если незаполненные поля, которые присылает бэк
			  if (this.$user.blankFields.some((k) => k === 'workINN') && this.$user.blankFields.length === 1) {
				  return false // если с бэка пришло только одно поле workINN - то мы не показываем блок
			  } else {
				  return true // в остальных случаях блок виден, если пришел массив данных
			  }
		  } else {
			  return false // не показываем блок, если массив данных пуст
		  }
      }
    }
  }
}
