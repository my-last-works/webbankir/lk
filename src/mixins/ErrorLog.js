export default {
  methods: {
    $errorField(code, error_type = 'invalid_field_value' ) {
      console.log('ErrorField code:', code);
      const ErrorMapper = require('../utils/error_mapper/user').default;

      const el = document.querySelector(`[data-code*='${code}']`);
      console.log(el);
      const report = {};
      if (el) {
        const name = el.getAttribute("name");
        if (name) {
          const message = ErrorMapper[code] ? ErrorMapper[code].message : '';
          if (!message) {
            report.missingMessage = true;
          }
          this.vErrors.add(name, message);
          report.field = name;
          report.inputValue = el.value;
        } else {
          report.missingName = true;
        }
      } else {
        report.missingCode = true;
      }
      report.code = code;

      const vModel = ErrorMapper[code];
      report.inMap = (vModel && vModel.vModel);
      if (vModel && vModel.vModel) {
        if (vModel.vModel.indexOf('.') > -1) {
          let [a, b] = vModel.vModel.split('.');
          report.storage = typeof this.$user[a] === "object" ? this.$user[a][b] : 'Value not found';
        } else {
          report.storage = this.$user[vModel.vModel];
        }
      }
      this.$backend.log({
        error_type,
        report,
        user_id: this.$user.id,
        user_login: this.$user.login,
        user_phone: this.$user.mobilePhone ? this.$user.mobilePhone.replace(/[^\d]/g, '') : '', //can be null
        user_name: `${this.$user.lastName} ${this.$user.firstName} ${this.$user.middleName}`
      })
    },
  }
}