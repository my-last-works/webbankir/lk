export default {
  data () {
    return {
      recountTimeout: null
    }
  },
  methods: {
    initUpdate (where) {
      console.log('init update ', where);

      const promises = [];
      Object.keys(where).forEach((key) => {
        switch(key) {
          case 'pdl': promises.push(this.$store.dispatch('GET_LOAN_PDL'));break;
          case 'calc': promises.push(this.$store.dispatch('GET_PROLONGATION_DATA'));break;
        }
      });

      const old_1c = this.$pdl.webserviceLastUpdate;
      Promise.all(promises).then(() => {
        if (old_1c === this.$pdl.webserviceLastUpdate) {
          setTimeout(() => {
            this.initUpdate(where);
          }, 10000);
        }
      }).catch((e) => {
        setTimeout(() => {
          this.initUpdate(where);
        }, 10000);
      })

    },

    getUserLocation () {
      this.$backend.log({
          action: 'getCurrentPosition',
          state: 'request',
          user_id: this.$user.id
      }, false);
      
      if(window.navigator) {
        window.navigator.geolocation.getCurrentPosition((data) => {
          this.$store.commit('SET_USER_LOCATION', data.coords);
          
          this.$backend.log({
              action: 'getCurrentPosition',
              state: 'ok',
              data: {
				  latitude: data.coords.latitude,
				  longitude: data.coords.longitude,
              },
              user_id: this.$user.id,
          }, false);
          
        }, (e) => {
          this.$backend.log({
              action: 'getCurrentPosition',
              state: 'error',
              error: e.code,
              user_id: this.$user.id
          }, false);
        })
      } else {
		  this.$backend.log({
			  action: 'getCurrentPosition',
			  state: 'error',
              error: 'not supported device',
			  user_id: this.$user.id
		  }, false);
      }
    },
  }
}
