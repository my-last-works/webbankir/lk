export default {
  methods: {
    changeDataOfUser ({target}) { // для всех input text
      // console.log('bla', target.id, target.value)
      this.$store.commit('changeDataOfUserByField', {key: target.id, text: target.value})
    },
    changeGenderOfUser ({target}) { // исключение для пола
      this.$store.commit('changeDataOfUserByField', {key: 'gender', text: target.id})
    },
    changeHasPreviousConvictionOfUser ({target}) { // исключение для осужденных
      let flag = null
      if (target.value === 'false') {
        flag = false
      }
      if (target.value === 'true') {
        flag = true
      }
      this.$store.commit('changeDataOfUserByField', {key: 'hasPreviousConviction', text: flag})
    },
    changeChildrenOfUser (target) { // исключение для детей
      let key = Object.keys(target).join()
      let text = Object.values(target).join()
      this.$store.commit('changeDataOfUserByField', {key: key, text: text})
    },
    updatedDataOfUser (user) { // для всех типов мультиселектов
      // console.log('blaster', user)
      this.$store.commit('changeDataOfUserByAllFields', user)
    },
    changeDataPublicityOfUser ({target}) { // персонально для блока публисити
      this.$store.commit('changeDataOfUserByPublicity', {key: target.id, text: target.value})
    },
    changeDataOfUserByBeneficial ({target}) { // персонально для блока бенефициала
      this.$store.commit('changeDataOfUserByBeneficial', {key: target.id, text: target.value})
    }
  }
}
