export default {
  methods: {
    validateProperty (propertyName, event) {
      if (event.target.value === '') {
        return false
      }
      let res = {}
      res[propertyName] = event.target.value
      this.$http.post('suggest/validator', res)
        .then(() => {
          event.target.className = ''
          this.errors = {}
          this.$store.dispatch('setUserValuesAction', this.user)
        })
        .catch(error => {
          event.target.className = 'error'
          this.errors = {...this.errors, ...JSON.parse(error.bodyText).errors.list}
        })
    }
  }
}
