export default {
  data () {
    return {
      callBack: true,
      backTime: 60,
      ending: ''
    }
  },
  methods: {
    tiktak () {
      if (!this.backTime) {
        this.callBack = false
        this.backTime = 60
        return false
      }
      this.callBack = true
      setTimeout(() => {
        switch (this.backTime && Number(this.backTime.toString().slice(-1))) {
          case 5:
            this.ending = 'ы'
            break
          case 4:
            this.ending = 'ы'
            break
          case 3:
            this.ending = 'ы'
            break
          case 2:
            this.ending = 'у'
            break
          case 1:
            this.ending = ''
            break
          case 0:
            this.ending = ''
            break
        }
        this.backTime--
        this.tiktak()
      }, 1000)
    }
  }
}
