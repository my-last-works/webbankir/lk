/**
 * @todo move to Services.js
 */
export default {
    data () {
        return {
            ready: false,
            _additionalServices: [],
            selectedServices: {},
        }
    },

    computed: {
        additionalServices() {
            this.selectedServices = {};
            console.log('_additionalServices', this._additionalServices);
            const result =  this._additionalServices.filter((row) => {
                const r = row.credit_cards_only ? this.selectedPayMethod?.type === 1 : true;
                if (r) {
                    this.selectedServices[row.id] = row.state;
                }

                return r;
            });

            return result;
        },
    },


    created () {
        this.AJAX_LOADING(true);
        this.getAdditionalServices()
            .finally(() => {
                this.AJAX_LOADING(false);
                this.ready = true;
            });
    },

    methods: {
        setAdditionalService(service) {
            this.additionalServices.forEach((row) => {
                if (row.id === service.id) {
                    row.state = service.state;
                }
            });
        },
        getAdditionalServices() {
            return this.$backend.api.services.all()
                .then(({ data }) => {
                    console.log('data service', data);
                    this._additionalServices = data.data.filter((row) => {
                        return  [4, 5, 7].includes(row.location_int) && row.active_for_user && row.active;
                    });
                    console.log('_additionalServices', this._additionalServices);
                    return Promise.resolve();
                })
                .catch((e) => {
                    console.warn('getAdditionalServices', e);
                    this._additionalServices = [];
                });
        },

        async getSelectedAdditionalServices () {
            if (!this.hasAdditionalService) {
              return [];
            }
            const services_ids = [];
            this.services.forEach((/**iAdditionalService**/service) => {
                if (service.selected) services_ids.push(service.id);
            });

            console.log('services_ids', services_ids);

            return services_ids;
        }
    }
}
