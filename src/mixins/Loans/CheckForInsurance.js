export default {
  computed: {
      insuranceIsAllowed () {
          return this.$store.getters.getCalculation.insurance && this.$store.getters.getCalculation.insurance.status && Array.isArray(this.$calculate.InsuranceTable);
      },
      insuranceParameters () {
          let sum = 0;
          let price = 0;

          const result =  this.$calculate.InsuranceTable.find(range => {
              if (this.loan.sum >= range.Min && this.loan.sum <= range.Max) {
                  sum = range.Max;
                  return range.Value.find((period) => {
                      if (this.loan.days >= period.Min && this.loan.days <= period.Max) {
                          price = period.Value;
                          return period.Value;
                      }
                  })
              }
          });

          return result ? {
              sum,
              price,
          } : {
              sum: 0,
              price: 0
          };

      }
  },
  methods: {
    async checkForInsurance () {
      if (!await this.$refs.snils.$validator.validateAll()) {
        this.$toastr.error('Заполните поле СНИЛС или ИНН', 'Ошибка')
        this.$refs.controlQuestion.$validator.validateAll()
        this.$helpers.focus()
        return
      }
      if (!await this.$refs.controlQuestion.$validator.validateAll()) {
        this.$toastr.error('Заполните поле контрольный ответ', 'Ошибка')
        this.$helpers.focus()
        return
      }

      if(this.$config.serviceModalEnabled) {
        await this.$nextTick(() => {
          this.modalAdditionalServices = true;
          return Promise.resolve()
        })

        if ((!this.isInsured && this.insuranceIsAllowed) || (!this.isLegal && this.legal.status)) {
          this.$modal.show('modal-additional-services', {
            insuredPrice: this.insuranceParameters.price,
            insuredAmount: this.insuranceParameters.sum,
            legalPrice: this.legal.sum,
            legalPeriod: this.legal.period
          })
          // исключаем дублирование евентов, если окно вызвано несколько раз
          if (!this.$refs.modalAdditionalServices._events.answer) {
            this.$refs.modalAdditionalServices.$on('answer', (answer) => {
              this.isInsured = answer.insured
              this.isLegal = answer.legal
              this.$modal.hide('modal-additional-services')
              this.createLoan()

              if (this.isInsured || this.isLegal) {
                window.wbEvent.send('CLIENT_SIGN_CONTRACT')
              }
            })
          }
        } else {
          window.wbEvent.send('CLIENT_SIGN_CONTRACT')
          this.createLoan()
        }
      } else {
        this.createLoan();
      }
      // return
    }
  }
}
