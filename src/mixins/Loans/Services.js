export default {
    computed: {
        additionalServicesPrice () {
            /**
             * Пока не выбран аккаунт для выбора, услуги не показываются. Соотвественно калькуляция не должна участвовать.
             */
            if(!this.showFinal && this.$route.name === 'registration') return 0;

			/**
             * если пользователю недоступны доп.услуги для показа и/или заказа
			 */
			if(!this.hasAdditionalService) return 0;
	
			
	
			let sum = 0;
            this.services.filter((/**iAdditionalService**/service) => {
                return service.selected;
            }).forEach((/**iAdditionalService**/service) => {
				sum += this.calculateServicePrice(service);
            });
            

            let add_price = 0;
            if(sum) {
                for(let i = 0; i < this.loan.days; i++) {
                    add_price += sum * this.$calculate.period_percents[i] / 100;
                }
                sum += add_price;
            }

            return sum;
        },
        /**
         * Возвращает массив всех доступных клиенту услуги.
         *
         * @returns [iAdditionalService]
         */
        services () {
        	console.log('Selected method type', this.selectedPayMethod?.type);
        	const list =  this.$store.getters.ADDITIONAL_SERVICES_GET_LIST.filter((/**iAdditionalService**/service) => {
                switch(this.$route.name) {
                    case 'registration':
                        return [4].includes(service.location_int)
                            && service.active
                            && service.active_for_user;
                    case 'my-loan':
                        return [4, 5, 7].includes(service.location_int)
                            && service.active
                            && service.active_for_user;
                    default:
                        console.warn('Не найдено списка дополнительных услуг для $route.name ' + this.$route.name);
                        return false;
                }
            }).filter((/**iAdditionalService**/service) => {
                if (!this.selectedPayMethod) {
                    return true;
                }

                if (service.credit_cards_only) {
                    return this.selectedPayMethod.type === 1;
                }
                return true;
            });
            
        	console.log('Service::services.filtered', list);
            return list;
        },
	
	
		hasAdditionalService () {
			/**
			 * Новый метод по заданию WB-6677, в котором проверяем, заканчивает $user.id на 0 или 9
			 * в случае, если ДА - то не показываем блок доп. услуг
			 * в случае, если нет - показываем как всегда
			 **/
			if(!this.$config.ab.serviceBlock.enable) {
				return true;
			}
			let exception = this.$config.ab.serviceBlock.ids.toString(); // концовки исключений
			return ![...exception].includes(this.$user.id.toString().slice(-1))
		},
		
		__AB_TEST_PAGE_ID () {
        	return this.$route.name === 'my-loan' ? 'FirstLoan' : 'SecondLoan';
		},
    },
	
	mounted () {
		if (this.$config.ab.insure) {
			console.group('ABTest for insure')
			const insure = this.services.find(s => s.id === 6);
			
			if(insure) {
				console.log('insure found', insure);
				this.$backend.ABTestLog({
					state: Number(insure.selected),
					user_id: this.$user.id,
					action: 'InsureCheckboxChange',
					ABTest: 1,
					page: this.__AB_TEST_PAGE_ID
				})
			} else {
				console.log('insure not found');
			}
			console.groupEnd();
		}
	},
	
    methods: {
		/**
         * Возвращает текст для подробного описания услуги.
         *
		 * @param service
		 * @returns {string}
		 */
		blockDescription (/**iAdditionalService**/service)  {
			let	sum = 0;
			
			switch(service.price_graph.type) {
				case 'GRAPH':
					sum =  service.price_graph.graph.find((/**iAdditionalServicePriceGraph**/graph) => {
						return graph.sum >= this.loan.sum
					}).sum;
					break;
                default:
                    sum = service.price;
                    break;
			}
		
			return service.description.replace(/%AMOUNT%/gi, sum);
		},
        /**
         * Калькуляция стоимости услуги в зависимости от параметров графика услуги.
         *
         * @param service
         * @returns {number}
         */
        calculateServicePrice(/**iAdditionalService**/service) {
            switch(service.price_graph.type) {
                case 'PERCENT':
                    const percent =  service.price_graph.graph.find((/**iAdditionalServicePriceGraph**/graph) => {
                        return graph.sum <= this.loan.sum
                    }).value;

                    return this.loan.sum / 100 * percent;
                case 'FIXED':
                    return service.price;
					
                case 'GRAPH':
					return service.price_graph.graph.find((/**iAdditionalServicePriceGraph**/graph) => {
					    return graph.sum >= this.loan.sum && graph.period >= this.loan.days
                    })?.value;
            }
        },
        /**
         * Тугл статуса iAdditionalService.expand
         * @param {number} id
         * @return void
         */
        setServiceExpand (id) {
            this.$store.commit('ADDITIONAL_SERVICE_TOGGLE_EXPAND', id);
        },

        /**
         * Тугл статуса iAdditionalService.selected
         * @param {number} id
         * @return void
         */
        setServiceSelected (id, current_state) {
            this.$store.commit('ADDITIONAL_SERVICE_TOGGLE', id);
	
            if(this.$config.ab.insure && id === 6) {
				if (typeof current_state !== 'undefined') {
					this.$backend.ABTestLog({
						state: Number(!current_state),
						user_id: this.$user.id,
						action: 'InsureCheckboxChange',
						ABTest: 1,
						page: this.__AB_TEST_PAGE_ID
					})
				}
			}

        },

        /**
         * Договор в новом окне
         * @param url
         */
        openDocument(url) {
            window.open(url);
        }
    }
}
