export default {
  mounted: function () {
    const savedUser = this.$store.getters.getUser
    this.user = {...this.user, ...savedUser}
  }
}
