export default {
  methods: {
    getValidateClass: function (key) {
      return this.vErrors.has(key) ? 'error' : ''
    }
  }
}
