export default {
  watch: {
    user: {
      handler: function () {
        this.$store.dispatch('setUserValuesAction', this.user)
      },
      deep: true
    }
  }
}
