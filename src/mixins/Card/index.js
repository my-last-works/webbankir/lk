export default {
  computed: {
    holder: {
      get() {
        return this.card.holder;
      },
      set(value) {
        let a = {};

        a['Й']='Q';a['й']='q';a['Ц']='W';a['ц']='w';a['У']='E';a['у']='e';a['К']='R';a['к']='r';a['Е']='T';a['е']='t';a['Н']='Y';a['н']='y';a['Г']='U';
        a['г']='u';a['Ш']='I';a['ш']='i';a['Щ']='O';a['щ']='o';a['З']='P';a['з']='p';a['Ф']='A';a['ф']='a';a['Ы']='S';a['ы']='s';a['В']='D';a['в']='d';
        a['А']='F';a['а']='f';a['П']='G';a['п']='g';a['Р']='H';a['р']='h';a['О']='J';a['о']='j';a['Л']='K';a['л']='k';a['Д']='L';a['д']='l';a['Я']='Z';
        a['я']='z';a['Ч']='X';a['ч']='x';a['С']='C';a['с']='c';a['М']='V';a['м']='v';a['И']='B';a['и']='b';a['Т']='N';a['т']='n';a['Ь']='M';a['ь']='m';
        a['Q']='Q';a['q']='q';a['W']='W';a['w']='w';a['E']='E';a['e']='e';a['R']='R';a['r']='r';a['T']='T';a['t']='t';a['Y']='Y';a['y']='y';a['U']='U';
        a['u']='u';a['I']='I';a['i']='i';a['O']='O';a['o']='o';a['P']='P';a['p']='p';a['A']='A';a['a']='a';a['S']='S';a['s']='s';a['D']='D';a['d']='d';
        a['F']='F';a['f']='f';a['G']='G';a['g']='g';a['H']='H';a['h']='h';a['J']='J';a['j']='j';a['K']='K';a['k']='k';a['L']='L';a['l']='l';a['Z']='Z';
        a['z']='z';a['X']='X';a['x']='x';a['C']='C';a['c']='c';a['V']='V';a['v']='v';a['B']='B';a['b']='b';a['N']='N';a['n']='n';a['M']='M';a['m']='m';

        Object.keys(a).forEach((key) => {
          value = value.replaceAll(key, a[key])
        });

        this.card.holder = value.toUpperCase();
      }
    },
    secretCard() {
      let part;
      if (this.series[1] !== undefined) {
        part = this.series[1].slice(0, 2)
      }

      return `${this.series[0]}-${part}**-****-${this.series[3]}`
    }
  },
  watch: {
    'formatter' (date) {
      this.card.month = date.slice(0, 2)
      this.card.year = date.slice(3, 5)
    }
  }
}
