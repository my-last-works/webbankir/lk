export default {
  beforeMount: function () {
    document.body.classList.add(this.bodyClassName)
  },
  beforeDestroy: function () {
    document.body.classList.remove(this.bodyClassName)
  }
}
