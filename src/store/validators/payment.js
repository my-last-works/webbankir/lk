const validPaymentMethods = ['POS', 'POS_REPAY', 'PDL', 'PDL_SUPPLEMENTARY'];

export function paymentCheckType (type) {
    if(validPaymentMethods.includes(type) === false) {
        console.warn(`Неизвестный тип платежа. Получен: ${type}, но известны только ${validPaymentMethods.join(', ')}`);
    }
    
    return true;
}

export function paymentCheckAmount (amount) {
    if(typeof amount !== "number") {
        console.warn('Неверный тип `amount`. Должен быть number, но получен ' + typeof amount);
    }
    
    if(amount < 10) {
        console.warn('Значение слишком маленькое. Сумма платежа не может быть менее 10р');
    }
    
    return true;
}
