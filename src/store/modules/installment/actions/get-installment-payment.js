import backend from '@/utils/backend';
import config from '@/config';
import store from '@/store';
const BASE_URL = config.POS_BASE_URL;
/**
 * @type iBackend.api
 */
const api = backend.api;

export default function () {
	api.transactions.history.pos().then(response => {
		const responseData = response?.data;
		if(!responseData) {
			return Promise.reject('responseData is empty');
		}
		const dataTransactions = responseData.transactions;
		if(!dataTransactions) {
			return Promise.resolve();
		}
		//2019-03-14 11:56
		store.commit(
			'ADD_INSTALLMENT_OPERATIONS',
			dataTransactions.map(transaction => ({
				...transaction,
				date: transaction.transaction_date,
				type: 'pos',
				shop: {},
			}))
		);
	}).catch((e) => {
		console.warn('get-installment-payment.js::history', e);
	});
	
	console.log('get-installment-payment.js::check user', `v2/user/${store.getters.$user.id}`);
	
	return new Promise((resolve, reject) => {
		return api.pos.user.get().then((data) => {
			store.commit('SET_INSTALLMENT_PAYMENT', data?.data?.data);
			store.commit('INTERFACE_SHOW_INSTALLMENT', true);
			resolve();
		}).catch((e) => {
			console.warn('get-installment-payment.js::get user', e.response);
			store.commit('INTERFACE_SHOW_INSTALLMENT', false);
			reject(e?.response?.data?.data?.errors[0]?.code || e);
		})
	})
};
