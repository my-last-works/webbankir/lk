import api from '@/utils/backend';
import config from '@/config/';
import store from '@/store';

export default () => {
	const {state, getters} = store;
	const { operations } = state.installment;

	/*очистка массива (ленты) операций*/
	operations.splice(0, operations.length);
	/*промис получения списка операций операций*/
	const getUserOperationById = new Promise((resolve, reject) => {
		api.getData(`v1/user/${getters.$user.id}/operation`, config.POS_BASE_URL).then(response => {
			const responseData = response.data;
			if(!responseData) return;
			const responseDataData = responseData.data;
			if(!responseDataData) return;
			resolve(
				responseDataData.map(
					row => ({
						...row,
						type: 'operation',
					})
				)
			);
		});
	});
	/*промис получения списка операций возврата*/
	const getUserRejectionById = new Promise((resolve, reject) => {
		api.getData(`v1/user/${getters.$user.id}/rejection`, config.POS_BASE_URL).then(response => {
			const responseData = response.data;
			if(!responseData) return;
			const responseDataData = responseData.data;
			if(!responseDataData) return;
			resolve(
				responseDataData.map(
					row => ({
						...row,
						type: 'rejection',
						isRejection: true,
						sum: row.amount,
						// date: row.date.date,
					})
				)
			);
		})
	});

	/*ожидание завершения выполнения всей очереди асинхронных операций*/
	return Promise
		.all([getUserOperationById, getUserRejectionById])
		.then(operationsLists => {
			operationsLists.forEach(operationList => {
				store.commit('ADD_INSTALLMENT_OPERATIONS', operationList)
			});
			
			return Promise.resolve();
		})
	;
};
