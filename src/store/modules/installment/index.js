import api from '@/utils/backend-api';
import config from '@/config';
const BASE_URL = config.POS_BASE_URL;
import GET_INSTALLMENT_OPERATIONS from './actions/get-installment-operations';
import GET_INSTALLMENT_PAYMENT from './actions/get-installment-payment';

const state = {
    partnerShops: [],
    operations: [],
    schedule: [],
    payment: {
        current_limit: 0,
        id: 0,
        current_minimal_payment: 0,
        current_arrear: 0,
    },
    limit: 0,

    geo_type: 'city_fias',
    geo_fias_id: '',
    location: {
        lat: null,
        lon: null,
    }
};

/**
 * Вычисление минимально платежа за текущий период
 * @TODO это бредятина, высчитывать должен бэк
 * @param state
 * @return number
 */
const paymentMinimal = (state) => {
    return state.schedule.length ? state.schedule[0].amount : 0;
}


/**
 * Высчитывание общей задолженности
 * @TODO это тоже бредятина и костыль. бэк...
 * @param state
 */
const paymentArrear = (state) => {
  let amount = 0;
  state.schedule.forEach(
    /**
     * @param {INSTALLMENT_SCHEDULE} s
     */
    (s) => {
    amount += s.amount;
  });

  return amount;
};

const paymentCurrentSchedule = (state) => {
  return state.schedule[0];
}

const getters = {
  $installment: (state) => {
    state.payment.current_minimal_payment = paymentMinimal(state);
    state.payment.current_arrear = paymentArrear(state);
    state.currentPaymentBySchedule = paymentCurrentSchedule(state);
    return state;
  }
}

const mutations = {
  SET_INSTALLMENT_PARTNERS: (state, data) => {
    state.partnerShops = data;
  },
  ADD_INSTALLMENT_OPERATIONS: (state, data) => {
	  state.operations.push(/*... Array.isArray(data) ? data : [data]*/...data);
  },
  SET_INSTALLMENT_OPERATIONS: (state, data) => {
    data.forEach((data) => {
      data.expand = false;
    })
    state.operations = data;
  },
  SET_INSTALLMENT_SCHEDULE: (state, data) => {
    data.forEach((s) => {
      if(Array.isArray(s.sales)) {
        s.sales.forEach((sale) => {
          sale.selected = true;
        })
      }
    })
    state.schedule = data;
  },

  SET_INSTALLMENT_PAYMENT: (state, data) => {
    state.payment = data;
  },

  SET_INSTALLMENT_LIMIT: (state, limit) => {
    state.limit = limit;
  },
    SET_POS_FIAS(state, data) {

    //base cities
    if(data.city && data.city_fias_id) {
        state.city = data.city;
        state.geo_fias_id = data.city_fias_id;
        state.geo_type = 'city_fias';
    }
    // city from dadata
    else if (data.city && data.fias_id) {
        state.city = data.city;
        state.geo_fias_id = data.fias_id;
        state.geo_type = 'city_fias';
    }
    else if (data.region && data.fias_id) {
      state.city = data.region;
      state.geo_fias_id = data.fias_id;
      state.geo_type = 'region_fias';
    }
    console.log(data);
        // console.log(city, fias_id, region);
        // state.city = city || region;
        // state.geo_fias_id = fias_id;
        // state.geo_type = city ? 'city_fias' : 'region_fias';
    },
    SET_POS_LOCATION(state, { lat, lon }) {
        console.log(lat, lon);
        state.geo_type = 'geo';
        state.location.lat = lat;
        state.location.lon = lon;
    }

}

const actions = {

  GET_INSTALLMENT_DOC: (store, id) => {
    return api.getData(`v1/user/${store.getters.$user.id}/operation/${id}/contract`, BASE_URL, true).then((response) => {
       api.getDoc(response.data, `contract_${id}.pdf`);
       return Promise.resolve();
    })
  },

  GET_INSTALLMENT_DATA: (store) => {
    const promises = [];
    promises.push(store.dispatch('GET_INSTALLMENT_OPERATIONS'));
    promises.push(store.dispatch('GET_INSTALLMENT_SCHEDULE'));
    promises.push(store.dispatch('GET_INSTALLMENT_PARTNERS'));
    promises.push(store.dispatch('GET_INSTALLMENT_PAYMENT'));
    return Promise.all(promises);
  },

  GET_INSTALLMENT_PARTNERS: (store) => {
    console.log('GET_INSTALLMENT_PARTNERS', store.state.geo_type);
    let path = '';
    if(store.state.geo_type === 'city_fias') {
        path = `?city_fias_id=${store.state.geo_fias_id}`;
    } else if(store.state.geo_type === 'region_fias') {
        path = `?region_fias_id=${store.state.geo_fias_id}`;
    } else if (store.state.geo_type === 'geo') {
        path = `?latitude=${store.state.location.lat}&longitude=${store.state.location.lon}&distance=5000`;
    }
    console.log(path);

    if(path) {
        path += '&publication=on_publication'
    } else {
        path = '?publication=on_publication'
    }


    return api.getData('v2/merchant'+path, BASE_URL).then((data) => {
      store.commit('SET_INSTALLMENT_PARTNERS', data.data.data)
      return data;
    })
  },

    GET_INSTALLMENT_OPERATIONS,

  GET_INSTALLMENT_SCHEDULE: (store) => {
    return api.getData(`v2/user/${store.getters.$user.id}/schedule`, BASE_URL).then((data) => {
      store.commit('SET_INSTALLMENT_SCHEDULE', data.data.data)
      return data;
    })
  },

  GET_INSTALLMENT_PAYMENT,

  GET_INSTALLMENT_PARTNER_ID: (store, id) => {
    const promises = [];
    promises.push(api.getData(`v2/merchant/${id}`, BASE_URL).then((data) => {
      return data.data.data;
    }));

    promises.push(api.getData(`v2/merchant/${id}/shop`, BASE_URL).then((data) => {
      return data.data.data;
    }));

    return Promise.all(promises).then((data) => {
      return {
        merchant: data[0],
        shop: data[1]
      }
    })
  }

  /*GET_INSTALLMENT_OPERATION_INFO: (store, {shop_id, merchant_id}) => {
    return api.getData(`merchant/${merchant_id}/shop/${shop_id}`, BASE_URL).then(({data}) => {
      return data;
    })
  },*/
}


export default {
  state, actions,
  getters, mutations
}
