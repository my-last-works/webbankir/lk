import backendApi from '../../utils/backend-api'
import {DOCS_SET_STATUS, DOCS_SET_DOCS} from '../mutation-types'
const DOCS_GET = 'DOCS_GET'

const state = {
  docs: [
  ],
  status: ''
}
const getters = {
  getDocsStatus: (state) => state.status,
  getDocs: (state) => state.docs
}

const mutations = {
  [DOCS_SET_STATUS]: (state, status) => { state.status = status },
  [DOCS_SET_DOCS]: (state, loans) => {
    state.status = 'ok'
    state.docs = loans
  }
}

const actions = {
  [DOCS_GET]: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit(DOCS_SET_STATUS, 'firstLoad')
      backendApi.getData('docs').then((response) => {
        commit(DOCS_SET_DOCS, response.data)
        resolve(response)
      }).catch(response => {
        console.log(response)
        reject(response)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
