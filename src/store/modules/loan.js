// loan storage refactoring
import backend from '@/utils/backend';
import findKey from 'lodash/findKey';
import dayjs from 'dayjs';
import {getImgFilename} from '@/utils/mappings/accounts';
import router from '@/router';

/**
 * @type iBackend.api
 */
const api = backend.api;
/*export const getImgFilename = (type, number) => {
  if (type === undefined) {
    return
  }
  const types = {
    1: (cardNumber) => cardNumber ? _.get(cardType(cardNumber.toString().slice(0, 3)), `[0].type`, 'visa') + '.png' : 'visa.png',
    5: () => 'old-contact.png',
    3: () => 'yandex.png',
    6: () => 'qiwi.png'
  }
  return _.get(types, type, () => 'visa.png')(number)
}*/

export const getPaySystemText = (paySystems) => ({
	1: 'Карта',
	3: 'Яндекс кошелёк',
	5: 'Контакт',
	6: 'QIWI Wallet',
})[paySystems];

const needToSign = (loan) => {
	if (loan.status === 3) { // Договор ожидает подписания
		// const needToSign = {type: 'contract'} // Именно объект, т7к7 подписание может быть договора, допника или еще чего
		return {needToSign: true};
	}
	return null;
};

const statusMapper = (status) => {
	const iconClass = {
		'stat-1': [1, 2, 3],
		'stat-2': [4, 5, 6, 7],
		'stat-3': [100, 101, 102],
	};
	const statusGroup = {
		'wait': [1, 2, 3],
		'approved': [4, 5, 6, 7],
		'canceled': [100, 101, 102],
	};
	const statusText = {
		1: 'Ожидает рассмотрения',
		2: 'Рассматривается',
		3: 'Ожидает подписания',
		4: 'Договор подписан',
		5: 'В очереди на оплату',
		6: 'Займ выдан',
		7: 'Займ погашен',
		100: 'Отказано',
		101: 'Клиент отказался',
		102: 'Истек срок подписания',
	};
	return {
		class: findKey(iconClass, (statusArray) => statusArray.includes(status)),
		statusGroup: findKey(statusGroup, (statusArray) => statusArray.includes(status)),
		statusText: statusText[status],
		canBePayed: status === 6,
	};
};

const default_state = () => {
	return {
		pdl: { // loan/get/current/pdl
			supplementary: {
				amnesty2019: null,
				installment: null,
				prolongation: null,
				loanNewDateEnd: null,
			},
		},
		pos: [], // loan/get/current/pos
		calculate: { // calculator-loan/v2
			insurance: {},
			legal: {},
		},
		history: [],// loan/get
		prolongation: {}, // supplementary/prolongation/calc,
		payment: {
			totalDebt: 0,
			iframe: true,
			process: false,
			type: 1,
		},
		accounts: [],
		activeLoanAction: '',
		graphicInstallment: [],
		loanType: 1,
		qiwi: false,
		serviceHistory: [],
	};
};

const state = default_state();

const getters = {
	$pdl: (state) => {
		return {...state.pdl, ...statusMapper(state.pdl.status), ...needToSign(state.pdl)};
	},
	$pos: (state) => state.pos.filter((pos) => pos.type !== 3),
	$posServices: (state) => state.pos.find(({type}) => type === 3),
	$calculate: (state) => state.calculate,
	$history: (state) => state.history,
	$prolongation: (state) => state.prolongation,
	$payment: (state) => {
		state.payment.totalDebt = 0;
		state.payment.prolongationDeposit = 0;
		const payment = state.payment;
		const posServices = state.pos.find((pos) => pos.type === 3);
		if (posServices) {
			payment.totalDebt += parseFloat(posServices.sumTotal);
			payment.prolongationDeposit += parseFloat(posServices.sumTotal);
		}
		
		if (state.pdl.supplementary && state.pdl.supplementary.prolongation) {
			payment.prolongationDeposit += parseFloat(state.pdl.supplementary.prolongation.depositAmount);
		}
		payment.totalDebt += parseFloat(state.pdl.sumTotal);
		return payment;
	},
	$accounts: (state) => {
		const accounts = [];
		state.accounts.forEach((a) => {
			if (a.type === 6) {
				a.name = 'QIWI Wallet';
			}
			accounts.push({
				...a,
				canBeMain: a.type !== 6,
				editable: a.type === 1,
				deletable: !a.main && (a.type === 3 || a.type === 1),
				img: getImgFilename(a.type, a.description),
			});
		});
		return accounts;
	},
	ActiveLoanAction: state => state.activeLoanAction,
	$graphicInstallment: state => state.graphicInstallment,
	$loanType: state => state.loanType,
	$qiwi: state => state.qiwi,
};

const mutations = {
	CLEAR_LOAN_DATA: (state) => {
		state = Object.assign({}, default_state());
	},
	LOAN_SET_LOAN_TYPE: (state, type) => state.loanType = type,
	'SET_ACTIVE_LOAN_ACTION': (state, action) => state.activeLoanAction = action,
	'LOAN_SET_PDL_RECOUNT': (state, value) => state.pdl.needRecount = value,
	'LOAN_SET_PAYMENT_PROCESS': (state, data) => state.payment.process = data,
	'LOAN_SET_PAYMENT_IFRAME': (state, data) => state.payment.iframe = data,
	'LOAN_SET_PDL': (state, data) => {
		if (data && data.status === 3) {
			data.needToSign = {
				type: 'contract',
			};
		}
		data.needRecount = false;
		state.pdl = data;
	},
	'LOAN_SET_POS': (state, data) => {
		data.forEach((pos, index) => {
			data[index] = {...pos, ...statusMapper(pos.status), ...needToSign(pos), needRecount: false};
		});
		state.pos = data;
	},
	'LOAN_SET_CALC': (state, data) => {
		state.calculate = data;
	},
	'LOAN_SET_HISTORY': (state, data) => state.history = data,
	'LOAN_SET_PROLONGATION': (state, data) => state.prolongation = data,
	'LOAN_SET_ACCOUNTS': (state, data) => {
		const a = [];
		data.forEach((acc) => {
			a.push({...acc, iconSmall: `/lk/static/img/accounts/${getImgFilename(acc.type, acc.description)}`});
		});
		state.accounts = a;
	},
	LOAN_SET_INSTALLMENT: (state, graphic) => state.graphicInstallment = graphic,
	CHANGE_LOAN_TYPE: (state, type) => state.loanType = type,
	CHECK_QIWI: (state, bool) => state.qiwi = bool,
	LOAN_SET_SERVICE_HISTORY: (state, note) => {
		console.log('state.serviceHistory = note,', note);
		state.serviceHistory = note || [];
	}
};

const installment_graph_query = [];
let installment_query = false;

const actions = {
	LOAN_GET_CALC: ({commit}) => {
		return api.calc.get().then(({data}) => {
			commit('LOAN_SET_CALC', data.data);
			return Promise.resolve();
		});
	},
	/**
	 *
	 * @param {vuex} context
	 * @param data
	 * @returns {Promise<any>}
	 * @constructor
	 */
	'LOAN_CREATE': ({commit}, data) => {
		return new Promise((resolve, reject) => {
			api.loan.create(data).then(({data}) => {
				data.data.forEach((loan) => {
					switch (loan.type) {
						case 1:
							commit('LOAN_SET_PDL', loan);
							break;
						case 3:
							/**
							 * POS должны быть в массиве
							 */
							commit('LOAN_SET_POS', [loan]);
							break;
						default:
							console.warn('unsupported loan type: ', loan.type);
							break;
					}
				});
				resolve();
			}).catch((e) => {
				reject(e);
			});
		});
	},
	
	'LOAN_INSTALLMENT_CREATE': ({commit}, data) => {
		return new Promise((resolve, reject) => {
			api.installment.create(data).then(() => {
				resolve();
			}).catch((e) => {
				reject(e);
			});
		});
	},
	
	'GET_LOAN_PDL': (context) => {
		return api.loan.pdl().then(async ({data}) => {
			if (data.data && data.data.id) {
				context.commit('LOAN_SET_PDL', data.data);
				return Promise.resolve(data.data);
			} else {
				return await api.installment.get().then((data) => {
					if (data.data && !data.data.id || !data.data.supplementary) {
						data.data.supplementary = {
							prolongation: null,
							installment: null,
							amnesty2019: null,
							loanNewDateEnd: null,
						};
					}
					context.commit('LOAN_SET_PDL', data.data);
					context.commit('CHANGE_LOAN_TYPE', 8);
					return Promise.resolve(data.data);
				}).catch((e) => {
					console.log('e', e);
				});
			}
			
		});
	},
	
	'GET_LOAN_POS': (context) => {
		return api.loan.pos().then(({data}) => {
			context.commit('LOAN_SET_POS', data.data);
			return Promise.resolve();
		});
	},
	
	'LOAN_GET_ACCOUNTS': (context) => {
		return api.accounts.get().then(({data}) => {
			console.log(data.data);
			context.commit('LOAN_SET_ACCOUNTS', data.data);
			return Promise.resolve();
		});
	},
	
	// LOAN_GET_CREDIT_HISTORY: (context) => {
	//   return api.getData('creditrating/history').then(({data}) => {
	// 	  context.commit('LOAN_SET_SERVICE_HISTORY', data.data.active);
	// 	  return Promise.resolve();
	//   });
	// },
	
	GET_GRAPHIC_INSTALLMENT: (context, [amount, mouths, date]) => {
		if (installment_query) {
			installment_graph_query.push('GET_GRAPHIC_INSTALLMENT');
			return;
		}
		installment_query = true;
		context.commit('LOAN_SET_INSTALLMENT', []);
		const days = Math.abs(dayjs().diff(dayjs().add(mouths, 'months'), 'days'));
		console.log(days);
		return api.installment.schedule({
			amount,
			days,
			date,
		}).then(({data}) => {
			context.commit('LOAN_SET_INSTALLMENT', data.data);
			return Promise.resolve();
		}).finally(() => {
			installment_query = false;
			if (installment_graph_query.length) {
				context.dispatch(installment_graph_query.pop());
			}
		});
	},
	
	'GET_PROLONGATION_DATA': (context) => {
		return api.supplementary.prolongation.calc().then(({data}) => {
			context.commit('LOAN_SET_PROLONGATION', data.data);
			return Promise.resolve();
		});
	},
	
	'GET_ALL_LOANS': (context) => {
		if (context.getters.getAuthStatus !== 'authorized') {
			return Promise.resolve();
		}
		
		const promises = [];
		// get current pdl or installment-loan
		promises.push(context.dispatch('GET_LOAN_PDL'));
		
		promises.push(api.loan.pos().then(({data}) => {
			context.commit('LOAN_SET_POS', data.data);
			return Promise.resolve();
		}));
		
		promises.push(api.calc.get().then(({data}) => {
			context.commit('LOAN_SET_CALC', data.data);
			return Promise.resolve();
		}));
		
		promises.push(api.loan.all().then(({data}) => {
			context.commit('LOAN_SET_HISTORY', data.data);
			return Promise.resolve();
		}));
		
		promises.push(api.accounts.get().then(({data}) => {
			context.commit('LOAN_SET_ACCOUNTS', data.data);
			return Promise.resolve();
		}));
		
		api.services.history().then(({data}) => {
			context.commit('LOAN_SET_SERVICE_HISTORY', data?.data?.active);
		});
		
		api.supplementary.prolongation.calc().then(({data}) => {
			context.commit('LOAN_SET_PROLONGATION', data.data);
		});
		
		// api.getData('creditrating/history').then(({data}) => {
		// 	context.commit('LOAN_SET_SERVICE_HISTORY', data?.data?.active);
		// 	return Promise.resolve();
		// });
		//
		// api.getData('supplementary/prolongation/calc').then(({data}) => {
		// 	context.commit('LOAN_SET_PROLONGATION', data.data);
		// 	return Promise.resolve();
		// });
		
		//   promises.push(api.getData('supplementary/prolongation/calc').then(({data}) => {
		//     context.commit('LOAN_SET_PROLONGATION', data.data);
		//     return Promise.resolve();
		//   }));
		
		return Promise.all(promises);
	},
};

export default {
  state,
  actions,
  mutations,
  getters
}
