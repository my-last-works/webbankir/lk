export default {
    state: {
        notifications: {
            messageTitle: '',
            messageText: '',
            messageType: '',
            display: false,
            buttons: [],
        }
    },

    mutations: {
        NOTIFICATION_MESSAGE_TITLE: (state, title) => state.notifications = {...state.notifications, messageTitle: title},
        NOTIFICATION_MESSAGE_TEXT: (state, text) => state.notifications = {...state.notifications, messageText: text},
        NOTIFICATION_MESSAGE_TYPE: (state, type) => state.notifications = {...state.notifications, messageType: type},
        NOTIFICATION_CLOSE: (state) => state.notifications = {...state.notifications, display: false, buttons: []},
        NOTIFICATION_SHOW: (state) => state.notifications = {...state.notifications, display: true},
        NOTIFICATIONS_BUTTONS: (state, buttons) => state.notifications = {...state.notifications, buttons}
    },


    getters: {
        /**
         * iNotification
         * @param state
         * @returns {iNotification}
         */
        $notifications: state => state.notifications
    }
}
