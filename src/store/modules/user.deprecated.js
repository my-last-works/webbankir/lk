import {UNSET_USER, SET_AUTHORIZED, UNSET_AUTHORIZED, SET_USER, UNSET_ERRORS, SET_USER_GEOPOSITION} from '../mutation-types'
import backendApi from '@/utils/backend-api'
import {SET_STATUS, USER_RESET} from '@/store/mutation-types'
import clone from 'lodash/clone'
import merge from 'lodash/merge'
import pick from 'lodash/pick'
import reverse from 'lodash/reverse'
import Vue from 'vue'

const initialUserState = {
  // esia
  storageKey: '',
  passportEsiaStatus: '',
  // main
  lastLoginTimeOnTheRegistrationPage: null,
  id: null,
  firstName: null,
  lastName: null,
  middleName: null,
  bDay: null,
  bPlace: null,
  login: null,
  gender: null,
  mobilePhone: null,
  email: null,
  smsCode: null,
  promoCode: null,
  promoCodeCounter: 3,
  promoCheck: false,
  promoPercent: null,
  percent: null,
  blocked: false,

  // СЕКРЕТНЫЙ ВОПРОС
  controlQuestionType: 1,
  controlQuestion: null,
  controlAnswer: null,

  // ПАСПОРТНЫЕ ДАННЫЕ
  passport: null,
  passportDivisionCode: null,
  passportDateOfIssue: null,
  passportIssuedBy: null,

  // АДРЕС РЕГИСТРАЦИИ
  address: {
    region_with_type: null,
    city_type: null,
    area_with_type: null,
    settlement_with_type: null,
    street_type: null,
    postalCode: null,
    region: null,
    city: null,
    settlement: null,
    street: null,
    doNotHaveStreet: false,
    house: null,
    geoLat: null,
    geoLon: null,
    housing: null,
    building: null,
    flat: null
  },
  addressFiases: {
    value: null,
    region_fias_id: null,
    area_fias_id: null,
    city_fias_id: null,
    settlement_fias_id: null,
    street_fias_id: null,
    house_fias_id: null
  },

  // ДОПОЛНИТЕЛЬНЫЙ ТЕЛЕФОН
  additionalPhone: null,
  additionalPhoneOwner: null,

  // ИНФОРМАЦИЯ О РАБОТЕ И ДОХОДЕ
  typeOfEmployment: null,
  workSalary: null,
  workINN: null,
  workFullName: null,
  workType: null,
  workScope: null,
  workName: null,
  workPhone: null,
  workNumberOfEmployees: null,
  workPeriod: null,
  workPosition: null,
  workAddress: {
    region_with_type: null,
    city_type: null,
    area_with_type: null,
    settlement_with_type: null,
    street_type: null,
    postalCode: null,
    region: null,
    city: null,
    settlement: null,
    street: null,
    doNotHaveStreet: false,
    house: null,
    geoLat: null,
    geoLon: null,
    housing: null,
    building: null,
    flat: null
  },
  workAddressFiases: {
    value: null,
    region_fias_id: null,
    area_fias_id: null,
    city_fias_id: null,
    settlement_fias_id: null,
    street_fias_id: null,
    house_fias_id: null
  },

  // ДОПОЛНИТЕЛНАЯ ИНФОРМАЦИЯ
  // snils: null,
  snilsOrInn: null,
  maritalStatus: null,
  educationType: null,
  sourceOfInformation: null,
  numberOfChildren: null,
  hasPreviousConviction: null,

  // ПУБЛИЧНОСТЬ
  publicity: {
    // Публичное должносное лицо
    officialState: false,
    officialOwnerIsMe: false,
    officialName: null,
    // Публичное должностное лицо международной организации
    internationalOrganizationState: false,
    internationalOrganizationOwnerIsMe: false,
    internationalOrganizationName: null,
    // Публичное должностное лицо российской организации
    officialRussiaState: false,
    officialRussiaOwnerIsMe: false,
    officialRussiaName: null,
    // Выгодоприобретатель
    benefitsOfAnotherPerson: false
  },
  // БЕНЕФИЦИАРНЫЙ ВЛАДЕЦ
  beneficial: {
    state: false,
    // В случае, если пользователь указал другого БВ
    lastName: null,
    firstName: null,
    middleName: null,
    passport: null,
    passportDivisionCode: null,
    passportDateOfIssue: null
  },
  // В КОНЦЕ 3 ШАГА ПРИ НАЖАТИЕ НА ПРОДОЛЖИТЬ СЧЕТЧИК СТАНОВИТЬСЯ 1
  final: 0,

  // ТИП ПОЛУЧЕНИЯ БАБОСИКОВ
  type_get_money: null,
  bank_card: null,
  monthBankCard: null,
  yearBankCard: null,
  yandex_wallet: null,

  // История займов
  creditHistory: {
    hasMortgage: false,
    mortgageSum: null,
    mortgageCurrency: 1,
    mortgageTerm: null,
    mortgageTypeDate: 1,
    // автокредит
    hasAutoCredit: false,
    autoCreditSum: null,
    autoCreditCurrency: 1,
    autoCreditTerm: null,
    autoCreditTypeDate: 1,
    // кредитная карта
    hasCreditCard: false,
    creditCardLimit: null,
    creditCardCurrency: 1,
    creditCardTermYear: 1,
    creditCardTermMonth: 1,
    // потребительский кредит
    hasLoan: false,
    loanSum: null,
    loanCurrency: 1,
    loanTerm: null,
    loanTypeDate: 1,
    // в случае - если у клиента нет каких-либо долгов
    hasNoCredit: false,
    // когда пользователь ответил на вопросы по истории займов и нажал на кнопку подтвердить - счетчик меняется на true
    historyIsFull: false
  }
}

const state = {
  registerSmsSendCount: 0, // сколько всего отправлено смс при регистрации
  registerSmsSendByPhoneCount: 0, // сколько всего отправлено смс на конкретный номер телефона.
  registerSmsSendTime: 0, // блокировка должно быть на час
  geoposition: null,
  user: clone(initialUserState),
  registrationStep: 1,
  isPersonalDataValid: true,
  errorText: {},
  authorized: true,
  validatedForms: {
    step1: {
      firstLastMiddleName: false,
      contactMobilePhone: false,
      contactEmail: false
      // controlQuestionAndAnswer: false //moved to step 4
    },
    step2: {
      birthDate: false,
      passportInformation: false,
      // snils: false, // moved to step 4
      registrationAddress: false
    },
    step3: {
      additionPhone: false,
      workInformation: false,
      additionalInfo: false,
      publicInformation: false,
      beneficialInformation: false
    },
    other: {
      employer: false,
      workAddress: false,
      businessman: false,
      founderFullName: false
    }
  }
}

const getters = {
  getRegisterSmsSendCount: state => {
    const now = Math.round(new Date().getTime() / 1000)
    if (now - state.registerSmsSendTime > 3600) {
      state.getRegisterSmsSendCount = 0
    }
    return state.registerSmsSendCount
  },
  getRegisterSmsSendByPhoneCount: state => {
    const now = Math.round(new Date().getTime() / 1000)
    if (now - state.registerSmsSendTime > 3600) {
      state.registerSmsSendByPhoneCount = 0
    }
    return state.registerSmsSendByPhoneCount
  },
  getUserAge: (state) => {
    if (!state.user.bDay) {
      return null
    }
    const dateString = reverse(state.user.bDay.split('.')).join('/')
    const today = new Date()
    const birthDate = new Date(dateString)
    let age = today.getFullYear() - birthDate.getFullYear()
    const m = today.getMonth() - birthDate.getMonth()
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--
    }
    return age
  },
  getUserLogin: (state) => state.user.login,
  getUser: state => {
    return {...state.user, validate: state.validatedForms}
  },
  getLoading: state => state.loading,
  getUserFio: state => state.user.lastName ? state.user.lastName + ' ' + state.user.firstName + ' ' + state.user.middleName : '',
  getIndicatorRegistration: state => {
    return state.indicatorRegistration
  },
  getValidatedForms: state => {
    return state.validated_forms
  },
  getValidationFields: state => {
    return state.validatedForms
  },
  getButtonContinue: state => {
    return state.button_continue
  },
  getRegistrationStep: state => {
    return state.registrationStep
  },
  getErrorText: state => {
    return state.errorText
  },
  isPersonalDataValid: state => {
    return state.isPersonalDataValid
  },
  personalPercent: state => {
    return state.user.percent
  }
}

// Mutations
const mutations = {
  'registerSmsSendCountIncrement': state => {
    state.registerSmsSendCount += 1
    state.registerSmsSendByPhoneCount += 1
    state.registerSmsSendTime = Math.round(new Date().getTime() / 1000)
  },
  'registerSmsCountReset': state => {
    state.registerSmsSendByPhoneCount = 0
    state.registerSmsSendCount = 0
    state.registerSmsSendTime = Math.round(new Date().getTime() / 1000)
  },
  'registerSmsSendByPhoneReset': state => {
    state.registerSmsSendByPhoneCount = 0
    state.registerSmsSendTime = Math.round(new Date().getTime() / 1000)
  },
  [USER_RESET]: (state) => {
    state.user = clone(initialUserState)
    state.registrationStep = 1
    // address
    Object.keys(state.user.address).forEach(key => {
      state.user.address[key] = null
      state.user.address['doNotHaveStreet'] = false
    })
    Object.keys(state.user.addressFiases).forEach(key => state.user.addressFiases[key] = null)
    // work address
    Object.keys(state.user.workAddress).forEach(key => {
      state.user.workAddress[key] = null
      state.user.workAddress['doNotHaveStreet'] = false
    })
    Object.keys(state.user.workAddressFiases).forEach(key => state.user.workAddressFiases[key] = null)
    Object.keys(state.user.publicity).forEach(key => {
      state.user.publicity[key] = false
      state.user.publicity['officialName'] = null
      state.user.publicity['internationalOrganizationName'] = null
      state.user.publicity['officialRussiaName'] = null
    })
    Object.keys(state.user.beneficial).forEach(key => {
      state.user.beneficial[key] = null
      state.user.beneficial['state'] = false
    })
  },
  [SET_AUTHORIZED]: (state) => {
    state.authorized = true
  },
  [UNSET_AUTHORIZED]: (state) => {
    state.authorized = false
  },
  [UNSET_USER]: (state) => {
    state.user = {}
  },
  [SET_USER] (state, userDeprecated) {
    state.user = {
      ...state.user,
      ...userDeprecated
    }
    // console.log('SET_USER', state.user)
  },
  SET_LOADING: (state) => {
    state.loading = true
  },
  UNSET_LOADING: (state) => {
    state.loading = false
  },
  SET_INVALID_PERSONAL_DATA: (state) => {
    state.isPersonalDataValid = false
  },
  UNSET_INVALID_PERSONAL_DATA: (state) => {
    state.isPersonalDataValid = true
  },
  [SET_USER_GEOPOSITION]: (state, geoposition) => {
    state.geoposition = geoposition
  },
  DECREMENT_PROMO: (state) => {
    state.user.promoCodeCounter--
  },
  PROMO_RESET: (state) => {
    state.user.promoCode = null
  }
}

const actions = {
  logout: function ({commit}) {
    commit(UNSET_AUTHORIZED)
    commit(UNSET_USER)
    commit(UNSET_ERRORS)
    localStorage.removeItem('token')
    localStorage.removeItem('token_expire')
  },
  setUserValuesAction: async ({commit}) => {
    commit(SET_STATUS, 'loadingUser')
    commit(SET_STATUS, 'ok')
  },
  sendSelectiveField: ({state}, field) => {
    let user = field.reduce((acc, field) => merge(acc, pick(state.user, field)), {})
    /**
     mobilePhone: postPhoneToBackendAndTransform(user.mobilePhone),
     passport: postPassportToBackendAndTransform(user.passport),
     passportDivisionCode: postCodeOfPassportToBackendAndTransform(user.passportDivisionCode),
     snils: postSnilsToBackendAndTransform(user.snils),
     additionalPhone: postPhoneToBackendAndTransform(user.additionalPhone),
     workPhone: postPhoneToBackendAndTransform(user.workPhone)
     * @type {{}}
     */
    if (field.includes('mobilePhone')) {
      user.mobilePhone = postPhoneToBackendAndTransform(user.mobilePhone)
    }
    if (field.includes('passport')) {
      user.passport = postPassportToBackendAndTransform(user.passport)
    }
    if (field.includes('passportDivisionCode')) {
      user.passportDivisionCode = postCodeOfPassportToBackendAndTransform(user.passportDivisionCode)
    }
    if (field.includes('additionalPhone')) {
      user.additionalPhone = postPhoneToBackendAndTransform(user.additionalPhone)
    }
    if (field.includes('workPhone')) {
      user.workPhone = postPhoneToBackendAndTransform(user.workPhone)
    }
    // user = {
    //   ...user
    // }
    return backendApi.postData(
      'user/create', user
    )
  },
  sendAllFields: ({state}, field) => {
    let user = field.reduce((acc, field) => merge(acc, pick(state.user, field)), {})
    user = {
      ...user,
      mobilePhone: postPhoneToBackendAndTransform(user.mobilePhone),
      passport: postPassportToBackendAndTransform(user.passport),
      passportDivisionCode: postCodeOfPassportToBackendAndTransform(user.passportDivisionCode),
      // snils: postSnilsToBackendAndTransform(user.snils), //moved to 4 step
      additionalPhone: postPhoneToBackendAndTransform(user.additionalPhone),
      workPhone: postPhoneToBackendAndTransform(user.workPhone),
      beneficial: {
        state: user.beneficial.state,
        lastName: user.beneficial.lastName,
        firstName: user.beneficial.firstName,
        middleName: user.beneficial.middleName,
        passport: postPassportToBackendAndTransform(user.beneficial.passport),
        passportDivisionCode: postCodeOfPassportToBackendAndTransform(user.beneficial.passportDivisionCode),
        passportDateOfIssue: user.beneficial.passportDateOfIssue
      },
      final: 1,
      referer: Vue.prototype.$cookie.get('site_referer')
    }
    return backendApi.postData(
      'user/v2/create', user
    )
  },
  lastLoginTimeOnTheRegistrationPageAndClearVuex: (state) => {
    let lastEntryTime = state.state.user.lastLoginTimeOnTheRegistrationPage
    console.log('Запущен таймер на сохранение параметров')
    if (Date.now() - lastEntryTime > 43200000) { // сессия расчитана на 12 часа, после чего происходит очищение vuex'a 43200000
      console.log('Таймер сохранения данных истек - данные очищены')
      state.commit('USER_RESET')
    }
  }
}

// Трансформирует данные для отправки на бэкенд

const postPhoneToBackendAndTransform = (phone) => phone ? String(phone).match(/\d/g).join('') : null
const postPassportToBackendAndTransform = (passport) => passport ? String(passport).split(' ').join('') : null
const postCodeOfPassportToBackendAndTransform = (code) => code ? String(code).split('-').join('') : null
// const postSnilsToBackendAndTransform = (snils) => snils ? String(snils).match(/\d/g).join('') : null

export default {
  state,
  getters,
  mutations,
  actions
}
