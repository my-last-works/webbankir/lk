const state = {
  guid: {
    data: []
  },
  price: 0,
  services: []
}

const getters = {
  $service: (state => state)
}

const mutations = {
  setServiceGuid (state, guid) {
    state.guid.data = guid;
  },
  setServicePrice (state, price) {
    state.price = price;
  },
  setServiceList (state, list) {
    state.services = list;
  }
};


export default {
  state, getters, mutations
}

