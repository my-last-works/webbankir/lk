import backend from '@/utils/backend';
const api = backend.api;

const default_state = () => {
    return {
        activeContentComponentName: 'FillingTheForm',
        firstRequestCreditRatingTimestamp: 0,
        resendRequestToGetLatestCreditRatingIntervalMs: 10 * 1000,
        getCreditRatingByGuidTimeoutMs: 3 * 1000 * 60,
        dataForRequestBuildCreditRatingGuid: null,
        // latestHash: '',
        creditRatingRequestsHistory: [],
        paymentCreditRatingAggregator: '',
        paymentCreditRatingFrameUrl: '',
        latestResponseGuid: '',
        latestResponsePrice: '',
    };
}

const state = default_state();

const getters = {

};

const mutations = {
	setActiveContentComponentName: (state, componentName) => state.activeContentComponentName = componentName,
	setdataForRequestBuildCreditRatingGuid: (state, data) => {
		state.dataForRequestBuildCreditRatingGuid = data;
	},
	// setCreditRatingLatestHash
	setCreditRatingRequestsHistory: (state, data) => {
		state.creditRatingRequestsHistory = data;
	},
	setPaymentCreditRatingFrameUrl: (state, data) => {
		state.paymentCreditRatingFrameUrl = data;
	},
	setPaymentCreditRatingAggregator: (state, data) => {
		state.paymentCreditRatingAggregator = data;
	},
	setLatestResponseGuid: (state, data) => {
		state.latestResponseGuid = data;
	},
    setLatestResponsePrice: (state, data) => {
		state.latestResponsePrice = data;
	},
    CLEAR_CR_DATA: (state) => {
        state = default_state();
    }
};

const actions = {
	getCreditRatingRequestsHistory: (store) => {
		return api.services.history()
			.then(({ data }) => {
				if (Array.isArray(data?.data) && data.data.length) {
					/*установить массив с историей*/
					store.commit('setCreditRatingRequestsHistory', data.data);
				}
			})
			.catch((error) => {

			});
	},
};

export default {
	state,
	getters,
	mutations,
	actions,
};
