const state = {
  address: [],
  email: [],
  loader: {
    b_day: false,
    email: false,
    passport: false,
    code_division: false,
    location: false,
    house: false
  }
};

const getters = {
  SUGGESTIONS_GET_EMAIL_LIST: state => state.email,
  SUGGESTIONS_GET_ADDRESS_LIST: state => state.address,
  SUGGESTIONS_LOADER: state => state.loader
};


const mutations = {
  SUGGESTIONS_SET_EMAIL_LIST: (state, list) => state.email = JSON.parse(list).suggestions,
  SUGGESTIONS_TRUNCATE_EMAIL: state => state.email = [],
  SUGGESTIONS_SET_ADDRESS_LIST: (state, list) => state.address = JSON.parse(list).suggestions,
  SUGGESTIONS_TRUNCATE_ADDRESS: state => state.address = [],
  SUGGESTIONS_SET_LOADER: (state, status) => state.loader = {...state.loader, ...status}
}

const actions = {
  SUGGESTIONS_GET_B_DAY: (store, data) => {
    const xhr = new XMLHttpRequest(),
      method = 'post',
      url = 'https://new.webbankir.com/api/user/verify';
    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify({bDay: data.bDay, firstName: data.firstName, lastName: data.lastName, middleName: data.middleName}));
    
    return xhr;
  },
  SUGGESTIONS_GET_EMAIL: (store, query) => {
    const xhr = new XMLHttpRequest(),
      method = 'post',
      url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/email';
    
    xhr.open(method, url, true);
    xhr.setRequestHeader('Authorization', 'Token 97ee570fe3d0b6a0548a35250c1f22e18f0bb24c');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify({
      query: query,
      count: 5
    }));
    
    return xhr;
  },
  SUGGESTIONS_GET_PASSPORT: (store, passport) => {
    const xhr = new XMLHttpRequest(),
      method = 'post',
      url = 'https://new.webbankir.com/api/user/verify';
    
    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify({passport: passport}));
    
    return xhr;
  },
  SUGGESTIONS_GET_CODE_DIVISION: (store, code) => {
    const xhr = new XMLHttpRequest(),
      method = 'get',
      url = `https://new.webbankir.com/api/passport-issuer/${code}`;
    
    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send();
    
    return xhr;
  },
  SUGGESTIONS_GET_ADDRESS: (store, query) => {
    const xhr = new XMLHttpRequest(),
      method = 'post',
      url = 'https://api.webbankir.com/v3/suggest/address';
    
    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify({query: query}));
    
    return xhr;
  },
}


export default {
  state,
  actions,
  getters,
  mutations
}
