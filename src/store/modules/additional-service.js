import backend from '@/utils/backend.js'
import store from '@/store';
/**
 * @type iBackend.api
 */
const api = backend.api;

/**
 * Возвращаем текущий роут из document.location
 * @returns {string}
 */
const current_page = () => {
    const location = document.location.pathname.split('/');
    return location[location.length-1];
}

/**
 * Возвращает дефолтовый стейт чекбокса с учетом аб-тестов.
 *
 * @param {iAdditionalService} service
 */
const default_state  = (service) => {
    /**
     * ABTest for insure (service id === 6)
     * Нужно только когда аб-тест включен.
     */
    if(service.id === 6 && store.getters.$config.ab.insure) {
        const page = current_page();
        switch (page) {
            case 'my-loan':
            case 'registration':
                const user_id = String(store.getters.$user.id);
                /**
                 * Если user_id кончается на 1 или 2, то галочка по условиям теста должна быть снята.
                 * В ином случае возвращаем значение по-умолчанию из конфига
                 */
                return [1, 2].includes(Number(user_id[user_id.length - 1])) ? false : store.getters.$config.defaultInsureState;
            default:
                return service.state;
        }
    }
    
    return service.state;
}

const state = {
    list: []
};

const getters = {
    ADDITIONAL_SERVICES_GET_LIST: state => state.list
};

const mutations = {
    ADDITIONAL_SERVICES_SET_LIST: (state, list) => state.list = list,
    ADDITIONAL_SERVICE_TOGGLE_EXPAND: (state, id) => {
        state.list = state.list.map((/**iAdditionalService**/service) => {
            if(service.id === id) {
                return {
                    ...service,
                    expand: !service.expand
                }
            }

            return service;
        })
    },
    ADDITIONAL_SERVICE_SET_STATE: (state, {selected, id}) => {
        state.list = state.list.map((/**iAdditionalService**/service) => {
            if(service.id === id) {
                return {
                    ...service,
                    selected
                }
            }
        
            return service;
        })
    },
    ADDITIONAL_SERVICE_TOGGLE: (state, id) => {
        state.list = state.list.map((/**iAdditionalService**/service) => {
            if(service.id === id) {
                return {
                    ...service,
                    selected: !service.selected
                }
            }

            return service;
        })

    },
    
    ADDITIONAL_SERVICE_FALSE: (state, id) => {
        state.list = state.list.map((/**iAdditionalService**/service) => {
            if(service.id === id) {
                return {
                    ...service,
                    selected: false
                }
            }

            return service;
        })

    },
};

const actions = {
    ADDITIONAL_SERVICES_LOAD_LIST: ({commit}) => {
        api.services.all().then(({data}) => {
            if(Array.isArray(data?.data)) {
                data.data.forEach((service) => {
                    service.expand = false;
                    service.selected = default_state(service);
                })
            }
            
            
            commit('ADDITIONAL_SERVICES_SET_LIST', data?.data);
        })
    }
}

export default {
    state, getters, mutations, actions
}
