// import { UNSET_USER, UNSET_AUTHORIZED, SET_USER } from '../mutation-types'

// Initial state
const state = {
  activeFocus: '',
  identificationDone: false,
  userLocation: {}
}

// Getters
const getters = {
  getActiveFocus: state => state.activeFocus,
  getIdentificationStatus: state => state.identificationDone,
  getUserLocation: state => state.userLocation
}

// Mutations
const mutations = {
  SET_ACTIVE_FOCUS (state, elRef) {
    state.activeFocus = elRef
  },
  SET_CREDIT_HISTORY_DONE (state, value) {
    state.identificationDone = value
  },
  SET_USER_LOCATION (state, value) {
    state.userLocation = value
  }
}

export default {
  state,
  getters,
  mutations
}
