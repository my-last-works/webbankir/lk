import {
  ACCOUNTS_GET,
  ACCOUNTS_SET_STATUS,
  ACCOUNTS_SET_PENDING,
  ACCOUNTS_SET_DELETING,
  ACCOUNTS_SET_ACCOUNTS,
  ACCOUNTS_SET_OK,
  ACCOUNTS_DELETE_ACCOUNT,
  ACCOUNTS_SET_FIRSTLOAD,
  ACCOUNTS_SET_PRIMARY,
  ACCOUNTS_SET_NAME,
  ACCOUNTS_ADD_ACCOUNT,
  SET_STATUS
} from '@/store/mutation-types'
import {getImgFilename} from '@/utils/mappings/accounts'
import * as api from '@/utils/api'

const state = {
  accounts: [],
  status: '',
  reinstatement: {
    status: false,
    phone: null
  }
}

const getters = {
  getAccountsStatus: (state) => state.status,
  getAccounts: (state) => state.accounts,
  getReinstatement: (state) => state.reinstatement
}

const mutations = {
  [ACCOUNTS_SET_PENDING]: (state) => { state.status = 'pending' },
  [ACCOUNTS_SET_STATUS]: (state, status) => { state.status = status },
  [ACCOUNTS_SET_DELETING]: (state) => { state.status = 'deleting' },
  [ACCOUNTS_SET_FIRSTLOAD]: (state) => { state.status = 'firstLoad' },
  [ACCOUNTS_SET_OK]: (state) => { state.status = 'ok' },
  [ACCOUNTS_SET_ACCOUNTS]: (state, accounts) => {
    state.status = 'ok'
    state.accounts = accounts.map(account => ({...account, iconSmall: `/lk/static/img/accounts/${getImgFilename(account.type, account.description)}`}))
  },
  [ACCOUNTS_ADD_ACCOUNT]: (state, account) => {
    state.accounts = [
      ...state.accounts,
      {...account, iconSmall: `/lk/static/img/accounts/${getImgFilename(account.type, account.description)}`}]
  },
  [ACCOUNTS_SET_PRIMARY]: (state, id) => {
    console.log('ACCOUNTS_SET_PRIMARY', id);
    console.log('accounts', state.accounts);
    state.accounts.forEach((card) => {
      console.log('card', card);
      return {...card,
        main: card.id === id
      }
    })
  },
  [ACCOUNTS_SET_NAME]: (state, {id, name}) => state.accounts = state.accounts.map(card => id === card.id ? {...card, name: name} : card),
  [ACCOUNTS_DELETE_ACCOUNT]: (state, {id}) => state.accounts = state.accounts.filter((account) => account.id !== id),
  REINSTATEMENT_BY_PHONE: (state, {status, phone}) => {
    state.reinstatement.status = status
    state.reinstatement.phone = phone
  }
}

const actions = {
  [ACCOUNTS_GET]: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit(SET_STATUS, 'load-accounts')
      api.getAccounts().then((response) => {
        console.log('ACCOUNTS_GET', response.data.data);
        commit(ACCOUNTS_SET_ACCOUNTS, response.data.data)
        commit(SET_STATUS, 'ok')
        resolve(response)
      }).catch(response => {
        commit(SET_STATUS, 'ok')
        console.log(response)
        reject(response)
      })
    })
  },
  [ACCOUNTS_DELETE_ACCOUNT]: ({commit, state}, account) => {
    return new Promise((resolve, reject) => {
      commit(ACCOUNTS_SET_PENDING)
      api.deleteAccount(account).then((response) => {
        commit(ACCOUNTS_DELETE_ACCOUNT, account)
        commit(ACCOUNTS_SET_OK)
        resolve(response)
      }).catch((error) => {
        commit(ACCOUNTS_SET_OK)
        reject(error)
      })
    })
  },
  [ACCOUNTS_SET_PRIMARY]: ({commit, state}, account) => {
    return new Promise((resolve, reject) => {
      commit(ACCOUNTS_SET_PENDING)
      commit(SET_STATUS, 'loading_accounts')
      api.updateAccount(account).then((response) => {
        commit(ACCOUNTS_SET_PRIMARY, account.id)
        commit('ACCOUNTS_SET_STATUS', 'primary-is-changed')
        commit(SET_STATUS, 'ok')
        resolve(response)
      }).catch((error) => {
        commit(ACCOUNTS_SET_OK)
        commit(SET_STATUS, 'ok')
        reject(error)
      })
    })
  },
  [ACCOUNTS_SET_NAME]: ({commit, state}, account) => {
    return new Promise((resolve, reject) => {
      api.updateAccount(account).then((response) => {
        commit(ACCOUNTS_SET_NAME, account)
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  createAccount: async ({commit, dispatch}, account) => {
    try {
      const {data} = await api.createAccounts(account)
      const newAccount = data.data[0]
      commit(ACCOUNTS_ADD_ACCOUNT, newAccount)
      await dispatch('LOAN_GET_ACCOUNTS');
      return newAccount
    } catch (e) {
      console.log(e)
      return Promise.reject(e)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
