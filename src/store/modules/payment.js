import {getLinksForPayMethods, getPayOrder} from '@/utils/api';
import {SET_PAYMENT_ORDER, SET_PAYMENT_METHODS} from '@/store/mutation-types';
import {loanTypeMapper} from '@/utils/mappings/loans';
import find from 'lodash/find';
import backend from '@/utils/backend';
import {paymentCheckType, paymentCheckAmount} from '@/store/validators/payment';
import store from '@/store';

/**
 * @type iBackend.api
 */
const api = backend.api;

const state = {
    paymentOrder: null,
    paymentMethods: null,
    paymentAmount: 0,
    paymentType: '',
    paymentValues: null,
    paymentAdditionalServices: null,
    selectedPaymentMethod: '',
    /**
     * Необходимо для оплаты доп.соглашения о пролонгации
     */
    paymentData: {
        dop: true,
        pos: true,
        pdl: true,
    },
    lastPaymentData: '',
    showPaymentForm: false,
    showPaymentFormHeight: 1521,
    process: false,
    aggregator: 'payler',
    tdsStep: 1,
    status: {
        installment: '',
    },
    paymentEventInit: false,
    additionalServices: [],
    bonusBalance: 0,
    bonusPaySum: 0,
    methods: [],
    paymentInProcess: false,
    lastSuccessPayment: {
        amount: 0,
        type: '',
        date: null,
    }
};

const getters = {
    getPaymentOrder: ({paymentOrder}) => paymentOrder,
    showPaymentOrderButton: ({paymentOrder}) => paymentOrder && paymentOrder.length > 1,
    getPaymentMethods: ({paymentMethods}) => (type) => find(paymentMethods, {type}),
    getPaymentOrderString: ({paymentOrder}) => paymentOrder && paymentOrder.map(contract => `${contract.type}_${contract.id}`).join(','),
    getPaymentAmount: state => parseFloat(parseFloat(state.paymentAmount).toFixed(2)),
    getPaymentType: state => state.paymentType,
    $pay: state => state,
    paymentJSON(state) {
        if (!Array.isArray(state.paymentOrder) || state.paymentOrder.length === 0) {
            console.warn('paymentOrder пустой или не является массивом');
        }
        
        if (parseInt(state.paymentAmount) < 10) {
            console.warn('Минимальная сумма оплаты должна быть равна или более 10 рублей, текущее значение: ' + parseInt(state.paymentOrder));
        }
        
        return {
            paymentData: state.paymentOrder,
            sum: state.paymentAmount,
            source: 1,
            user_id: store.getters.$user.id,
        };
    },
    additionalServicesPrice (state) {
        return state.additionalServices.filter((/*iAdditionalService*/ service) => {
            return service.selected && [3].includes(service.location_int);
        }).reduce( (a, /*iAdditionalService*/ service) => {
            return a + service.price
        }, 0);
    }
};

const mutations = {
    PAYMENT_SET_LAST_SUCCESS_PAYMENT(state, {amount, type}) {
        state.lastSuccessPayment = {
            amount, type,
            date: new Date(),
        };
    },
    PAYMENT_ADDITIONAL_SERVICES_SET_SELECTED(state, {id, selected}) {
        state.additionalServices = state.additionalServices.map((service) => {
            if (service.id === id) {
                return {
                    ...service,
                    selected,
                };
            }
            
            return service;
        });
    },
    PAYMENT_IN_PROCESS: (state, v) => {
        state.paymentInProcess = v;
    },
    PAYMENT_SET_AMOUNT: (state, amount) => {
        state.paymentAmount = amount;
    },
    
    PAYMENT_SET_TYPE: (state, type) => {
        paymentCheckType(type);
        state.paymentType = type;
    },
    
    PAYMENT_SET_ORDER: (state, order) => {
        state.paymentOrder = order;
    },
    
    PAYMENT_ADD_BONUS_SUM: (state, sum) => {
        state.bonusPaySum = sum;
    },
    
    PAYMENT_CLEAR_BONUS_SUM: (state) => {
        state.bonusPaySum = 0;
    },
    
    PAYMENT_SET_BONUS_BALANCE: (state, balance) => {
        state.bonusBalance = balance;
    },
    PAYMENT_SET_ADDITIONAL_SERVICES: (state, services) => {
        state.additionalServices = services.map((/*iAdditionalService*/service) => {
            return {
                ...service,
                selected: service.state,
            };
        });
    },
    PAYMENT_TRUNCATE_ADDITIONAL_SERVICES: (state) => {
        state.additionalServices = [];
    },
    
    'PAYMENT_EVENT_INIT': (state, value) => {
        state.paymentEventInit = value;
    },
    'SET_PAY_STATUS': (state, data) => {
        if (typeof state.status[data.type] === 'undefined') {
            console.warn(`Unknown ${data.type}`);
            return;
        }
        
        state.status[data.type] = data.status;
    },
    [SET_PAYMENT_ORDER]: (state, newOrder) => {
        state.paymentOrder = newOrder.map(order => {
            return {...order, typeText: loanTypeMapper(order.type)};
        });
    },
    [SET_PAYMENT_METHODS]: (state, newMethods) => state.paymentMethods = newMethods,
    'CLEAR_PAYMENT_DATA': (state) => state.paymentMethods = state.paymentOrder = null,
    SET_PAYMENT: (state, {amount, type, data, additionalServices}) => {
        if(type !== undefined) paymentCheckType(type);
        if(amount !== undefined) paymentCheckAmount(amount);
        
        if(amount !== undefined) state.paymentAmount = amount;
        if(type !== undefined) state.paymentType = type;
        if(data !== undefined) state.paymentValues = data;
        if(additionalServices !== undefined) state.paymentAdditionalServices = additionalServices;
        
    },
    
    /**
     * Выбор типа платежа при подписании пролонгации
     */
    SET_PAYMENT_DATA: (state, data) => {
        data.pdl = true;
        state.paymentData = data;
        console.log('SET_PAYMENT_DATA', data);
    },
    
    'SET_TDS_STEP'(state, step) {
        state.tdsStep = step;
    },
    
    PAYMENT_SET_METHODS(state, methods) {
        state.methods = methods;
    },
};

const actions = {
    PAYMENT_POST_CASHBACK (state, payload) {
        api.cashback.postBonus({
            sum: payload,
            paymentData: [
                {
                    "type": "1",
                    "number": "3855958"
                }
            ]
        })
        .then(() => {
            console.log(state)
            state.commit('PAYMENT_SET_BONUS_BALANCE', state.state.bonusBalance - payload);
        })
    },

    PAYMENT_GET_METHODS(store) {
        return api.payment.all().then(({data}) => {
            store.commit('PAYMENT_SET_METHODS', data?.data);
        });
    },
    
    PAYMENT_GET_ADDITIONAL_SERVICES(store) {
        return api.services.all().then(({data}) => {
            store.commit('PAYMENT_SET_ADDITIONAL_SERVICES', data.data);
        });
    },
    
    PAYMENT_GET_BONUS_BALANCE({commit}) {
        return api.cashback.balance().then(({data}) => {
            if (data?.data?.available_balance) {
                commit('PAYMENT_SET_BONUS_BALANCE', data.data.available_balance);
            }
        });
    },
    
    async getPaymentParams({commit}) {
        console.groupCollapsed('Store.payment::getPaymentParams');
        console.trace();
        console.groupEnd();
        const promises = [
            getPayOrder(),
            getLinksForPayMethods(),
        ];
        // console.log('promises', promises)
        const paymentParams = await Promise.all(promises);
        commit(SET_PAYMENT_ORDER, paymentParams[0]);
        commit(SET_PAYMENT_METHODS, paymentParams[1]);
    },
    
    createPayment(store, data) {
        console.log('createPayment', store, data);
        const paymentData = [];
        
        this.getters.getPaymentOrder.forEach((payment) => {
            paymentData.push({
                type: payment.type,
                number: payment.id,
            });
        });
        
        console.log('paymentData', paymentData);
        
    },
    
    getLinkToPayWithMainMethods({commit, state, getters}, params) {
        const {payMethod, sum} = params;
        const paymethodUrls = {
            card: 'payment/create/card',
            yandex: 'payment/create/yandex',
            sberbank: 'payment/create/sberbank',
        };
        
        let paymentData;
        if (state.paymentType === 'credit_rating') {
            paymentData = `5_${getters.$user.id}`;
        } else if (state.paymentType === 'AMNESTY_2019') {
            const loans = [];
            if (getters.$posServices) {
                loans.push(`3_${getters.$posServices.id}`);
            }
            loans.push(`2_${getters.$pdl.supplementary.amnesty2019.id}`);
            paymentData = loans.join(',');
        } else if (state.paymentType === 'installment_payment') {
            let schedule = getters.$installment.currentPaymentBySchedule;
            const loans = [];
            schedule.sales.forEach((sale) => {
                if (sale.selected) {
                    loans.push(`6_${sale.id}`);
                }
            });
            paymentData = loans.join(',');
        } else if (state.paymentType === 'installment_loan') {
            const loans = [];
            loans.push(`8_${getters.$pdl.id}`);
            paymentData = loans.join(',');
        } else if (params.type === 1) {
            const loans = [];
            paymentData = state.paymentOrder.map((product) => `${product.type}_${product.id}`).join(',');
            console.log('origin paymentData', paymentData);
            state.paymentOrder.forEach((product) => {
                //pdl
                if (product.type === 1 && state.paymentData.pdl) {
                    loans.push(`${product.type}_${product.id}`);
                }
                //dop
                if (product.type === 2 && state.paymentData.dop) {
                    loans.push(`${product.type}_${product.id}`);
                }
                //pos
                if (product.type === 3 && state.paymentData.pos) {
                    loans.push(`${product.type}_${product.id}`);
                }
            });
            
            if (!state.paymentData.dop) {
                const dop = state.paymentOrder.find((product) => {
                    return product.type === 2;
                });
                loans.push(`${dop.type}_${dop.id}`);
            }
            
            if (!state.paymentData.pos) {
                const pos = state.paymentOrder.find((product) => {
                    return product.type === 3;
                });
                loans.push(`${pos.type}_${pos.id}`);
            }
            
            paymentData = loans.join(',');
        } else if (params.type === 2) {
            paymentData = `${params.type}_${params.id}`;
        }
        let source;
        if (payMethod === 'card') {
            source = 4;
        }
        state.lastPaymentData = paymentData;
        
        // return api.payment.getPaymentSystem(paymethodUrls[payMethod], {
        //     sum,
        //     paymentData,
        //     source
        // })
        
        return api.payment.pay[payMethod]({
            sum,
            paymentData,
            source,
        });
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};
