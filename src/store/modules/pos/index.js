import api from '@/utils/backend';
import config from '@/config';
const BASE_URL = config.POS_BASE_URL;

const calculateDebt = (state) => {
	let percent = 0;
	let penalties = 0;
	
	state.schedule.filter(schedule => schedule.isOverdue).forEach((schedule) => {
		schedule.sales.forEach((/**iPosSale**/sale) => {
			percent += sale.percents;
			penalties += sale.penalties;
		})
	});
	
	const body = state.schedule.filter(schedule => schedule.isOverdue).reduce((a, /**iPosSchedule**/schedule) => {
		return a + schedule.sales.reduce((a, /**iPosSale**/sale) => {
			return a + sale.amount - sale.paid_amount
		}, 0)
	}, 0);
	
	return  percent + penalties + body;
};


const calculateCurrentPaySum  = (state) => {
	return state.schedule.find((s) => !s.isPaid && !s.isOverdue)?.amount;
};

/**
 * Создание ид договоров для текущего платежа
 * @param state
 */
const createCurrentPayNumbers = (state) => {
	const numbers = [];
	
	state.schedule.filter(schedule => schedule.isOverdue).forEach((schedule) => {
		schedule.sales.forEach((/**iPosSale**/sale) => {
			numbers.push(sale.id);
		})
	});
	
	return numbers;
};



const state = {
	schedule: [],
	loans: [],
	operations: [],
	rejections: [],
	transactions: [],
	partners: [],
	limit: 0,
	geo: {
		type: '',
		fias_id: '',
		location: {
			lat: 0,
			lon: 0,
		}
	},
};

const getters = {
	$POS: (state) => {
		return {
			debt: calculateDebt(state),
			currentPaySum: calculateCurrentPaySum(state),
			currentPayNumbers: createCurrentPayNumbers(state),
			...state
		}
	},
};

const mutations = {
	POS_SET_LIMIT: (state, limit) => state.limit = limit,
	POS_SET_SCHEDULE: (state, data) => {
		state.schedule = data.map((/**iPosSchedule**/schedule) => {
			const sales =  schedule.sales.map((sale) => {
				sale.selected = true;
				return sale;
			});
			
			const isPaid = schedule.amount <= schedule.sales.reduce((a, c) => {
				return a + c.paid_amount;
			}, 0);
			
			const isOverdue = !isPaid && new Date(schedule.date) < new Date();
			
			return {...schedule, sales, isPaid, isOverdue}
		}).sort((a, b) => {
			return new Date(a.date).getTime() - new Date(b.date).getTime();
		});
		
	},
	
	POS_SET_OPERATIONS: (state, data) => state.operations = data.map((operation) => {
		operation.isOperation = true;
		operation.isRejection = false;
		operation.isTransaction = false;
		return operation;
	}),
	POS_SET_REJECTIONS: (state, data) => state.rejections = data.map((rejection) => {
		rejection.isRejection = true;
		rejection.isOperation = false;
		rejection.isTransaction = false;
		return rejection;
	}),
	POS_SET_TRANSACTIONS: (state, data) => state.transactions = data.map((transaction) => {
		transaction.isTransaction = true;
		transaction.isOperation = false;
		transaction.isRejection = false;
		transaction.date = transaction.transaction_date;
		transaction.status = 'refill';
		return transaction;
	})
};


const actions = {
	
	POS_GET_OPERATION_DOCUMENT(store, id) {
		return api.getData(`v1/user/${store.getters.$user.id}/operation/${id}/contract`, BASE_URL, true).then(async (response) => {
			return await api.getDoc(response.data, `contract_${id}.pdf`);
		})
	},
	
	POS_RECOUNT_DEBT () {
		return new Promise((resolve, reject) => {
			setTimeout(() => resolve(2322), 2000);
		})
	},
	
	POS_CHECK_USER ({getters, commit}) {
		const user_id = getters.$user.id;
		
		return new Promise((resolve, reject) => {
			api.getData(`v2/user/${user_id}`, config.POS_BASE_URL).then(({data}) => {
				commit('POS_SET_LIMIT', data.data.current_limit);
				resolve(true);
			}).catch((e) => {
				resolve(false);
			})
		})
	},
	
	POS_GET_SCHEDULE ({getters, commit}) {
		const user_id = getters.$user.id;
		
		return new Promise((resolve, reject) => {
			api.getData(`v2/user/${user_id}/schedule`, config.POS_BASE_URL).then(({data}) => {
				commit('POS_SET_SCHEDULE', data.data);
				resolve(data.data);
			}).catch((e) => {
				reject(e);
			})
		})
	},
	
	POS_GET_OPERATIONS({getters, commit}) {
		const user_id = getters.$user.id;
		
		return new Promise((resolve, reject) => {
			api.getData(`v2/user/${user_id}/operation`, config.POS_BASE_URL).then(({data}) => {
				commit('POS_SET_OPERATIONS', data.data);
				resolve(data.data);
			}).catch((e) => {
				reject(e);
			});
		})
	},
	
	POS_GET_REJECTIONS({getters, commit}) {
		const user_id = getters.$user.id;
		
		return new Promise((resolve, reject) => {
			api.getData(`v1/user/${user_id}/rejection`, config.POS_BASE_URL).then(({data}) => {
				commit('POS_SET_REJECTIONS', data.data);
				resolve(data.data);
			}).catch((e) => {
				reject(e);
			});
		})
	},
	
	
	POS_GET_TRANSACTIONS({commit}) {
		return new Promise((resolve, reject) =>{
			api.api.transactions.history.pos().then(({data}) => {
				if(data.transactions) {
					commit('POS_SET_TRANSACTIONS', data.transactions);
					resolve(data.transactions);
				} else {
					resolve([]);
				}
			}).catch((e) => {
				reject(e);
			})
		})
	}
	
};


export default {
	state,
	getters,
	actions,
	mutations,
}
