import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import user from './modules/user'
import accounts from './modules/accounts'
import docs from './modules/docs'
import loan from './modules/loan'
import interfaceStore from './modules/interface'
import creditRating from './modules/credit-rating'
import * as types from './mutation-types'
import registration from './modules/registration'
import payment from './modules/payment'
import installment from './modules/installment/'
import service from './modules/service';
import suggestions from './modules/suggestions';
import AdditionalService from './modules/additional-service';
import Notifications from './modules/notifications';
import POS from './modules/pos';
import VuexPersistence from 'vuex-persist'
import omit from 'lodash/omit'
import get from 'lodash/get'


Vue.use(Vuex);
const vuexLocal = new VuexPersistence({
    storage: (window.localStorage.getItem('secure_login')) ? window.sessionStorage : window.localStorage,
    modules: ['user'],

    reducer: (state) => omit(state, 'payment', 'service', 'Notifications',
        'installment.partnerShops', 'installment.operations',
        'installment.schedule', 'installment.payment', 'installment.limit',
        'authorizedUser', 'interface', 'suggestions',
        'cfg', 'config', 'loans', 'loan', 'accounts', 'docs', 'clearSession', 'errors', 'errors500', 'user.messageAuxiliaryWindows',
        'interfaceModule', 'registration', 'status', 'user.errorText', 'user.authorized', 'user.user.creditHistory',
        'user.isPersonalDataValid', 'user.user.id', 'user.user.id', 'user.socialNetworks', 'user.user.login', 'user.user.final', 'user.messageWarningWindows', 'errorMessagesWithChatHelp', 'calculation', 'userDataIsLoading', 'authorizedUser.buttonActionLoaderClassProfile', 'authorizedUser.buttonSaveProfile', 'creditRating.creditRatingRequestsHistory',
        'displayWarningBlock' // блок справа - ! внимание
    )
})


export default new Vuex.Store({
    modules: {
        'interface': interfaceStore,
        auth,
        user,
        accounts,
        docs,
        registration,
        payment,
        loan,
        installment,
		    creditRating,
        service,
        suggestions,
        AdditionalService,
        Notifications,
        POS,
    },
    state: {
        // errors: [{name: 'birth_city', Message: 'something'}],
        errors: [],
        errors500: [],
        clearSession: false,
        status: 'ok',
        errorMessagesWithChatHelp: {
            'smsLimit': 'Не получили код подтверждения в SMS?',
            'changePhoneError': 'Превышен лимит запросов. Помощь онлайн'
        },
        calculation: {},
        userDataIsLoading: false,
        displayWarningBlock: true,
        cfg: { // конфигурация
            /**
             * Управление токенизацией карты при добавлении
             *
             * 0 - отключена везде
             * 1 - включена на регистрации (для инстоллмента, если typeLoan === 8)
             * 2 - на регистрации для пдл и инстоллмента
             * 3 - включена всегда и везде
             * **/
            newCard: 3,
            legalEnabled: true, // вкл/выкл юр.услуги
            serviceModalEnabled: false,
            /**
             * Если null, то высчитывается автоматически.
             * Если boolean, то включено/выключено
             * @deprecated
             */
            defaultInsureState: true,
            /**
             * Если null, то высчитывается автоматически.
             * Если boolean, то включено/выключено
             * @deprecated
             */
            defaultLegalState: false,
            /**
             * @deprecated
             */
            promoCodeEnabled: false, // включитб/выключить поле промкода
            /**
             * A/B
             */
            ab: {
                /**
                 * страховка
                 */
                insure: true,
                /**
                 * юр.услуги
                 */
                legal: false,

				serviceBlock: {
                	enable: true,
					ids: [0, 9]
				},
            },
            promoDusterEnable: true,
            tdsUseModal: false,
            posEnable: ['seb-web.ru', 'localhost'].includes(document.location.hostname),
            creditRatingEnable: ['seb-web.ru', 'localhost'].includes(document.location.hostname),
            smsInfo: {
                prechecked: true,
                enabled: true,
            }
        }
    },
    mutations: {
        'setUserDataIsLoading': (state, data) => (state.userDataIsLoading = data),
        'setDisplayWarningBlock': (state, data) => (state.displayWarningBlock = data),
        [types.SET_ERRORS]: (state, errors) => {
            state.errors = errors
        },
        [types.ADD_ERROR]: (state, error) => {
            state.errors.push(error)
        },
        [types.SET_ERRORS500]: (state, errors, mutations) => {
            // disableScroll()
            // state.status = 'ok'
            state.errors500 = errors
        },
        [types.UNSET_ERRORS]: (state) => {
            // enableScroll()
            state.errors = []
            state.errors500 = []
        },
        [types.SET_SESSION]: (state) => {
            state.clearSession = true
        },
        [types.UNSET_SESSION]: (state) => {
            state.clearSession = false
        },
        [types.SET_STATUS]: (state, status) => {
            state.status = status
        }
    },
    actions: {},
    getters: {
        $config: state => state.cfg,
        getConfig: state => state.cfg,
        getDisplayWarningBlock: state => state.displayWarningBlock,
        getUserDataIsLoading: state => state.userDataIsLoading,
        getCalculation: state => state.calculation,
        gerErrorMessages: state => state.errorMessages,
        getErrors: state => state.errors,
        getErrors500: state => state.errors500,
        getStatus: state => state.status,
        blockViewEdit: (state, getters) => get(getters.getActiveLoanPdl, 'mappedStatus.statusGroup', null) === 'wait' // Вернет true только если есть активный займ в ожидании
    },
    plugins: [vuexLocal.plugin]
})
