const AnimateScroll = require('animated-scroll-to');
/**
 * Функция-помощник для проверки всех значений объекта на заданное или не пустое значение
 *
 * @param object
 * @param value
 * @returns {boolean}
 */
const every = (object, value) => {
    return Object.keys(object)
        .every((key) => {
            if (value !== undefined) {
                return object[key] === value;
            } else {
                return object[key];
            }
        });
};

const focus = () => {
    try {
        const errors = [].slice.call(document.querySelectorAll('p.red'))
            .filter((item) => item.style.display !== 'none');
        if (errors.length === 0) {
            console.info('no errors found');
            return;
        }
        let block = errors[0].closest('.column, .columns');
        if (!block) return;

        if (block.querySelector('.multiselect.error')) {
            scroll(block.querySelector('.multiselect.error'));
        } else if (block.querySelector('input.error')) {
            block.querySelector('input.error')
                .focus();
        } else {
            scroll(block.querySelector('input.error'));
        }
    } catch (e) {
        console.warn('Field focus error: ' + e.message);
    }
};
/**
 * Множественное число в правильном падеже
 *
 * @example plural(1, ['яблоко', 'яблока', 'яблок']) => яблоко
 * @example plural(157, ['яблоко', 'яблока', 'яблок'])
 * @example plural(1, ['*секунду', 'секунды', 'секунд'])
 * @example plural(2, ['секунду', '*секунды', 'секунд'])
 * @example plural(5, ['секунду', 'секунды', '*секунд'])
 * @param n
 * @param cases
 * @returns {*}
 * @url http://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html?id=l10n/pluralforms
 */
const plural = (n, cases) => {
    return cases[(n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2)];
};

const isEmpty = (v) => {
    return !v;
};

let canScroll = true;
const scroll = (el, options) => {
    console.log('Scroll:', options);
    if (!canScroll) {
        console.log('%cScroll busy', 'font-weight: bold');
        return Promise.resolve();
    }

    if (!el) {
        console.log('%cScroll error: el is undefined', 'font-weight: bold;color: orange');
        return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
        try {
            canScroll = false;
            AnimateScroll(el, {
                onComplete: function () {
                    canScroll = true;
                    resolve();
                }
            });
        } catch (e) {
            canScroll = true;
            reject();
        }
    });
};

const noscroll = (state) => {
    if (state) {
        document.querySelector('body').style.overflow = 'hidden';
    } else {
        document.querySelector('body').style.overflow = '';
    }
};

const formatMoney = function (n, x) {
    const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return n.toFixed(Math.max(0, ~~n))
        .replace(new RegExp(re, 'g'), '$& ');
};

export const ruMonthName = (now) => {
    let months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][now - 1];
    return months;
};


export default {
    every,
    plural,
    isEmpty,
    focus,
    scroll,
    noscroll,
    formatMoney,
    ruMonthName
};
