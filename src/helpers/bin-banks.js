export const nameBank = (bin) => {
  
  bin = bin.replace('-', '').slice(0,6) // обрезаем номер карты и проверяем на условие
  
  if (['427638', '605461', '427683', '63900', '67758', '427901', '54693', '427644', '427601', '427631'].includes(bin)) {
    return {id: 'sberbank', name: 'Сбербанк', size: '0 0 125 30'};
  } else if (['434146', '405870', '544573', '532301'].includes(bin)) {
    return {id: 'otkritie', name: 'Открытие', size: '0 0 125 30'};
  } else if (['548999', '526483'].includes(bin)) {
    return {id: 'gazprombank', name: 'Газпромбанк', size: '0 0 125 30'};
  } else if (['427229', '46223', '527883', '447520'].includes(bin)) {
    return {id: 'vtb', name: 'ВТБ 24', size: '-10 0 90 30'};
  } else if (['521324', '437773', '553691'].includes(bin)) {
    return {id: 'tinkoff', name: 'Тинькофф', size: '-10 0 100 30'};
  } else if (['462730', '526483'].includes(bin)) {
    return {id: 'raiffeisen', name: 'Райффайзенбанк', size: '0 0 125 30'};
  } else if (['521178', '627119', '676248', '676230', '548673', '548601', '45841', '415428', '676371', '477964', '552175'].includes(bin)) {
    return {id: 'alfabank', name: 'Альфа-банк', size: '-10 0 125 30'};
  } else if (['676967', '443222', '443223', '443271', '443272', '443273', '443274', '471358'].includes(bin)) {
    return {id: 'mkb', name: 'МКБ', size: '0 0 163 30'};
  } else if (['405991', '405992', '405993'].includes(bin)) {
    return {id: 'pochtabank', name: 'Почта-банк', size: '0 0 70 30'};
  } else if (['418384', '418385', '418386', '418387', '418388', '422608', '435986', '436100'].includes(bin)) {
    return {id: 'rosselhozbank', name: 'РоссельхозБанк', size: '-15 0 125 30'};
  } else {
    return {id: 'unknownbank', name: '', size: '-35 0 101 30'};
  }
  // *Свойство size предназначено для иконок с постфиксом '-full' на конце (пример: vtb-full)
}
