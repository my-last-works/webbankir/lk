/* eslint-disable */
import dayjs from 'dayjs'

export const createDate = (date) => {
  let d1
  const regex_format = /^(0[1-9]|[1-2][1-9]|3[0-1])\.(0[1-9]|1[0-2])\.(\d{4})$/
  if (!Object.prototype.toString.call(date) === '[object Date]') {
    if (!date || !regex_format.test(date)) {
      console.warn('$date.before argument 0 error: Invalid date format')
      return
    } else {
      const _date = date.match(regex_format)
      d1 = dayjs(new Date(_date[3], _date[2], _date[1]))
    }
  } else {
    d1 = dayjs(date)
  }

  return d1
}

const dateCompare = (date, beforeDate) => {
  let d1 = createDate(date)
  let d2 = createDate(beforeDate)
  return dayjs(d1).isBefore(d2)
}

export const before = (date, beforeDate) => {
  return dateCompare(date, beforeDate)
}

export const after = (date, afterDate) => {
  return !dateCompare(date, afterDate)
}

export const diff = (date, unit = 'days') => {
   return dayjs().diff(createDate(date), unit)
}


