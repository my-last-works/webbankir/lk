import backend from '../utils/backend'

const send = (data) => {
    backend.putData('errorlog-front', data, 'https://new.webbankir.com/')
}

const meta = () =>  {
    const storage = localStorage.getItem('vuex');
    let login, user_id, phone, user_name, user_bdate;
    
    if (storage) {
        try {
            const vuex = JSON.parse(storage);
            login = vuex.user.login;
            user_id = vuex.user.id;
            phone = vuex.user.mobilePhone;
            user_name = `${vuex.user.firstName} ${vuex.user.middleName} ${vuex.user.lastName}`;
            user_bdate = vuex.user.bDay;
        } catch (e) {}
    }
    
    return {
        screen: {
            x: screen.width,
            y: screen.height
        },
        browser: navigator.userAgent,
        platform: navigator.platform,
        host: document.location.hostname,
        path: document.location.href,
        login,
        user_id,
        phone,
        user_name,
        user_bdate
    }
}

export default {
    send,
    meta
}