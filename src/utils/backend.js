const axios = require('axios');

const meta = () =>  {
    const storage = localStorage.getItem('vuex');
    let login, user_id, phone, user_name, user_bdate;

    if (storage) {
        try {
            const vuex = JSON.parse(storage);
            login = vuex.user.login;
            user_id = vuex.user.id;
            phone = vuex.user.mobilePhone;
            user_name = `${vuex.user.firstName} ${vuex.user.middleName} ${vuex.user.lastName}`;
            user_bdate = vuex.user.bDay;
        } catch (e) {}
    }

    return {
        screen: {
            x: screen.width,
            y: screen.height
        },
        browser: navigator.userAgent,
        platform: navigator.platform,
        host: document.location.hostname,
        path: document.location.href,
        login,
        user_id,
        phone,
        user_name,
        user_bdate
    }
};

const api = {
    /**
     * Метод получения похожих или исправленных адресов с дадаты
     * @param query
     * @returns {object}
     */
    email(query, count = 5) {
        return axios({
            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/email',
            method: 'post',
            headers: {
                'Authorization': 'Token 97ee570fe3d0b6a0548a35250c1f22e18f0bb24c'
            },
            data: {
                query,
                count
            }
        }).then((response) => {
            return Promise.resolve(response.data)
        })
    },


    log(options, m = true) {

        const data = {
            lk_log: 'stats',
            options
        };

        if(m) {
            data.meta = meta();
        }

        if (typeof APP !== "undefined") {
            console.log('Report: ', data);
            return;
        }
        return axios({
            url: 'https://new.webbankir.com/errorlog-front',
            method: 'put',
            data
        });
    },

    ABTestLog(data) {
        if (typeof APP !== "undefined") {
            console.groupCollapsed('Report');
            console.log('Object:', data);
            console.log('JSON:', JSON.stringify(data));
            console.groupEnd();
            return;
        }
        return axios({
            url: 'https://new.webbankir.com/errorlog-front',
            method: 'put',
            data
        });
    }
}
const api2 = require('./backend-api').default;
const a = {...api2, ...api};
/** @type iBackend **/
export default a // методы в этом файле имеют приоритет над старой реализацией апи
