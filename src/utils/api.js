import backend from '@/utils/backend'

/**
 * @type iBackend.api
 */
const api = backend.api;

export const getLoans = () => api.loan.all()

// *********** ACCOUNTS ************ //
export const deleteAccount = (account) => api.accounts.delete(account.id)

export const updateAccount = (account) => api.accounts.update(account)

export const getAccounts = (account) => api.accounts.get()

export const createAccounts = (account) => api.accounts.create(account)

// *********** PAYMENT ************ //

export const getLinksForPayMethods = () => api.payment.all().then(({data}) => data.data)

export const getPayOrder = () => api.payment.order().then(({data}) => data.data)

// *********** LOANS ************ //
export const getLoanById = (loanId) => api.documents.byLoan(loanId).then(({data}) => data.data)


export const resendSMSLoan = () => api.loan.sign.resend().then(({data}) => data.data)

// *********** SUGGEST ************ //

export const suggestAddress = (address) => api.suggestions.address(address)

// *********** RECOVER PASSWORD ************ //

export const recoverPassword = (newPassword, hash) => api.user.password.recovery({
  password: newPassword,
  emailHash: hash
});

// *********** CHANGE PASSWORD ************ //

export const changePassword = (password, oldPassword) => api.user.password.change({password, oldPassword})
  .then(({data}) => data.data)

// *********** STATEMENTS ************ //

export const getStatementsByLoanId = (loanId) => api.documents.statement(loanId).then(({data}) => data.Data)
export const getStatementsByLoanIdAndType = (loanId, type) => api.documents.statement(loanId, type)
export const putStatementsByLoanIdAndType = (loanId, type) => api.documents.createStatement(loanId, type)
