export const errorChangeEmail = (err) => {
  switch (err) {
    case 'emailEqualToCurrent':
      return 'Указанный e-mail уже используется в системе'
    case 'userEmailNotUnique':
      return 'Указанный e-mail уже используется в системе'
    case 'userHaveActiveLoan':
      return 'На момент рассмотрения заявки на займ, изменение email запрещено'
    case 'emailAddressInvalidFormat':
      return 'Email не соответствует формату nickname@hostname.com'
    case 'emailDomainInBlackList':
      return 'Данный email находиться в черном списке'
    case 'userNotHaveLoans':
      return 'У вас не было ни одной заявки на займ. Операция для смены email не доступна'
    case 'emailAddressInvalidHostname':
      return 'Данный тип email является не валидным для использования'
    case 'emailAddressInvalidMxRecord':
      return 'Данный тип значения после @ является не валидным для использования'
    case 'notAllRequiredFieldSet':
      return 'Поле Email было заполнено не корректно'
    case 'emailAddressInvalidSegment':
      return 'Возможно вы ошиблись при вводе E-Mail. Невозможно отправить письмо на ваш адресс'
    default:
      return 'Произошла непредвиденная ошибка'
  }
}
