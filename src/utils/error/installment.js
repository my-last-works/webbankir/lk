export const errorInstallment = (err) => {
  switch (err) {
    case 'smsCodeIsEmpty':
      return 'Ошибка! СМС-код был отправлен пустым.'
    case 'smsCodeInvalidFormat':
      return 'СМС-код содержит что-то кроме цифр. Код должен состоять из 4-х цифр'
    case 'mobilePhoneDoNotHaveActiveSmsCode':
      return 'На данный момент сервис не доступен, попробуйте повторить запрос позднее'
    case 'mobilePhoneBlocked':
      return 'Превышен лимит попыток ввода СМС-кода'
    case 'smsCodeInvalid':
      return 'СМС-код введен не корректно'
    case 'notAvailableForSign':
      return 'У вас нет доступных рассрочек для подписания'
    case 'notHaveUnsignedSupplementary':
      return 'У вас нет рассрочки, ожидающей подписания'
    case 'spamProtectionRestriction':
      return 'Привышено лимит запросов отправки СМС-кода'
  }
}
