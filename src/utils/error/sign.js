export const errorSign = (err, commit) => {
  switch (err) {
    case 'codeIsNotCorrect':
      let message = 'Вы ввели неверный СМС код'
      commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      // commit('CALL_WARNING_WINDOW', Message)
      break
    case 'userDataNotFound':
      message = 'Ошибка. Данные о пользователи не обнаружены. Попробуйте повторить запрос позже или обратитесь в тех-поддержку'
      commit('CALL_WARNING_WINDOW', message)
      break
    case 'activeLoanNotFound':
      message = 'Активный займ не обнаружен'
      commit('CALL_WARNING_WINDOW', message)
      break
    case 'codeNotFound':
      message = 'СМС код не был введен в поле для СМС'
      commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      // commit('CALL_WARNING_WINDOW', Message)
      break
    case 'exceededLimitAttempts':
      message = 'Превышен лимит запросов отправки СМС для подтверждения'
      commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      // commit('CALL_WARNING_WINDOW', Message)
      break
    case 'mobilePhoneNotFound':
      message = 'Номер телефона не обнаружен в базе данных'
      commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      // commit('CALL_WARNING_WINDOW', Message)
      break
    case 'loanIdNotFound':
      message = 'Данный займ вы не можете подписать'
      commit('CALL_WARNING_WINDOW', message)
      break
  }
}
