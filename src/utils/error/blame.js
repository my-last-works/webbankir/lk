const dateFrom = new Date(new Date().setFullYear(new Date().getFullYear() - 1)).toLocaleString('ru-RU').replace(/,.*/, '')
const dateTo = new Date().toLocaleString('ru-RU')

export const errorBlame = (err, store) => {
  switch (err) {
    case 'limitComplaint':
      store.commit('CALL_WARNING_WINDOW', 'Повторную жалобу можно оставить через 5 минут!')
      break
    case 'descriptionTooShort':
      var message = 'В поле Подробности должно быть содержаться минимум 10 символов'
      store.commit('SET_ERRORS', [{field: 'подробности', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'descriptionTooLong':
      message = 'В поле Подробности превышен лимит вводимых символов. Максимальное количество может быть 600 символов.'
      store.commit('SET_ERRORS', [{field: 'подробности', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'dateFalseFormat':
      message = 'Ошибка. Указан неверный формат времени. Должно быть ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'когда велся диалог', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'dialogueDateInvalidDateFormat':
      message = 'Формат времени указан не верно. Должно быть ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'когда велся диалог', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'datePeriodError':
      message = `Формат времени указан не верно. Указанная жалоба может быть ${dateFrom} и ${dateTo}`
      store.commit('SET_ERRORS', [{field: 'когда велся диалог', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'complaintTypeNotFound':
      message = 'Передан несуществующий тип парметра жалобы'
      store.commit('SET_ERRORS', [{field: 'Хочу пожаловаться на', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
