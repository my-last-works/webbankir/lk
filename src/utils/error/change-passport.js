export const errorChangePassport = (err, store) => {
  switch (err) {
    case 'passwordMismatching':
      let message = 'Неверно введен текущий пароль'
      store.commit('SET_ERRORS', [{field: 'Текущий пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'passwordTooShort':
      message = 'Новый пароль должен состоять минимум из 7 символов'
      store.commit('SET_ERRORS', [{field: 'Пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'passwordInvalidFormat':
      message = 'Внимание, пароль должен содержать минимум 7 символов, включающих: цифры, БОЛЬШИЕ и маленькие латинские буквы'
      store.commit('SET_ERRORS', [{field: 'Пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'passwordWrongSymbols':
      message = 'Новый пароль не должен содержать в себе кириллицу, пробелы, точки, запятые и другие спецсимволы'
      store.commit('SET_ERRORS', [{field: 'Пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
