export const getError = (error) => {
  let message = '';
  switch (error) {
    case 'codeIsNotCorrect':
      message = 'Вы ввели неверный СМС код';
      return message;
    case 'userDataNotFound':
      message = 'Ошибка. Данные о пользователи не обнаружены. Попробуйте повторить запрос позже или обратитесь в тех-поддержку';
      return message;
    case 'activeLoanNotFound':
      message = 'Активный займ не обнаружен';
      return message;
    case 'codeNotFound':
      message = 'СМС код не был введен в поле для СМС';
      return message;
    case 'exceededLimitAttempts':
      message = 'Превышен лимит запросов отправки СМС для подтверждения';
      return message;
    case 'mobilePhoneNotFound':
      message = 'Номер телефона не обнаружен в базе данных';
      return message;
    case 'loanIdNotFound':
      message = 'Данный займ вы не можете подписать';
      return message;
    case 'doNotHaveActiveLoan':
      message = 'У вас нет активных займов';
      return message;
    case 'notAllowed':
      message = 'Вам недоступна амнистия';
      return message;
    case 'notHaveUnsignedSupplementary':
      message = 'У вас нет амнистии, ожидающей подписания';
      return message;
    case 'spamProtectionRestriction':
      message = 'Превышен лимит запросов';
      return message;
    case 'smsCodeIsEmpty':
      message = 'Не передан смс-код';
      return message;
    case 'smsCodeInvalidFormat':
      message = 'СМС-код должен содержать 4 цифры';
      return message;
    case 'mobilePhoneDoNotHaveActiveSmsCode':
      message = 'Срок действия смс-кода истек. Попробуйте запросить код еще раз';
      return message;
    case 'mobilePhoneBlocked':
      message = 'Вы превысили количество попыток ввода. Повторите попытку через 1 час';
      return message;
    case 'smsCodeInvalid':
      message = 'Введенный код не совпадает с высланным вам';
      return message;
    case 'notAvailableForSign':
      message = 'Нет доступных договоров для подписания';
      return message;
    case 'emptyCodeInDB':
      message = 'Код для подписания не найден. Запросите код повторно';
      return message;
    default:
      message = 'Ошибка с сервера. Повторите Вашу попытку позднее';
      return message;
  }
};
