export const errorRestore = (err, store) => {
  switch (err) {
    case 'restoreUserNotFound':
      let message = 'Данный пользователь не был найден'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'userIdIncorrect':
      message = 'Данный пользователь не был найден'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'userNotHaveLoans':
      message = 'Восстановление пароля для данного пользователя не возможна'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'userNotFound':
      message = 'Данный пользователь не был найден'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLoginIsEmpty':
      store.commit('SET_ERRORS', [{field: 'электронной почты / логин / номер телефона', message: message}])
      message = 'Поле электронной почты / логин / номер телефона было передано пустым. Необходимо заполнить его и нажать кнопку Восстановить пароль'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLoginInvalidFormat':
      message = 'Поле не является ни электронной почты, ни логином, ни номером телефона'
      store.commit('SET_ERRORS', [{field: 'электронной почты / логин / номер телефона', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLastNameIsEmpty':
      message = 'В поле фамилия не была указана. Необходимо заполнить его и нажать кнопку Восстановить пароль'
      store.commit('SET_ERRORS', [{field: 'фамилия, на которую зарегистрирован аккаунт', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLastNameTooShort':
      message = 'В поле фамилия должно быть минимум 2 символа'
      store.commit('SET_ERRORS', [{field: 'фамилия, на которую зарегистрирован аккаунт', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLastNameTooLong':
      message = 'В поле фамилия должно быть не более 50 символов'
      store.commit('SET_ERRORS', [{field: 'фамилия, на которую зарегистрирован аккаунт', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreLastNameInvalidFormat':
      message = 'В поле фамилия указаны недопустимые символы'
      store.commit('SET_ERRORS', [{field: 'фамилия, на которую зарегистрирован аккаунт', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'chosenMethodNotAvailableForUser':
      message = 'Восстановление пароля для вас на данный момент не доступно. Попробуйте повторить запрос позднее'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'restoreSmsCodeInvalidFormat':
      message = 'В поле для восстановления пароля должны быть только цифры'
      store.commit('SET_ERRORS', [{field: 'код СМС', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'smsCodeInvalid':
      message = 'СМС код введен не корректно'
      store.commit('SET_ERRORS', [{field: 'код СМС', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'mobilePhoneDoNotHaveActiveSmsCode':
      message = 'По техническим причинам СМС-код вам не был отослан. Попробуйте повторить запрос позднее. Приносим извинения за доставленные неудобства'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'spamProtectionRestriction':
      message = 'Превышен лимит запрос'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'mobilePhoneBlocked':
      message = 'Превышено максимальное число попыток ввода кода из СМС'
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
