export const errorPayment = (err, store) => {
  switch (err) {
    case 'codeIsNotCorrect':
      let message = 'Вы ввели неверный СМС код'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'createPaymentOrderError':
      message = 'Ошибка при подтверждение займа. Попробуйте повторить запрос позже или обратитесь в тех-поддержку'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'maxNumberAttempts':
      message = 'Количество попыток было превышено!'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'paymentOrderIdError':
      message = 'Ошибка в платежном поручении. Обратитесь в тех-поддержку'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'paymentCheckDataError':
      message = 'Неверно были указананы платежных данные. Необходимо исправить и повторить запрос.'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'paymentDataError':
      message = 'Ошибка в платежных данных. Необходимо исправить и повторить запрос.'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'notEnoughParameters':
      message = 'Вы ввели неверный СМС код'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'headerUserIdIsEmpty':
      message = 'В поле Подробности превышен лимит вводимых символов. Максимальное количество может быть 600 символов.'
      store.commit('SET_ERRORS', [{field: 'подробности', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'decodeJsonCard':
      message = 'Ошибка. Указан неверный формат времени. Должно быть ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'когда велся диалог', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'userIdNotFound':
      message = 'Вы ввели неверный СМС код'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'emptyHashedData':
      message = 'В поле Подробности превышен лимит вводимых символов. Максимальное количество может быть 600 символов.'
      store.commit('SET_ERRORS', [{field: 'подробности', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'JwtTokenGenerationFailed':
      message = 'Ошибка. Указан неверный формат времени. Должно быть ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'когда велся диалог', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
