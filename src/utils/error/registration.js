export const errorRegistration = (err, store) => {
  switch (err) {
    case 'restoreUserNotFound':
      let message = 'Данный пользователь не был найден'
      store.commit('SET_ERRORS', [{field: 'restoreForm', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'userNotFound':
      message = 'Адрес электронной почты не найден или неправильно введена фамилия'
      store.commit('SET_ERRORS', [{field: 'Логин', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'restoreLoginInvalidFormat':
      message = 'Неправильный логин или пароль'
      store.commit('SET_ERRORS', [{field: 'f-email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'loginInvalidFormat':
      message = 'Неправильный логин или пароль'
      store.commit('SET_ERRORS', [{field: 'Логин', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'snilsTooShort':
      message = 'Снилс указан не полностью'
      store.commit('SET_ERRORS', [{field: 'snils', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'loginIsEmpty':
      message = 'Не заполнено поле МОБИЛЬНЫЙ ТЕЛЕФОН, ЛОГИН ИЛИ ПОЧТА'
      store.commit('SET_ERRORS', [{field: 'Логин', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneNotUnique':
      message = 'Указанный вами номер уже зарегистрирован'
      store.commit('SET_ERRORS', [{field: 'мобильный телефон', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'restoreLoginIsEmpty':
      message = 'Не заполнено поле МОБИЛЬНЫЙ ТЕЛЕФОН, ЛОГИН ИЛИ ПОЧТА'
      store.commit('SET_ERRORS', [{field: 'f-email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'restoreLastNameIsEmpty':
      message = 'Не заполнено поле ФАМИЛИЯ'
      store.commit('SET_ERRORS', [{field: 'f-email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passwordIsEmpty':
      message = 'Не заполнено поле ПАРОЛЬ'
      store.commit('SET_ERRORS', [{field: 'Пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'loginOrPasswordMismatching':
      message = 'Неправильный логин или пароль'
      store.commit('SET_ERRORS', [{field: 'Пароль', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'tokenIsEmpty':
      message = 'Передан пустой token'
      store.commit('SET_ERRORS', [{field: 'Пользователь', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'tokenIncorrect':
      message = 'Передан некорректный token'
      store.commit('SET_ERRORS', [{field: 'Пользователь', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneIsEmpty':
      message = 'Пустое поле телефона'
      store.commit('SET_ERRORS', [{field: 'мобильный телефон', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneInvalidFormat':
      message = 'Номер мобильного телефона должен быть записан 79ХХХХХХХХХ'
      store.commit('SET_ERRORS', [{field: 'мобильный телефон', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneNotUniq':
      message = 'Текущий номер телефона уже имеет учетную запись'
      store.commit('SET_ERRORS', [{field: 'мобильный телефон', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'spamProtectionRestriction':
      message = 'Превышен лимит запросов. За помощью обратитесь в онлайн-чат'
      store.commit('SET_ERRORS', [{field: 'мобильный телефон', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'emailAddressInvalidSegment':
      message = 'Указанный вами почтовый ящик является не корректным'
      store.commit('SET_ERRORS', [{field: 'email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'emailAddressInvalidFormat':
      message = 'Поле не соответствует формату example@mail.ru'
      store.commit('SET_ERRORS', [{field: 'email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'emailAddressInvalidHostname':
      message = 'После @ формат указан неверно'
      store.commit('SET_ERRORS', [{field: 'email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'emailAddressInvalidMxRecord':
      message = 'Данный email ранее не был создан'
      store.commit('SET_ERRORS', [{field: 'email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'emailDomainInBlackList':
      message = 'Email находиться в черном списке'
      store.commit('SET_ERRORS', [{field: 'email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'lastNameTooShort':
      message = 'Поле фамилия содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'lastName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'restoreLastNameTooShort':
      message = 'Поле фамилия содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'f-fio', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'lastNameTooLong':
      message = 'Поле фамилия содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'lastName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'lastNameInvalidFormat':
      message = 'Поле фамилия содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'lastName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'restoreLastNameInvalidFormat':
      message = 'Поле фамилия содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'f-email', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'firstNameTooShort':
      message = 'Поле имя содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'firstName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'firstNameTooLong':
      message = 'Поле имя содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'firstName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'firstNameInvalidFormat':
      message = 'Поле имя содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'firstName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'middleNameTooShort':
      message = 'Поле отчество содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'middleName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'middleNameTooLong':
      message = 'Поле отчество содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'middleName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'middleNameInvalidFormat':
      message = 'Поле отчество содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'middleName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlQuestionTypeInvalidFormat':
      message = 'Контрольый тип вопроса содержит что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'controlQuestionType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlQuestionTypeNotInAvailableRange':
      message = 'Контрольый тип вопроса содержит поля меньше 1 или больше 3'
      store.commit('SET_ERRORS', [{field: 'controlQuestionType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlQuestionTooShort':
      message = 'Контрольый вопрос содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'controlQuestion', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlQuestionTooLong':
      message = 'Контрольый вопрос содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'controlQuestion', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlAnswerTooShort':
      message = 'Контрольый ответ содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'controlAnswer', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlAnswerTooLong':
      message = 'Контрольый ответ содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'controlAnswer', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'controlAnswerInvalidFormat':
      message = 'Контрольый ответ содержит неразрешенные символы (допустимы кириллица, пробел)'
      store.commit('SET_ERRORS', [{field: 'controlAnswer', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'smsCodeInvalid':
      message = 'Неверно введен код'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'userNotUnique':
      message = 'Указанный номер телефона ранее был зарегистрирован в личном кабинете'
      store.commit('SET_ERRORS', [{field: 'mobilePhone', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'smsCodeNotInAvailableRange':
      message = 'Код подтверждения введен ошибочно'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneDoNotHaveActiveSmsCode':
      message = 'Внутренняя ошибка. Код подтверждения пользователю не высылался, но передается в API'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'mobilePhoneBlocked':
      message = 'Превышено количество попыток ввода кода, запросите новый СМС код'
      store.commit('SET_ERRORS', [{field: 'для ввода СМС кода', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'genderNotInAvailableRange':
      message = 'Поле "пол" отлично от допустимых значений “male”/”female”'
      store.commit('SET_ERRORS', [{field: 'gender', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'bDayInvalidDateFormat':
      message = 'Значение поля дата рождения не соответствует формату ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'дата рождения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'minAgeRestriction':
      message = 'Вам должно быть больше 19 и меньше 100 лет'
      store.commit('SET_ERRORS', [{field: 'дата рождения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'maxAgeRestriction':
      console.log('or here');
      message = 'Вам должно быть больше 19 и меньше 100 лет'
      store.commit('SET_ERRORS', [{field: 'дата рождения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'bPlaceTooLong':
      message = 'В поле место рождения содержится более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'bPlace', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'bPlaceTooShort':
      message = 'В поле место рождения содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'bPlace', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'bPlaceInvalidFormat':
      message = 'В поле место рождения содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'bPlace', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportInvalidFormat':
      message = 'В поле серия и номер паспорта содержиться что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'passport', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportTooShort':
      message = 'В поле серия и номер паспорта содержит менее 10-и символов'
      store.commit('SET_ERRORS', [{field: 'passport', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportTooLong':
      message = 'В поле серия и номер паспорта содержит более 10-и символов'
      store.commit('SET_ERRORS', [{field: 'passport', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDivisionCodeInvalidFormat':
      message = 'В поле код подразделения паспорта содержиться что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'passportDivisionCode', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDivisionCodeTooShort':
      message = 'В поле код подразделения паспорта содержит менее 6-и символов'
      store.commit('SET_ERRORS', [{field: 'passportDivisionCode', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDivisionCodeTooLong':
      message = 'В поле код подразделения паспорта содержит более 6-и символов'
      store.commit('SET_ERRORS', [{field: 'passportDivisionCode', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDateOfIssueInvalidDateFormat':
      message = 'Значение поля выдача паспорта не соответствует формату ДД.ММ.ГГГГ'
      store.commit('SET_ERRORS', [{field: 'дата выдачи', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDateOfIssueLowerThenMin':
      message = 'Дата выдачи паспорта валироваться как минимум от 25.12.1991 года (дата образования РФ)'
      store.commit('SET_ERRORS', [{field: 'дата выдачи', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportDateOfIssueGreaterThenMax':
      store.commit('SET_ERRORS', [{field: 'дата выдачи', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      message = 'Паспорт РФ выдан позже настоящего времени'
      break

    case 'passportIssuedByTooShort':
      message = 'В поле паспорт выдан содержит менее 6-и символов'
      store.commit('SET_ERRORS', [{field: 'passportIssuedBy', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportIssuedByTooLong':
      message = 'В поле паспорт выдан содержит более 200-и символов'
      store.commit('SET_ERRORS', [{field: 'passportIssuedBy', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'passportIssuedByInvalidFormat':
      message = 'В поле паспорт выдан содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'passportIssuedBy', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'snilsInvalidFormat':
      message = 'Введен некорректный снилс. Снилс должен соответсвовать формату xxx-xxx-xxx xx'
      store.commit('SET_ERRORS', [{field: 'snils', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'snilsCodeIncorrect':
      message = 'Снилс заполнен некорректно'
      store.commit('SET_ERRORS', [{field: 'snils', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'additionalPhoneInvalidFormat':
      message = 'Номер мобильного дополнительного телефона должен быть записан 7ХХХХХХХХХХ'
      store.commit('SET_ERRORS', [{field: 'additionalPhone', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'additionalPhoneOwnerTooShort':
      message = 'В другой владелец номера содержит менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'additionalPhoneOwner', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'additionalPhoneOwnerTooLong':
      message = 'В другой владелец номера содержит более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'additionalPhoneOwner', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'additionalPhoneOwnerInvalidFormat':
      message = 'В другой владелец номера содержит недопустимые символы (допустимы цифры, кириллица, пробел)'
      store.commit('SET_ERRORS', [{field: 'additionalPhoneOwner', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'typeOfEmploymentInvalidFormat':
      message = 'Поле род занятий содержит что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'typeOfEmployment', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'typeOfEmploymentNotInAvailableRange':
      message = 'Поле род занятий содержит поля меньше значения 1 или больше 8'
      store.commit('SET_ERRORS', [{field: 'typeOfEmployment', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workSalaryInvalidFormat':
      message = 'В поле заработная плата содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workSalary', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workSalaryNotInAvailableRange':
      message = 'В поле заработная плата должна быть указана зарплата не менее 2000 рублей и не больше 1000000 рублей'
      store.commit('SET_ERRORS', [{field: 'workSalary', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'notBetweenStrict':
      message = 'Ежемесячный доход должен быть указан от 2000 и максимум до 1000000 рублей'
      store.commit('SET_ERRORS', [{field: 'workSalary', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workINNInvalidFormat':
      message = 'Поле ИНН содержит что-то кроме цифр, или содержит недопустимое число символов'
      store.commit('SET_ERRORS', [{field: 'ИНН', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workINNTooShort':
      message = 'Поле ИНН содержит менее 12-и символов'
      store.commit('SET_ERRORS', [{field: 'ИНН', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workINNTooLong':
      message = 'Поле ИНН содержит более 12-и символов'
      store.commit('SET_ERRORS', [{field: 'ИНН', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workFullNameTooShort':
      message = 'В поле название организации содержится менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'workFullName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workFullNameTooLong':
      message = 'В поле название организации содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'workFullName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workTypeInvalidFormat':
      message = 'В поле тип организации содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workTypeNotInAvailableRange':
      message = 'В поле тип организации содержится значение меньше 1 или больше 2'
      store.commit('SET_ERRORS', [{field: 'workType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workScopeInvalidFormat':
      message = 'В поле сфера деятельности организации содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workScope', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workScopeNotInAvailableRange':
      message = 'В поле сфера деятельности организации значение меньше 1 или больше 20'
      store.commit('SET_ERRORS', [{field: 'workScope', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workNameTooShort':
      message = 'В поле название организации содержится менее 2-х символов'
      store.commit('SET_ERRORS', [{field: 'workName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workNameTooLong':
      message = 'В поле название организации содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'workName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workNumberOfEmployeesInvalidFormat':
      message = 'В поле кол-во сотрудников в компании содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workNumberOfEmployees', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workNumberOfEmployeesNotInAvailableRange':
      message = 'В поле кол-во сотрудников в компании значение меньше 1 или больше 5'
      store.commit('SET_ERRORS', [{field: 'workNumberOfEmployees', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workPhoneInvalidFormat':
      message = 'Номер мобильного дополнительного телефона должен быть записан 7ХХХХХХХХХХ'
      store.commit('SET_ERRORS', [{field: 'workPhone', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workPeriodInvalidFormat':
      message = 'В поле стаж работы у данного работодателя в компании содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workPeriod', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workPeriodNotInAvailableRange':
      message = 'В поле стаж работы у данного работодателя в компании значение меньше 1 или больше 4'
      store.commit('SET_ERRORS', [{field: 'workPeriod', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'numberOfChildrenInvalidFormat':
      message = 'В поле стаж работы у данного работодателя в компании содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'количество детей', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'numberOfChildrenNotInAvailableRange':
      message = 'В поле стаж работы у данного работодателя в компании значение меньше 0 или больше 99'
      store.commit('SET_ERRORS', [{field: 'количество детей', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'maritalStatusInvalidFormat':
      message = 'В поле семейное положение содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'Выберите семейное положение', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'maritalStatusNotInAvailableRange':
      message = 'В поле семейное положение значение меньше 1 или больше 4'
      store.commit('SET_ERRORS', [{field: 'Выберите семейное положение', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'educationTypeInvalidFormat':
      message = 'В поле образование содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'educationType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'educationTypeNotInAvailableRange':
      message = 'В поле образование значение меньше 1 или больше 4'
      store.commit('SET_ERRORS', [{field: 'educationType', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'hasPreviousConvictionNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле судимость содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'hasPreviousConviction', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialStateNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле являетесь ли вы или ваши близкие родственники иностранным публичным должностным лицом содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'officialState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialOwnerIsMeNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле укажите должность иностранного публичного лица содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'officialState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialNameTooShort':
      message = 'В поле укажите должность иностранного публичного лица содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'officialName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialNameTooLong':
      message = 'В поле укажите должность иностранного публичного лица содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'officialName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.internationalOrganizationStateNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле являетесь ли вы или ваши близкие родственники должностным лицом публичной международной организации содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'internationalOrganizationState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.internationalOrganizationOwnerIsMeNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле укажите должность лица и наименование международной организации содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'internationalOrganizationState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.internationalOrganizationNameTooShort':
      message = 'В поле укажите должность лица и наименование международной организации содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'internationalOrganizationName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.internationalOrganizationNameTooLong':
      message = 'В поле укажите должность лица и наименование международной организации содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'internationalOrganizationName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialRussiaStateNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле являетесь ли вы или ваши близкие родственники публичным должностным лицом Российской Федерации содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'officialRussiaState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialRussiaOwnerIsMeNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле укажите должность лица и наименование Российской организации содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'officialRussiaState', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialRussiaNameTooShort':
      message = 'В поле укажите должность лица и наименование Российской организации содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'officialRussiaName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.officialRussiaNameTooLong':
      message = 'В поле укажите должность лица и наименование Российской организации содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'officialRussiaName', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'publicity.notAllRequiredFieldSetNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле укажите действуете ли вы к выгоде другого лица (выгодоприобретателя) в том числе на основании агентского договора, договора поручения, комиссии, доверительного управления содержит недопустимые значений, ответ не соотвествует “true”/”false”'
      store.commit('SET_ERRORS', [{field: 'benefitsOfAnotherPerson', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.valueTooShort':
      message = 'В поле район, населенный пункт, улица содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'address.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.valueTooLong':
      message = 'В поле район, населенный пункт, улица содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'address.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.valueInvalidFormat':
      message = 'В поле район, населенный пункт, улица содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'address.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.postalCodeInvalidFormat':
      message = 'В поле индекс содержиться что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'индекс', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.postalCodeNotInAvailableRange':
      message = 'В поле индекс должно быть указано значение от 100000 до 999999'
      store.commit('SET_ERRORS', [{field: 'индекс', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.fiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле fiasId не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'address.fiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.cityFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле cityFiasId не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'address.cityFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.houseFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле houseFiasId не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'address.houseFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.regionFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле regionFiasId не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'address.regionFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.streetFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле streetFiasId не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'address.streetFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.houseInvalidFormat':
      message = 'В поле адреса регистрации номер дома не должно содержать что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'houseRegistration', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.regionInvalidFormat':
      message = 'В поле номер региона не должно содержать что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'regionRegistration', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.cityInvalidFormat':
      message = 'Поле город в адресе регистрации содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'regionRegistration', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.streetInvalidFormat':
      message = 'Поле улица в адресе регистрации содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'regionRegistration', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'address.flatInvalidFormat':
      message = 'В поле квартиры адреса регистрации не должно содержать что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'address.flat', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.valueTooShort':
      message = 'В поле район, населенный пункт, улица рабочего адреса содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'workAddress.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.valueTooLong':
      message = 'В поле район, населенный пункт, улица рабочего адреса содержится более 200-т символов'
      store.commit('SET_ERRORS', [{field: 'workAddress.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.valueInvalidFormat':
      message = 'В поле район, населенный пункт, улица рабочего адреса содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'workAddress.value', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.postalCodeInvalidFormat':
      message = 'В поле индекс места работы содержиться что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'рабочий индекс', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.postalCodeNotInAvailableRange':
      message = 'В поле индекс места работы должно быть указано значение от 100000 до 999999'
      store.commit('SET_ERRORS', [{field: 'рабочий индекс', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.fiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле fiasId места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'workAddress.fiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.cityFiasIdNotMatchRequiredForma':
      message = 'Внутреняя ошибка. Поле cityFiasId места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'workAddress.cityFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.houseFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле houseFiasId места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'workAddress.houseFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.regionFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле regionFiasId места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'workAddress.regionFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.streetFiasIdNotMatchRequiredFormat':
      message = 'Внутреняя ошибка. Поле streetFiasId места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
      store.commit('SET_ERRORS', [{field: 'workAddress.streetFiasId', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.regionInvalidFormat':
      message = 'В поле адреса рабочего места в номере региона не должно содержать что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'regionWork', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.cityInvalidFormat':
      message = 'Поле город место работы содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'regionWork', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.streetInvalidFormat':
      message = 'Поле улица место работы содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
      store.commit('SET_ERRORS', [{field: 'regionWork', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.houseInvalidFormat':
      message = 'В поле дом места работы не должно содержиться что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'houseWork', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'workAddress.flatInvalidFormat':
      message = 'В поле офиса места работы не должно содержиться что-то, кроме цифр'
      store.commit('SET_ERRORS', [{field: 'workAddress.flat', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.lastNameTooShort':
      message = 'В поле фалилии бенефициального владелеца содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'фамилия бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.lastNameTooLong':
      message = 'В поле фалилии бенефициального владелеца содержится более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'фамилия бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.lastNameInvalidFormat':
      message = 'В поле фалилии бенефициального владелеца содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'фамилия бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.firstNameTooShort':
      message = 'В поле имени бенефициального владелеца содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'имя бенефицала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.firstNameTooLong':
      message = 'В поле имени бенефициального владелеца содержится более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'имя бенефицала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.firstNameInvalidFormat':
      message = 'В поле имени бенефициального владелеца содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'имя бенефицала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.middleNameTooShort':
      message = 'В поле отчества бенефициального владелеца содержится менее 3-х символов'
      store.commit('SET_ERRORS', [{field: 'отчество бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.middleNameTooLong':
      message = 'В поле отчества бенефициального владелеца содержится более 50-и символов'
      store.commit('SET_ERRORS', [{field: 'отчество бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.middleNameInvalidFormat':
      message = 'В поле отчества бенефициального владелеца содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
      store.commit('SET_ERRORS', [{field: 'отчество бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportInvalidFormat':
      message = 'В поле серии и номера паспорта бенефициального владелеца содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'паспорт бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportTooShort':
      message = 'В поле серии и номера паспорта бенефициального владелеца содержится менее 10-и символов'
      store.commit('SET_ERRORS', [{field: 'паспорт бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportTooLong':
      message = 'В поле серии и номера паспорта бенефициального владелеца содержится более 10-и символов'
      store.commit('SET_ERRORS', [{field: 'паспорт бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDivisionCodeInvalidFormat':
      message = 'В поле кода подразделения паспорта бенефициального владелеца содержится что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'код подразделения паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDivisionCodeTooShort':
      message = 'В поле кода подразделения паспорта бенефициального владелеца содержится менее 6-и символов'
      store.commit('SET_ERRORS', [{field: 'код подразделения паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDivisionCodeTooLong':
      message = 'В поле кода подразделения паспорта бенефициального владелеца содержится более 6-и символов'
      store.commit('SET_ERRORS', [{field: 'код подразделения паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDateOfIssueInvalidDateFormat':
      message = 'В поле дата выдачи паспорта бенефициального владелеца не соответствует формату dd.mm.YY '
      store.commit('SET_ERRORS', [{field: 'дата выдачи паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDateOfIssueLowerThenMin':
      message = 'В поле дата выдачи паспорта бенефициального владелеца должна быть указана дата выдан не позже 25.12.1991 (дата образования РФ)'
      store.commit('SET_ERRORS', [{field: 'дата выдачи паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'beneficial.passportDateOfIssueGreaterThenMax':
      message = 'Паспорт бенефициального владелеца не может быть выдан раньше настоящего времени'
      store.commit('SET_ERRORS', [{field: 'дата выдачи паспорта бенефициала', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'finalInvalidFormat':
      message = 'Внутреняя ошибка. Поле final содержит что-то кроме цифр'
      store.commit('SET_ERRORS', [{field: 'final', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'finalNotInAvailableRange':
      message = 'Внутреняя ошибка. Поле final содержит значение меньше 0 или больше 1'
      store.commit('SET_ERRORS', [{field: 'final', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'notAllRequiredFieldSet':
      message = 'Пропущенно обязательное поле. Пожалуйста, проверьте поля, заполните их и снова нажмите кнопку продолжить'
      store.commit('SET_ERRORS', [{field: 'passportIssuedBy, additionalPhoneOwner', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break

    case 'serverSideError':
      message = 'Что то пошло не так'
      store.commit('SET_ERRORS', [{field: 'passportIssuedBy, additionalPhoneOwner', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
