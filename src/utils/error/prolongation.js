export const errorProlongation = (err, store) => {
  switch (err) {
    case 'smsCodeInvalid':
      let message = 'Вы ввели неверный СМС код'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'mobilePhoneBlocked':
      message = 'Превышено количество попыток ввода кода'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'smsCodeIsEmpty':
      message = 'Ошибка. Код должен состоять из 4-х символов'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'smsCodeInvalidFormat':
      message = 'Повторите ввод смс кода. Код может состоять только из цифр'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'notAvailableForSign':
      message = 'Пролонгация Вам не может быть предоставлена'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'mobilePhoneDoNotHaveActiveSmsCode':
      message = 'Смс код не был отправлен. Попробуйте запрос выполнить позднее или обратитесь в он-лайн чат'
      store.commit('SET_ERRORS', [{field: 'sms-код для подтверждения допсоглашения', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'daysDurationIsEmpty':
      message = 'Не указано количество дней пролонгации'
      store.commit('SET_ERRORS', [{field: 'срок продления', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'daysDurationNotGreaterThan':
      message = 'Количество дней пролонгации не может быть меньше 1'
      store.commit('SET_ERRORS', [{field: 'срок продления', message: message}])
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'doNotHaveActiveLoan':
      message = 'Ошибка. У вас нету активного займа'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'prolongationIsDisable':
      message = 'Пролонгация была отклонена'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'loanBarrageTimeNotEnd':
      message = 'С момента начала займа не прошло 4 полных дня. Пролонгация вам не доступна'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'alreadyHaveUnsignedProlongation':
      message = 'У вас уже есть пролонгация, ожидающая подписания'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'alreadyHaveUnPayedProlongation':
      message = 'У вас действующая пролонгация, ожидающая оплаты'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'prolongationCreationTimeout':
      message = 'Между последней операцией по пролонгации прошло менее 12 часов. Создание новой пролонгации пока не доступно'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'daysNotInAvailableRange':
      message = 'Пролонгация не может быть предоставлена свыше максимального срока'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'spamProtectionRestriction':
      message = 'Превышен лимит запросов получения смс кода для подписания'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'notHaveUnsignedProlongation':
      message = 'У вас нету действующущей пролонгации, ожидающей подписания'
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
