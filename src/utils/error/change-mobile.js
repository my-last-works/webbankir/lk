export const errorChangeMobilePhone = (err) => {
  switch (err) {
    case 'mobilePhoneEqualToCurrent':
      return 'Указанный мобильный телефон уже используется в системе'
    case 'mobilePhoneNotUnique':
      return 'Указанный мобильный телефон уже используется в системе'
    case 'userHaveActiveLoan':
      return 'На момент рассмотрения заявки на займ, изменение телефон запрещено'
    case 'mobilePhoneInvalidFormat':
      return 'Мобильный телефон не соответствует формату +7(9ХХ)ХХХ-ХХХХ'
    case 'mobilePhoneDoNotHaveActiveSmsCode':
      return 'Внутренняя ошибка. Попробуйте повторить запрос позже'
    case 'userNotHaveLoans':
      return 'У вас не было ни одной заявки на займ. Операция для смены мобильного телефона не доступна'
    case 'smsCodeInvalidFormat':
      return 'СМС-код введен не корректно. Должны содержаться только цифры'
    case 'spamProtectionRestriction':
      return 'Привышено лимит запросов отправки СМС-кода'
    case 'notAllRequiredFieldSet':
      return 'Поле телефон было заполнено не корректно'
    case 'smsCodeInvalid':
      return 'СМС-код введен не корректно'
    case 'mobilePhoneBlocked':
      return 'Превышен лимит попыток ввода СМС-кода'
  }
}
