export const errorRepay = (err, store) => {
  switch (err) {
    case 'sumIsEmpty':
      let message = 'В поле сумма оплаты пусто!!! Необходимо указать сумму к оплате!!!'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'paymentDataError':
      message = 'Некорретно заполнены поля!!! Необходимо проверить поле к оплате суммы'
      store.commit('CALL_WARNING_WINDOW', message)
      break
    case 'amountError':
      message = 'Минимальная сумма оплаты составляет 10 рублей'
      store.commit('CALL_WARNING_WINDOW', message)
      break
  }
}
