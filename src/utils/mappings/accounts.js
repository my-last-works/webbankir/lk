import cv from 'card-validator'

export const getImgFilename = (type, number) => {
  if (type === 6 || (number && number.toString().length === 10)) {
    return 'qiwi.png';
  }

  if (type === 3) {
    return 'yandex.png';
  }

  if (type === 5) {
    return 'contact.png';
  }

  if (!number || !type) {
    return `card.png`
  }
  const card = cv.number(number.toString().slice(0, 4));
  console.log('Card detect', card);
  if ((typeof card !== "object" || typeof card.card !== "object") || card.card === null) return 'card.png';

  switch(card.card.type) {
    case 'visa':
    case 'maestro':
    case 'jsb':
      type = card.card.type;
      break;
    case 'mastercard':
      type = 'master-card';
      break;
    default:
      type = 'card';
      break;
  }

  return `${type}.png`
}

export const getPaySystemText = (paySystems) => ({
  1: 'Карта',
  3: 'Яндекс кошелёк',
  4: 'QIWI Wallet',
  5: 'Контакт'
})[paySystems]
