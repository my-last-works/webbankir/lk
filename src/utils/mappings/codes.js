export const SupplementaryStatus = {
  0: 'prolongationAvalible',
  1: 'needSign',
  2: 'needPay',
  3: 'prolongrationUnavalible'
}
