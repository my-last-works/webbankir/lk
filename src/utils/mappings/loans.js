export const getDescription = (statusCode) => {
  if (statusCode === undefined) {
    return
  }
  return ({
    0: 'Ожидает подписи',
    1: 'Ожидает очереди на рассмотрение',
    2: 'Рассматривается',
    3: 'Одобрен',
    4: 'Договор подписан',
    5: 'В очереди на оплату',
    6: 'Займ выдан',
    7: 'Займ погашен',
    8: 'Просрочен платеж',
    9: 'Начато судебное производство',
    100: 'Заявка отклонена',
    101: 'Клиент отказался',
    102: 'Истек срок подписания',
    200: 'Ручная проверка'
  })[statusCode]
}

export const getIconClass = (statusCode) => {
  if (statusCode === undefined) {
    return
  }
  return ({
    0: 'status-whaiting',
    1: 'status-whaiting',
    2: 'status-whaiting',
    3: 'status-sucess',
    4: 'status-sucess',
    5: 'status-sucess',
    6: 'status-sucess',
    7: 'status-sucess',
    8: 'status-sucess',
    9: 'status-sucess',
    100: 'status-deny',
    101: 'status-deny',
    102: 'status-deny',
    200: 'status-deny'
  })[statusCode]
}

export const loanTypeMapper = (type) => {
  return ({
    1: 'Займ',
    2: 'Дополнительное соглашение',
    3: 'Доп. услуги',
    4: 'Доп. услуги',
  })[type]
}
