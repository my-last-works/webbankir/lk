export default function () {
    const dev_host = [
        /^dev\-/,
        /seb\-web\.ru$/,
        /^alpha\./,
        /^172\.16\./,
        /^127\.0\.0\.1/,
       /^localhost$/,
        /^dev\./
    ];

    return dev_host.find((host) => {
        return host.test(document.location.hostname);
    }) ? {
        env: 'dev',
        base: 'https://dev-api.webbankir.com'
    } : {
        env: 'prod',
        base: 'https://new.webbankir.com/api'
    };
};
