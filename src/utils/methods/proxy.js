export default function(routers) {
    return new Proxy(() => {}, {
        get (f, key) {
            const vm = this;

            if(typeof routers[key] === "function") {
                return function () {
                    return routers[key].apply(vm, arguments);
                }
            } else if(routers[key]) {
                return routers[key];
            } else {
                throw new Error(`Метод ${key} не найден`)
            }
        }
    });
}
