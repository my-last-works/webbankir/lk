import proxy from './proxy';
import api from '../backend';
import env from './env';
import store from '@/store';
const routers = {
    user: {
        login: function (creds) {
            return api.postData('/user/login', creds);
        },
        get:  function () {
            return api.getData('/user/get');
        },
        create: function (data) {
            return api.postData('/user/v2/create', data);
        },
        edit: function (data) {
            return api.patchData('/user/v2/edit', data);
        },
        validate: function () {
            return api.getData('/user/v2/validate');
        },
        social: function () {
            return api.getData('/socialnetwork/user');
        },
        verify: function () {
            return api.postData('/user/verify', data)
        },
        password: {
            change (password, oldPassword) {
                return api.postData('/user/password/change/inside', {
                    password, oldPassword
                })
            },
            changeOutside(data) {
                return api.patchData('/user/password/change/outside', data)
            },
            recovery (password, emailHash) {
                return api.postData('/user/password/change/outside', {
                    password, emailHash
                })
            },
            restore(data) {
                return api.postData('/user/password/restore', data);
            }
        },
        esia: {
            get(key) {
                return api.postData('/user/esia/get', { key });
            },
            authUrl: env().base + '/user/esia/auth'
            
        },
        email: {
            edit(email) {
                return api.patchData('/user/edit/email', {email})
            },
            confirm(emailHash) {
                return api.postData('/user/edit/email/confirm', {emailHash})
            },
            spam(emailhash) {
                return api.postData('/user/email/spam/', {emailHash});
            }
        },
        phone: {
            edit(data) {
                return api.patchData('/user/edit/phone', data);
            }
        },
        additional: {
            fill(data) {
                return api.patchData('/user/v2/create/fill_additional', data);
            }
        }
    },
    
    autologin: {
        get (hash) {
            return api.postData('/autologin/get', { hash });
        }
    },
    
    loan: {
        pdl () {
            return api.getData('/loan/get/current/pdl');
        },
        
        pos () {
            return api.getData('/loan/get/current/pos');
        },
        
        create (data) {
            return api.postData('/loan/v2/create', data);
        },
        
        all () {
            return api.getData('/loan/get');
        },
        sign: {
            resend () {
                return api.getData('/loan/sign/resend');
            },
            resendUrl: '/loan/sign/resend',
            sign (code) {
                return api.postData('/loan/sign', {code});
            }
        }
    },
    
    calc: {
        get () {
            return api.getData('/calculator-loan/v2');
        }
    },
    
    installment: {
        get () {
            return api.getData('/installment-loan/loan/get');
        },
        
        create (data) {
            return api.postData('/installment-loan/loan/create', data);
        },
        schedule (data) {
            return api.postData('/schedule-payments/getPaymentSchedule', data);
        },
        sign: {
            sign(code) {
                return api.postData('/installment-loan/loan/sign', {code});
            },
            
            resendUrl: '/installment-loan/loan/sign/resend'
        },
        prolongation: {
            create(Days) {
                return api.postData('/installment-loan/prolongation/create', {Days});
            },
            
            sign (Code) {
                return api.postData('/installment-loan/prolongation/sign', {Code})
            }
        }
    },
    
    accounts: {
        get () {
            return api.getData('/accounts');
        },
        delete (account) {
            return api.deleteData(`/accounts/${account}`)
        },
        update (account) {
            return api.patchData('/accounts', account);
        },
        create (account) {
            return api.putData('/accounts', account);
        },
        checkCard(card) {
            return api.postData('/accounts/checkCard', card);
        }
    },
    
    
    supplementary: {
        prolongation: {
            calc () {
                return api.getData('/supplementary/prolongation/calc');
            },
            create (data) {
                return api.putData('/supplementary/prolongation/create', data);
            },
            sign (data) {
                return api.postData('/supplementary/prolongation/sign', data);
            },
            resendUrl: '/supplementary/prolongation/sign/resend'
        },
        
        installment: {
            sign (smsCode) {
                return api.postData('/supplementary/installment/sign', {smsCode});
            },
            
            resend () {
                return api.getData('/supplementary/installment/sign/resend');
            },
            resendUrl: '/installment-loan/prolongation/sign/resend'
        },
        
        amnesty: {
            create() {
                return api.postData('/supplementary/amnesty/create', {});
            }
        }
    },
    
    services: {
        all () {
            return api.getData('/creditrating/services');
        },
        
        cr: {
            create(data) {
                return api.postData('/creditrating/creditrating/create', data);
            },
            
            guid(guid) {
                return api.getData('/creditrating/creditrating/latest?guid=' + guid)
            }
        },
        
        history () {
            return api.getData('/creditrating/creditrating/history');
        }
    },
    
    cashback: {
        balance() {
            return api.getData('/payment/cashback/balance');
            // console.log('%cВернуть ручку апи', 'color: red;font-weight: bold');
            return Promise.resolve();
        }
    },
    
    payment: {
        getPaymentSystem (url, data) {
            return api.postData(url, data);
        },
        
        all () {
            return api.getData('/payment/get');
        },
        order () {
            return api.getData('/loan/get/payment_data');
        },
        
        pay: {
            card (data) {
                return api.postData('/payment/card', data);
            },
            yandex(data) {
                return api.postData('/payment/create/yandex', data);
            },
            sberbank(data) {
                return api.postData('/payment/create/sberbank', data);
            },
            token (data) {
                return api.postData('/payment/token', data);
            },
            cashback (data) {
                return api.postData('/payment/cashback', data);
            },
        },
        
        create: {
            token(card) {
                return api.postData('/payment/create/token', card);
            },
            check(data) {
                return api.postData('/payment/check/token', data);
            }
        },
        
        
        
    },
    
    documents: {
        byLoan(id) {
            return api.getData(`/documents/list/${id}`)
        },
        byUrl(url) {
            return api.getData(`/documents/${url}`)
        },
        statement(id, type = null) {
            if(type) {
                return api.getData(`/documents/statement/${id}/${type}`);
            } else {
                return api.getData(`/documents/statement/${id}`);
            }
        },
        createStatement(id, type) {
            return api.putData(`/documents/statement/${id}/${type}`)
        },
        supplementary: {
            last(loanId) {
                return api.getData(`/documents/supplementary/html/${loanId}/last`);
            }
        }
    },
    
    suggestions: {
        address (query) {
            return api.postData('/address', query, 'https://api.webbankir.com/v3/suggest/')
        }
    },
    
    complaints: {
        create(data) {
            return api.postData('/complaints/create', data);
        },
    
        get () {
            return api.getData('/complaints/get')
        }
    },
    
    transactions: {
        history: {
            pos() {
                console.log('%cвернуть назад перед пулом!', 'color: red;font-weight: bold');
                // return api.getData('/transaction-history/get/pos');
                return Promise.resolve([]);
            }
        }
    },
    
    pos: {
        user: {
            get () {
                return api.getData(`/api-lk-client/v2/user/${store.getters.$user.id}`, 'https://webbankir.partners');
            }
        }
    }
    
}

export default new function () {
    console.log('%c dev mode', 'color: orange;font-weight: bold');
    return proxy(routers);
}
