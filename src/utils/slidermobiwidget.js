(function (d, s, id, companyId) { var sjs = d.getElementsByTagName(s)[0]; var js = d.createElement(s); js.id = id; js.src = 'https://widget.sender.mobi/connect/loader.js'; js.setAttribute('data-company-id', companyId); sjs.parentNode.insertBefore(js, sjs) }
)(document, 'script', 'sender-connect', 'i95082015391')

var wbOpenSenderWidgetEdited = false
function wbPrepareSliderMobiList () {
  if (typeof (ConnectWidget) === 'undefined') {
    return
  }

  if (wbOpenSenderWidgetEdited) {
    return
  }

  var $li = document.querySelector('.sender-connect-list-item[data-connect-id="sender"]')
  if ($li) {
    $li.classList.add('sender-connect-list-item-chat')
    $li.querySelector('.sender-connect-item-text').innerHTML = 'Чат'
  }

  var $liWant = document.querySelector('.sender-connect-list-item[data-el="want"]')
  if ($liWant) {
    $liWant.remove()
  }

  wbOpenSenderWidgetEdited = true
}

function watcher ($) {
  var senderButtonEdited = false

  function editSenderButton () {
    if (senderButtonEdited) {
      return
    }

    var $button = document.querySelector('.sender-connect-button')
    if (!$button) {
      setTimeout(editSenderButton, 200)
      return
    }

    $button.removeAttribute('style')

    document.querySelector('.sender-connect-button').addEventListener('click', function (event) {
      window.wbEvent.send('USER_CLICK_OPEN_CHAT')
    })

    wbPrepareSliderMobiList()
  }

  editSenderButton()
}

window.exportedWidgetFunc = wbPrepareSliderMobiList

watcher()
