// import dateformat from 'dateformat'
// import moment from 'moment'
import dayjs from 'dayjs';

export const getDateAfterDays = (days) => {
  const currentDate = new Date()
  return new Date(new Date().setDate(currentDate.getDate() + Number(days - 1))).toLocaleString('ru', {day: 'numeric', month: 'long', year: 'numeric'})
}

export const getMonth = (now) => {
  let months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][now-1];
  return months;
}

export const getShortDate = (date) => {
  return dayjs(date).format('DD-MM-YYYY')
  // return dateformat(date, 'dd-mm-yyyy')
}

export const num2words = (num, words) => {
  if (words === 'day') {
    words = ['день', 'дня', 'дней']
  }
  if (words === 'proc') {
    words = ['процент', 'процента', 'процентов']
  }
  if (words === 'rub') {
    words = ['рубль', 'рубля', 'рублей']
  }
  if (words === 'credit') {
    words = ['займ', 'займа', 'займов']
  }
  if (words === 'ost') {
    words = ['остался', 'осталось', 'осталось']
  }

  num = num % 100
  if (num > 19) {
    num = num % 10
  }

  switch (num) {
    case 1: {
      return (words[0])
    }
    case 2: case 3: case 4: {
      return (words[1])
    }
    default: {
      return (words[2])
    }
  }
}
