/*
 * Маппер ошибок с сервера
 * Разделен по модулям
 */

import loan from './loan'
import user from './user'
import sign from './sign'
import passportIssuer from './passport-issuer'
import calculatorLoan from './calculator-loan'
import accounts from './accounts'

export default {
  loan,
  user,
  'passport-issuer': passportIssuer,
  'calculator-loan': calculatorLoan,
  accounts,
  sign
}

const sortOrder = [
  'firstName',
  'lastName',
  'middleName,',
  'additionalPhone'
]

export const sortErrors = (errors) => errors.sort((a, b) => {
  if (!sortOrder.includes(a.user) || !sortOrder.includes(b.user)) {
    return -1
  }
  return sortOrder.indexOf(a.user) > sortOrder.indexOf(b.user) ? 1 : -1
})
