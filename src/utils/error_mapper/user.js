export default {
  'snilsOrInnCodeIncorrect': {
    code: 'snilsOrInnCodeIncorrect',
    field: 'snilsOrInn',
    vModel: 'snilsOrInn',
    message: 'Неверное значение поля СНИЛС или ИНН'
  },
  'passportDateOfIssueLowerThenBirthDate': {
    code: 'passportDateOfIssueLowerThenBirthDate',
    field: 'passportDateOfIssue',
    vModel: 'passportDateOfIssue',
    message: 'Паспорт не может быть выдан до достижения 14-ти летнего возраста. Проверьте дату выдачи.'
  },
  'userNotFound': {
    code: 'userNotFound',
    field: 'Логин',
    vModel: 'login',
    message: 'Адрес электронной почты не найден или неправильно введена фамилия'
  },
  'restoreLoginInvalidFormat': {
    code: 'restoreLoginInvalidFormat',
    field: 'f-email',
    vModel: 'dataForRestore.login',
    message: 'Неправильный логин или пароль'
  },
  'loginInvalidFormat': {
    code: 'loginInvalidFormat',
    field: 'Логин',
    vModel: 'login',
    message: 'Неправильный логин или пароль'
  },
  'snilsTooShort': {
    code: 'snilsTooShort',
    field: 'СНИЛС или ИНН',
    vModel: 'value',
    message: 'Поле «Снилс» указан не полностью'
  },
  'loginIsEmpty': {
    code: 'loginIsEmpty',
    field: 'Логин',
    vModel: 'login',
    message: 'Не заполнено поле «МОБИЛЬНЫЙ ТЕЛЕФОН, ЛОГИН ИЛИ ПОЧТА»'
  },
  'mobilePhoneNotUnique': {
    code: 'mobilePhoneNotUnique',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Указанный вами номер уже зарегистрирован'
  },
  'restoreLoginIsEmpty': {
    code: 'restoreLoginIsEmpty',
    field: 'f-email',
    vModel: 'dataForRestore.login',
    message: 'Не заполнено поле «МОБИЛЬНЫЙ ТЕЛЕФОН, ЛОГИН ИЛИ ПОЧТА»'
  },
  'restoreLastNameIsEmpty': {
    code: 'restoreLastNameIsEmpty',
    field: 'f-email',
    vModel: 'dataForRestore.login',
    message: 'Не заполнено поле «ФАМИЛИЯ»'
  },
  'passwordIsEmpty': {
    code: 'passwordIsEmpty',
    field: 'Пароль',
    vModel: 'password',
    message: 'Не заполнено поле «ПАРОЛЬ» '
  },
  'loginOrPasswordMismatching': {
    code: 'loginOrPasswordMismatching',
    field: 'Пароль',
    vModel: 'password',
    message: 'Неправильный логин или пароль'
  },
  'tokenIsEmpty': {
    code: 'tokenIsEmpty',
    field: 'Пользователь',
    message: 'Передан пустой token'
  },
  'tokenIncorrect': {
    code: 'tokenIncorrect',
    field: 'Пользователь',
    message: 'Передан некорректный token'
  },
  'mobilePhoneIsEmpty': {
    code: 'mobilePhoneIsEmpty',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Пустое поле «Мобильный телефон»'
  },
  'mobilePhoneInvalidFormat': {
    code: 'mobilePhoneInvalidFormat',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Номер мобильного телефона должен быть записан 79ХХХХХХХХХ'
  },
  'mobilePhoneNotUniq': {
    code: 'mobilePhoneDoNotHaveActiveSmsCode',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Текущий номер телефона уже имеет учетную запись'
  },
  'spamProtectionRestriction': {
    code: 'spamProtectionRestriction',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Превышен лимит запросов. За помощью обратитесь в онлайн-чат'
  },
  'emailAddressInvalidSegment': {
    code: 'emailAddressInvalidSegment',
    field: 'email',
    vModel: 'email',
    message: 'Указанный вами почтовый ящик является не корректным'
  },
  'emailAddressInvalidFormat': {
    code: 'emailAddressInvalidFormat',
    field: 'email',
    vModel: 'email',
    message: 'Поле «Email» не соответствует формату example@mail.ru'
  },
  'emailAddressInvalidHostname': {
    code: 'emailAddressInvalidHostname',
    field: 'email',
    vModel: 'email',
    message: 'В поле «Email» после @ формат указан неверно'
  },
  'emailAddressInvalidMxRecord': {
    code: 'emailAddressInvalidMxRecord',
    field: 'email',
    vModel: 'email',
    message: 'Данный email ранее не был создан'
  },
  'emailDomainInBlackList': {
    code: 'emailDomainInBlackList',
    field: 'email',
    vModel: 'email',
    message: 'Указанный email находиться в черном списке'
  },
  'lastNameTooShort': {
    code: 'lastNameTooShort',
    field: 'lastName',
    vModel: 'lastName',
    message: 'Поле «Фамилия» содержит менее 2-х символов'
  },
  'restoreLastNameTooShort': {
    code: 'restoreLastNameTooShort',
    field: 'f-fio',
    vModel: 'dataForRestore.lastName',
    message: 'Поле «Фамилия» содержит менее 2-х символов'
  },
  'lastNameTooLong': {
    code: 'lastNameTooLong',
    field: 'lastName',
    vModel: 'lastName',
    message: 'Поле «Фамилия» содержит более 50-и символов'
  },
  'lastNameInvalidFormat': {
    code: 'lastNameInvalidFormat',
    field: 'lastName',
    vModel: 'lastName',
    message: 'Поле «Фамилия» содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'restoreLastNameInvalidFormat': {
    code: 'restoreLastNameInvalidFormat',
    field: 'f-email',
    vModel: 'dataForRestore.login',
    message: 'Поле «Фамилия» содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'firstNameTooShort': {
    code: 'firstNameTooShort',
    field: 'firstName',
    vModel: 'firstName',
    message: 'Поле «Имя» содержит менее 2-х символов'
  },
  'firstNameTooLong': {
    code: 'firstNameTooLong',
    field: 'firstName',
    vModel: 'firstName',
    message: 'Поле «Имя» содержит более 50-и символов'
  },
  'firstNameInvalidFormat': {
    code: 'firstNameTooShort',
    field: 'firstName',
    vModel: 'firstName',
    message: 'Поле «Имя» содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'middleNameTooShort': {
    code: 'middleNameTooShort',
    field: 'middleName',
    vModel: 'middleName',
    message: 'Поле «Отчество» содержит менее 2-х символов'
  },
  'middleNameTooLong': {
    code: 'middleNameTooLong',
    field: 'middleName',
    vModel: 'middleName',
    message: 'Поле «Отчество» содержит более 50-и символов'
  },
  'middleNameInvalidFormat': {
    code: 'middleNameInvalidFormat',
    field: 'middleName',
    vModel: 'middleName',
    message: 'Поле «Отчество» содержит неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'controlQuestionTypeInvalidFormat': {
    code: 'controlQuestionTypeInvalidFormat',
    field: 'controlQuestionType',
    vModel: 'fieldControlQuestionType',
    message: 'Поле «Контрольый тип вопроса» содержит что-то кроме цифр'
  },
  'controlQuestionTypeNotInAvailableRange': {
    code: 'controlQuestionTypeNotInAvailableRange',
    field: 'controlQuestionType',
    vModel: 'fieldControlQuestionType',
    message: 'Поле «Контрольый тип вопроса» содержит поля меньше 1 или больше 3'
  },
  'controlQuestionTooShort': {
    code: 'controlQuestionTooShort',
    field: 'controlQuestion',
    vModel: 'controlQuestion',
    message: 'Поле «Контрольый вопрос» содержит менее 2-х символов'
  },
  'controlQuestionTooLong': {
    code: 'controlQuestionTooLong',
    field: 'controlQuestion',
    vModel: 'controlQuestion',
    message: 'Поле «Контрольый вопрос» содержит более 50-и символов'
  },
  'controlAnswerTooShort': {
    code: 'controlAnswerTooShort',
    field: 'controlAnswer',
    vModel: 'controlAnswer',
    message: 'Поле «Контрольый ответ» содержит менее 2-х символов'
  },
  'controlAnswerTooLong': {
    code: 'controlAnswerTooLong',
    field: 'controlAnswer',
    vModel: 'controlAnswer',
    message: 'Поле «Контрольый ответ» содержит более 50-и символов'
  },
  'controlAnswerInvalidFormat': {
    code: 'controlAnswerInvalidFormat',
    field: 'controlAnswer',
    vModel: 'controlAnswer',
    message: 'Поле «Контрольый ответ» содержит неразрешенные символы (допустимы кириллица, пробел)'
  },
  'smsCodeInvalid': {
    code: 'smsCodeInvalid',
    field: 'для ввода СМС кода',
    vModel: 'smsCode',
    message: 'Неверно введен код'
  },
  'userNotUnique': {
    code: 'userNotUnique',
    field: 'мобильный телефон',
    vModel: 'mobilePhone',
    message: 'Указанный номер телефона ранее был зарегистрирован в личном кабинете'
  },
  'smsCodeNotInAvailableRange': {
    code: 'smsCodeNotInAvailableRange',
    field: 'для ввода СМС кода',
    vModel: 'smsCode',
    message: 'Код подтверждения введен ошибочно'
  },
  'mobilePhoneDoNotHaveActiveSmsCode': {
    code: 'mobilePhoneDoNotHaveActiveSmsCode',
    field: 'для ввода СМС кода',
    vModel: 'smsCode',
    message: 'Внутренняя ошибка. Код подтверждения пользователю не высылался, но передается в API'
  },
  'mobilePhoneBlocked': {
    code: 'smsCodeInvalid',
    field: 'для ввода СМС кода',
    vModel: 'smsCode',
    message: 'Превышено количество попыток ввода кода, запросите новый СМС код'
  },
  'smsCodeInvalidFormat': {
    code: 'smsCodeInvalidFormat',
    field: 'для ввода СМС кода',
    vModel: 'smsCode',
    message: 'Ошибка. Поле «СМС код» содержит что-то, кроме цифр.'
  },
  'genderNotInAvailableRange': {
    code: 'genderNotInAvailableRange',
    field: 'gender',
    vModel: 'gender',
    message: 'Поле «Пол» отлично от допустимых значений “male”/”female”'
  },
  'bDayInvalidDateFormat': {
    code: 'bDayInvalidDateFormat',
    field: 'дата рождения',
    vModel: 'bDay',
    message: 'Значение поля «Дата рождения» не соответствует формату ДД.ММ.ГГГГ'
  },
  'minAgeRestriction': {
    code: 'minAgeRestriction',
    field: 'дата рождения',
    vModel: 'bDay',
    message: 'В поле «Дата рождения» должно быть указана дата не больше 19 и не меньше 100 лет'
  },
  'maxAgeRestriction': {
    code: 'maxAgeRestriction',
    field: 'дата рождения',
    vModel: 'bDay',
    message: 'В поле «Дата рождения» должно быть больше 19 и меньше 100 лет'
  },
  'bPlaceTooLong': {
    code: 'bPlaceTooLong',
    field: 'bPlace',
    vModel: 'bPlace',
    message: 'В поле «Место рождения» содержится более 50-и символов'
  },
  'bPlaceTooShort': {
    code: 'bPlaceTooShort',
    field: 'bPlace',
    vModel: 'bPlace',
    message: 'В поле «Место рождения» содержится менее 3-х символов'
  },
  'bPlaceInvalidFormat': {
    code: 'bPlaceInvalidFormat',
    field: 'bPlace',
    vModel: 'bPlace',
    message: 'В поле «Место рождения» содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'userPassportDataNotUnique': {
    code: 'userPassportDataNotUnique',
    field: 'passport',
    vModel: 'passport',
    message: 'Пользователь с данной серией и номером паспорта уже существует в личном кабинете'
  },
  'passportInvalidFormat': {
    code: 'passportInvalidFormat',
    field: 'passport',
    vModel: 'passport',
    message: 'В поле «Серия и номер паспорта» содержиться что-то кроме цифр'
  },
  'passportTooShort': {
    code: 'passportTooShort',
    field: 'passport',
    vModel: 'passport',
    message: 'В поле «Серия и номер паспорта» содержит менее 10-и символов'
  },
  'passportTooLong': {
    code: 'passportTooLong',
    field: 'passport',
    vModel: 'passport',
    message: 'В поле «Серия и номер паспорта» содержит более 10-и символов'
  },
  'passportDivisionCodeInvalidFormat': {
    code: 'passportDivisionCodeInvalidFormat',
    field: 'passportDivisionCode',
    vModel: 'passportDivisionCode',
    message: 'В поле «Код подразделения» паспорта содержиться что-то кроме цифр'
  },
  'passportDivisionCodeTooShort': {
    code: 'passportDivisionCodeTooShort',
    field: 'passportDivisionCode',
    vModel: 'passportDivisionCode',
    message: 'В поле «Код подразделения» паспорта содержит менее 6-и символов'
  },
  'passportDivisionCodeTooLong': {
    code: 'passportDivisionCodeTooLong',
    field: 'passportDivisionCode',
    vModel: 'passportDivisionCode',
    message: 'В поле «Код подразделения» паспорта содержит более 6-и символов'
  },
  'passportDateOfIssueInvalidDateFormat': {
    code: 'passportDateOfIssueInvalidDateFormat',
    field: 'дата выдачи',
    vModel: 'passportDateOfIssue',
    message: 'Значение поля «Выдача паспорта» не соответствует формату ДД.ММ.ГГГГ'
  },
  'passportDateOfIssueLowerThenMin': {
    code: 'passportDateOfIssueLowerThenMin',
    field: 'дата выдачи',
    vModel: 'passportDateOfIssue',
    message: 'Поле «Дата выдачи» паспорта валироваться как минимум от 25.12.1991 года (дата образования РФ)'
  },
  'passportDateOfIssueGreaterThenMax': {
    code: 'passportDateOfIssueGreaterThenMax',
    field: 'дата выдачи',
    vModel: 'passportDateOfIssue',
    message: 'В поле «Дата выдачи» паспорт РФ не может быть выдан ранее настоящего времени'
  },
  'passportIssuedByTooShort': {
    code: 'passportIssuedByTooShort',
    field: 'passportIssuedBy',
    vModel: 'passportIssuedBy',
    message: 'В поле «Паспорт выдан» содержит менее 6-и символов'
  },
  'passportIssuedByTooLong': {
    code: 'passportIssuedByTooLong',
    field: 'passportIssuedBy',
    vModel: 'passportIssuedBy',
    message: 'В поле «Паспорт выдан» содержит более 200-и символов'
  },
  'passportIssuedByInvalidFormat': {
    code: 'passportIssuedByInvalidFormat',
    field: 'passportIssuedBy',
    vModel: 'passportIssuedBy',
    message: 'В поле «Паспорт выдан» содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'snilsInvalidFormat': {
    code: 'snilsInvalidFormat',
    field: 'СНИЛС или ИНН',
    vModel: 'value',
    message: 'В поле «Снилс» введен некорректно. Снилс должен соответсвовать формату xxx-xxx-xxx xx'
  },
  'snilsTooLong': {
    code: 'snilsTooLong',
    field: 'СНИЛС или ИНН',
    vModel: 'value',
    message: 'В поле «Снилс» введен некорректно. Снилс содержит более 11 символов'
  },
  'snilsCodeIncorrect': {
    code: 'snilsCodeIncorrect',
    field: 'СНИЛС или ИНН',
    vModel: 'value',
    message: 'В поле «Снилс» введен некорректно'
  },
  'additionalPhoneInvalidFormat': {
    code: 'additionalPhoneInvalidFormat',
    field: 'additionalPhone',
    vModel: 'additionalPhone',
    message: 'В поле «Дополнительный телефон» должен быть записан 7ХХХХХХХХХХ'
  },
  'additionalPhoneOwnerTooShort': {
    code: 'additionalPhoneOwnerTooShort',
    field: 'additionalPhoneOwner',
    vModel: 'additionalPhoneOwner',
    message: 'В поле «ФИО владельца телефона» содержится менее 2-х символов'
  },
  'additionalPhoneOwnerTooLong': {
    code: 'additionalPhoneOwnerTooLong',
    field: 'additionalPhoneOwner',
    vModel: 'additionalPhoneOwner',
    message: 'В поле «ФИО владельца телефона» содержится более 50-и символов'
  },
  'additionalPhoneOwnerInvalidFormat': {
    code: 'additionalPhoneOwnerInvalidFormat',
    field: 'additionalPhoneOwner',
    vModel: 'additionalPhoneOwner',
    message: 'В поле «ФИО владельца телефона» содержится недопустимые символы (допустимы цифры, кириллица, пробел)'
  },
  'typeOfEmploymentInvalidFormat': {
    code: 'typeOfEmploymentInvalidFormat',
    field: 'typeOfEmployment',
    vModel: 'fieldTypeOfEmployment',
    message: 'Поле «Вид занятости» содержит что-то кроме цифр'
  },
  'typeOfEmploymentNotInAvailableRange': {
    code: 'typeOfEmploymentNotInAvailableRange',
    field: 'typeOfEmployment',
    vModel: 'fieldTypeOfEmployment',
    message: 'Поле «Вид занятости» содержит поля меньше значения 1 или больше 8'
  },
  'workSalaryInvalidFormat': {
    code: 'workSalaryInvalidFormat',
    field: 'workSalary',
    vModel: 'workSalary',
    message: 'В поле «заработная плата содержится что-то кроме цифр'
  },
  'workSalaryNotInAvailableRange': {
    code: 'workSalaryNotInAvailableRange',
    field: 'workSalary',
    vModel: 'workSalary',
    message: 'В поле «Ежемесячный доход» зарплата может быть указана не менее 2000 рублей и не больше 1000000 рублей'
  },
  'notBetweenStrict': {
    code: 'notBetweenStrict',
    field: 'workSalary',
    vModel: 'workSalary',
    message: 'В поле «Ежемесячный доход» должен быть указан от 2000 и максимум до 1000000 рублей'
  },
  'workINNInvalidFormat': {
    code: 'workINNInvalidFormat',
    field: 'ИНН',
    vModel: 'workINN',
    message: 'Поле «ИНН» содержит что-то кроме цифр, или содержит недопустимое число символов'
  },
  'workINNTooShort': {
    code: 'workINNTooShort',
    field: 'ИНН',
    vModel: 'workINN',
    message: 'Поле «ИНН» содержит менее 12-и символов'
  },
  'workINNTooLong': {
    code: 'workINNTooLong',
    field: 'ИНН',
    vModel: 'workINN',
    message: 'Поле «ИНН» содержит более 12-и символов'
  },
  'workFullNameTooShort': {
    code: 'workFullNameTooShort',
    field: 'workFullName',
    vModel: 'workFullName',
    message: 'В поле «Название организации» содержится менее 2-х символов'
  },
  'workFullNameTooLong': {
    code: 'workFullNameTooLong',
    field: 'workFullName',
    vModel: 'workFullName',
    message: 'В поле «Название организации» содержится более 200-т символов'
  },
  'workTypeInvalidFormat': {
    code: 'workTypeInvalidFormat',
    field: 'workType',
    vModel: 'workTypeStatus',
    message: 'В поле «Тип организации» содержится что-то кроме цифр'
  },
  'workTypeNotInAvailableRange': {
    code: 'workTypeNotInAvailableRange',
    field: 'workType',
    vModel: 'workTypeStatus',
    message: 'В поле «Тип организации» содержится значение меньше 1 или больше 2'
  },
  'workScopeInvalidFormat': {
    code: 'workScopeInvalidFormat',
    field: 'workScope',
    vModel: 'workScopeStatus',
    message: 'В поле «Сфера деятельности организации» содержится что-то кроме цифр'
  },
  'workScopeNotInAvailableRange': {
    code: 'workScopeNotInAvailableRange',
    field: 'workScope',
    vModel: 'workScopeStatus',
    message: 'В поле «Сфера деятельности организации» значение меньше 1 или больше 20'
  },
  'workNameTooShort': {
    code: 'workNameTooShort',
    field: 'workName',
    vModel: 'workName',
    message: 'В поле «Название организации» содержится менее 2-х символов'
  },
  'workNameTooLong': {
    code: 'workNameTooLong',
    field: 'workName',
    vModel: 'workName',
    message: 'В поле «Название организации» содержится более 200-т символов'
  },
  'workNumberOfEmployeesInvalidFormat': {
    code: 'workNumberOfEmployeesInvalidFormat',
    field: 'workNumberOfEmployees',
    vModel: 'workNumberOfEmployeesStatus',
    message: 'В поле «Кол-во сотрудников в компании» содержится что-то кроме цифр'
  },
  'workNumberOfEmployeesNotInAvailableRange': {
    code: 'workNumberOfEmployeesNotInAvailableRange',
    field: 'workNumberOfEmployees',
    vModel: 'workNumberOfEmployeesStatus',
    message: 'В поле «Кол-во сотрудников в компании» значение меньше 1 или больше 5'
  },
  'workPhoneInvalidFormat': {
    code: 'workPhoneInvalidFormat',
    field: 'workPhone',
    vModel: 'workPhone',
    message: 'В поле «Телефон работодателя» номер должен быть записан +7ХХХХХХХХХХ'
  },
  'workPeriodInvalidFormat': {
    code: 'workPeriodInvalidFormat',
    field: 'workPeriod',
    vModel: 'workPeriodStatus',
    message: 'В поле «Стаж работы у данного работодателя в компании» содержится что-то кроме цифр'
  },
  'workPeriodNotInAvailableRange': {
    code: 'workPeriodNotInAvailableRange',
    field: 'workPeriod',
    vModel: 'workPeriodStatus',
    message: 'В поле «Стаж работы у данного работодателя в компании» значение меньше 1 или больше 4'
  },
  'numberOfChildrenInvalidFormat': {
    code: 'numberOfChildrenInvalidFormat',
    field: 'количество детей',
    vModel: 'numberOfChildren',
    message: 'В поле «Стаж работы у данного работодателя в компании» содержится что-то кроме цифр'
  },
  'numberOfChildrenNotInAvailableRange': {
    code: 'numberOfChildrenNotInAvailableRange',
    field: 'количество детей',
    vModel: 'numberOfChildren',
    message: 'В поле «Стаж работы у данного работодателя в компании» значение меньше 0 или больше 99'
  },
  'maritalStatusInvalidFormat': {
    code: 'maritalStatusInvalidFormat',
    field: 'Выберите семейное положение',
    vModel: 'fieldMaritalStatus',
    message: 'В поле «Семейное положение» содержится что-то кроме цифр'
  },
  'maritalStatusNotInAvailableRange': {
    code: 'maritalStatusNotInAvailableRange',
    field: 'Выберите семейное положение',
    vModel: 'fieldMaritalStatus',
    message: 'В поле «Семейное положение» значение меньше 1 или больше 4'
  },
  'educationTypeInvalidFormat': {
    code: 'educationTypeInvalidFormat',
    field: 'educationType',
    vModel: 'fieldEducation',
    message: 'В поле «Образование» содержится что-то кроме цифр'
  },
  'educationTypeNotInAvailableRange': {
    code: 'educationTypeNotInAvailableRange',
    field: 'educationType',
    vModel: 'fieldEducation',
    message: 'В поле «Образование» значение меньше 1 или больше 4'
  },
  'sourceOfInformationInvalidFormat': {
    code: 'sourceOfInformationInvalidFormat',
    field: 'sourceOfInformation',
    vModel: 'fieldSourceOfInformation',
    message: 'В поле «Откуда Вы о нас узнали?» содержится что-то кроме цифр'
  },
  'sourceOfInformationNotInAvailableRange': {
    code: 'sourceOfInformationNotInAvailableRange',
    field: 'sourceOfInformation',
    vModel: 'fieldSourceOfInformation',
    message: 'В поле «Откуда Вы о нас узнали?» значение меньше 0 или больше 7'
  },
  'hasPreviousConvictionNotInAvailableRange': {
    code: 'hasPreviousConvictionNotInAvailableRange',
    field: 'hasPreviousConviction',
    vModel: 'hasPreviousConviction',
    message: 'Внутреняя ошибка. Поле «Есть ли у Вас судимость?» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.officialStateNotInAvailableRange': {
    code: 'publicity.officialStateNotInAvailableRange',
    field: 'officialState',
    vModel: 'fieldOfficialState',
    message: 'Внутреняя ошибка. Поле «Являетесь ли вы или ваши близкие родственники иностранным публичным должностным лицом» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.officialOwnerIsMeNotInAvailableRange': {
    code: 'publicity.officialOwnerIsMeNotInAvailableRange',
    field: 'officialState',
    vModel: 'fieldOfficialState',
    message: 'Внутреняя ошибка. Поле «Укажите должность иностранного публичного лица» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.officialNameTooShort': {
    code: 'publicity.officialNameTooShort',
    field: 'officialName',
    vModel: 'publicity.officialName',
    message: 'В поле «Укажите должность иностранного публичного лица» содержится менее 3-х символов'
  },
  'publicity.officialNameTooLong': {
    code: 'publicity.officialNameTooLong',
    field: 'officialName',
    vModel: 'publicity.officialName',
    message: 'В поле «Укажите должность иностранного публичного лица» содержится более 200-т символов'
  },
  'publicity.internationalOrganizationStateNotInAvailableRange': {
    code: 'publicity.internationalOrganizationStateNotInAvailableRange',
    field: 'internationalOrganizationState',
    vModel: 'fieldInternationalOrganizationState',
    message: 'Внутреняя ошибка. Поле «Являетесь ли вы или ваши близкие родственники должностным лицом публичной международной организации» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.internationalOrganizationOwnerIsMeNotInAvailableRange': {
    code: 'publicity.internationalOrganizationOwnerIsMeNotInAvailableRange',
    field: 'internationalOrganizationState',
    vModel: 'fieldInternationalOrganizationState',
    message: 'Внутреняя ошибка. Поле «Укажите должность лица и наименование международной организации» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.internationalOrganizationNameTooShort': {
    code: 'publicity.internationalOrganizationNameTooShort',
    field: 'internationalOrganizationName',
    vModel: 'publicity.internationalOrganizationName',
    message: 'В поле «укажите должность лица и наименование международной организации» содержится менее 3-х символов'
  },
  'publicity.internationalOrganizationNameTooLong': {
    code: 'publicity.internationalOrganizationNameTooLong',
    field: 'internationalOrganizationName',
    vModel: 'publicity.internationalOrganizationName',
    message: 'В поле «укажите должность лица и наименование международной организации» содержится более 200-т символов'
  },
  'publicity.officialRussiaStateNotInAvailableRange': {
    code: 'publicity.officialRussiaStateNotInAvailableRange',
    field: 'officialRussiaState',
    vModel: 'fieldOfficialRussiaState',
    message: 'Внутреняя ошибка. Поле «Являетесь ли вы или ваши близкие родственники публичным должностным лицом Российской Федерации» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.officialRussiaOwnerIsMeNotInAvailableRange': {
    code: 'publicity.officialRussiaOwnerIsMeNotInAvailableRange',
    field: 'officialRussiaState',
    vModel: 'fieldOfficialRussiaState',
    message: 'Внутреняя ошибка. Поле «Укажите должность лица и наименование Российской организации содержит» недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'publicity.officialRussiaNameTooShort': {
    code: 'publicity.officialRussiaNameTooShort',
    field: 'officialRussiaName',
    vModel: 'publicity.officialRussiaName',
    message: 'В поле «Укажите должность лица и наименование Российской организации» содержится менее 3-х символов'
  },
  'publicity.officialRussiaNameTooLong': {
    code: 'publicity.officialRussiaNameTooLong',
    field: 'officialRussiaName',
    vModel: 'publicity.officialRussiaName',
    message: 'В поле «Укажите должность лица и наименование Российской организации» содержится более 200-т символов'
  },
  'publicity.benefitsOfAnotherPersonNotInAvailableRange': {
    code: 'publicity.benefitsOfAnotherPersonNotInAvailableRange',
    field: 'benefitsOfAnotherPerson',
    vModel: 'fieldBenefitsOfAnotherPerson',
    message: 'Внутреняя ошибка. Поле «Укажите действуете ли вы к выгоде другого лица (выгодоприобретателя) в том числе на основании агентского договора, договора поручения, комиссии, доверительного управления» содержит недопустимые значений, ответ не соответствует “true”/”false”'
  },
  'addressFiases.valueTooShort': {
    code: 'addressFiases.valueTooShort',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'В поле «Район, населенный пункт, улица» содержится менее 3-х символов'
  },
  'addressFiases.valueInvalidFormat': {
    code: 'addressFiases.valueInvalidFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'В поле «Район, населенный пункт, улица» содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'address.postalCodeInvalidFormat': {
    code: 'address.postalCodeInvalidFormat',
    field: 'addressIndex',
    vModel: 'address.postal_code',
    message: 'В поле «Индекс» содержиться что-то кроме цифр'
  },
  'address.postalCodeNotInAvailableRange': {
    code: 'address.postalCodeNotInAvailableRange',
    field: 'addressIndex',
    vModel: 'address.postal_code',
    message: 'В поле «Индекс» должно быть указано значение от 100000 до 999999'
  },
  'addressFiases.house_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.house_fias_idNotMatchRequiredFormat',
    field: 'house',
    vModel: 'address.house',
    message: 'Внутреняя ошибка. Поле «Дом» раздел «House fias id» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'address.regionInvalidFormat': {
    code: 'address.regionInvalidFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. В поле «Район, населенный пункт, улица» раздел «номер региона» не должно содержать что-то, кроме цифр. Попробуйте заполнить раздел Адрес повторно'
  },
  'address.cityInvalidFormat': {
    code: 'address.cityInvalidFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Поле «Район, населенный пункт, улица» раздел «город» содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире). Попробуйте заполнить раздел Адрес повторно'
  },
  'address.streetInvalidFormat': {
    code: 'address.streetInvalidFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Поле «Район, населенный пункт, улица» раздел «улица» содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'address.settlementInvalidFormat': {
    code: 'address.settlementInvalidFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Поле «Район, населенный пункт, улица» раздел «улица» содержит недопустимые символы (допустимы только кирилица, цифры и символы ",.#()[]-")',
  },
  'address.doNotHaveStreetNotInAvailableRange': {
    code: 'address.doNotHaveStreetNotInAvailableRange',
    field: 'doNotHaveStreet',
    vModel: 'address.doNotHaveStreet',
    message: 'Внутреняя ошибка. Поле «Я подтверждаю, что по моему адресу регистрации улица отсутствует» содержится недопустимые значения, ответ не соответствует “true”/”false”'
  },
  'addressFiases.region_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.region_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Address fiases. Region fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddressFiases.region_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.region_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Work address fiases. Region fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'addressFiases.area_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.area_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Address fiases. Area fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddressFiases.area_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.area_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Work address fiases. Area fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'addressFiases.city_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.city_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Address fiases. City fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddressFiases.city_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.city_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Work address fiases. City fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'addressFiases.settlement_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.settlement_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'addressFiases.value',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Address fiases. Settlement fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddressFiases.settlement_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.settlement_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Work address fiases. Settlement fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'addressFiases.street_fias_idNotMatchRequiredFormat': {
    code: 'addressFiases.street_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «Address Fiases. Street fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddressFiases.street_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.street_fias_idNotMatchRequiredFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Внутреняя ошибка. Поле «Район, населенный пункт, улица» раздел «WorkAddress fiases. Street fias id not match required format» не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  
  'workAddress.doNotHaveStreetNotInAvailableRange': {
    code: 'workAddress.doNotHaveStreetNotInAvailableRange',
    field: 'doNotHaveStreet',
    vModel: 'address.doNotHaveStreet',
    message: 'Внутреняя ошибка. Поле «Я подтверждаю, что по моему адресу регистрации улица отсутствует» содержится недопустимые значения, ответ не соответствует “true”/”false”'
  },
  'workAddress.settlementInvalidFormat': {
    code: 'workAddress.settlementInvalidFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Поле «Район, населенный пункт, улица» в раздел «Улица» содержит недопустимые символы (допустимы только кирилица, цифры и символы ",.#()[]-")',
  },
  'workAddress.valueTooShort': {
    code: 'workAddress.valueTooShort',
    field: 'addressFiases.valueTooShort',
    vModel: 'region',
    message: 'В поле «Район, населенный пункт, улица» содержится менее 3-х символов'
  },
  'workAddress.valueTooLong': {
    code: 'workAddress.valueTooLong',
    field: 'addressFiases.valueTooShort',
    vModel: 'region',
    message: 'В поле «Район, населенный пункт, улица» содержится более 200-т символов'
  },
  'workAddress.valueInvalidFormat': {
    code: 'workAddress.valueInvalidFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'В поле «Район, населенный пункт, улица» содержится недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'workAddress.postalCodeInvalidFormat': {
    code: 'workAddress.postalCodeInvalidFormat',
    field: 'addressIndex',
    vModel: 'address.postal_code',
    message: 'В поле «Индекс» содержиться что-то кроме цифр'
  },
  'workAddress.postalCodeNotInAvailableRange': {
    code: 'workAddress.postalCodeNotInAvailableRange',
    field: 'addressIndex',
    vModel: 'address.postal_code',
    message: 'В поле «Индекс» должно быть указано значение от 100000 до 999999'
  },
  'workAddressFiases.house_fias_idNotMatchRequiredFormat': {
    code: 'workAddressFiases.house_fias_idNotMatchRequiredFormat',
    field: 'house',
    vModel: 'address.house',
    message: 'Внутреняя ошибка. Поле «Дом» в раздел «House fias id» места работы не соответствует формату xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddress.regionInvalidFormat': {
    code: 'workAddress.regionInvalidFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'В поле «адреса рабочего места в номере» в раздел «региона» не должно содержать что-то, кроме цифр'
  },
  'workAddress.cityInvalidFormat': {
    code: 'workAddress.cityInvalidFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Поле «Район, населенный пункт, улица» раздел «город» содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире). Попробуйте заполнить раздел Адрес повторно'
  },
  'workAddress.streetInvalidFormat': {
    code: 'workAddress.streetInvalidFormat',
    field: 'region',
    vModel: 'regionSearch',
    message: 'Поле «Район, населенный пункт, улица» раздел «улица» содержит недопустимые символы (допустимы цифры, кириллица, пробел, точка, запятая, тире)'
  },
  'workAddress.flatInvalidFormat': {
    code: 'workAddress.flatInvalidFormat',
    field: 'flat',
    vModel: 'address.flat',
    message: 'В поле «Офис» не должно содержиться что-то, кроме цифр'
  },
  'beneficial.stateNotInAvailableRange': {
    code: 'beneficial.stateNotInAvailableRange',
    field: 'beneficial',
    vModel: 'fieldBeneficial',
    message: 'Внутреняя ошибка. Поле «Бенефициальный владелец» содержится недопустимые значения, ответ не соответствует “true”/”false”'
  },
  'beneficial.lastNameTooShort': {
    code: 'beneficial.lastNameTooShort',
    field: 'фамилия бенефициала',
    vModel: 'beneficial.lastName',
    message: 'В поле «Фалилии бенефициального владелеца» содержится менее 3-х символов'
  },
  'beneficial.lastNameTooLong': {
    code: 'beneficial.lastNameTooLong',
    field: 'фамилия бенефициала',
    vModel: 'beneficial.lastName',
    message: 'В поле «Фалилии бенефициального владелеца» содержится более 50-и символов'
  },
  'beneficial.lastNameInvalidFormat': {
    code: 'beneficial.lastNameInvalidFormat',
    field: 'фамилия бенефициала',
    vModel: 'beneficial.lastName',
    message: 'В поле «Фалилии бенефициального владелеца» содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'beneficial.firstNameTooShort': {
    code: 'beneficial.firstNameTooShort',
    field: 'имя бенефицала',
    vModel: 'beneficial.firstName',
    message: 'В поле «Имени бенефициального владелеца» содержится менее 3-х символов'
  },
  'beneficial.firstNameTooLong': {
    code: 'beneficial.firstNameTooLong',
    field: 'имя бенефицала',
    vModel: 'beneficial.firstName',
    message: 'В поле «Имени бенефициального владелеца» содержится более 50-и символов'
  },
  'beneficial.firstNameInvalidFormat': {
    code: 'beneficial.firstNameInvalidFormat',
    field: 'имя бенефицала',
    vModel: 'beneficial.firstName',
    message: 'В поле «Имени бенефициального владелеца» содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'beneficial.middleNameTooShort': {
    code: 'beneficial.middleNameTooShort',
    field: 'отчество бенефициала',
    vModel: 'beneficial.middleName',
    message: 'В поле «Отчества бенефициального владелеца» содержится менее 3-х символов'
  },
  'beneficial.middleNameTooLong': {
    code: 'beneficial.middleNameTooLong',
    field: 'отчество бенефициала',
    vModel: 'beneficial.middleName',
    message: 'В поле «Отчества бенефициального владелеца» содержится более 50-и символов'
  },
  'beneficial.middleNameInvalidFormat': {
    code: 'beneficial.middleNameInvalidFormat',
    field: 'отчество бенефициала',
    vModel: 'beneficial.middleName',
    message: 'В поле «Отчества бенефициального владелеца» содержится неразрешенные символы (допустимы кириллица, пробел, тире и апостроф)'
  },
  'beneficial.passportInvalidFormat': {
    code: 'beneficial.passportInvalidFormat',
    field: 'паспорт бенефициала',
    vModel: 'beneficial.passport',
    message: 'В поле «Серии и номера паспорта бенефициального владелеца» содержится что-то кроме цифр'
  },
  'beneficial.passportTooShort': {
    code: 'beneficial.passportTooShort',
    field: 'паспорт бенефициала',
    vModel: 'beneficial.passport',
    message: 'В поле «Серии и номера паспорта бенефициального владелеца» содержится менее 10-и символов'
  },
  'beneficial.passportTooLong': {
    code: 'beneficial.passportTooLong',
    field: 'паспорт бенефициала',
    vModel: 'beneficial.passport',
    message: 'В поле «Серии и номера паспорта бенефициального владелеца» содержится более 10-и символов'
  },
  'beneficial.passportDivisionCodeInvalidFormat': {
    code: 'beneficial.passportDivisionCodeInvalidFormat',
    field: 'код подразделения паспорта бенефициала',
    vModel: 'beneficial.passportDivisionCode',
    message: 'В поле «Кода подразделения паспорта бенефициального владелеца» содержится что-то кроме цифр'
  },
  'beneficial.passportDivisionCodeTooShort': {
    code: 'beneficial.passportDivisionCodeTooShort',
    field: 'код подразделения паспорта бенефициала',
    vModel: 'beneficial.passportDivisionCode',
    message: 'В поле «Кода подразделения паспорта бенефициального владелеца» содержится менее 6-и символов'
  },
  'beneficial.passportDivisionCodeTooLong': {
    code: 'beneficial.passportDivisionCodeTooLong',
    field: 'код подразделения паспорта бенефициала',
    vModel: 'beneficial.passportDivisionCode',
    message: 'В поле «Кода подразделения паспорта бенефициального владелеца» содержится более 6-и символов'
  },
  'beneficial.passportDateOfIssueInvalidDateFormat': {
    code: 'beneficial.passportDateOfIssueInvalidDateFormat',
    field: 'дата выдачи паспорта бенефициала',
    vModel: 'beneficial.passportDateOfIssue',
    message: 'В поле «Дата выдачи паспорта бенефициального владелеца» не соответствует формату dd.mm.YY '
  },
  'beneficial.passportDateOfIssueLowerThenMin': {
    code: 'beneficial.passportDateOfIssueLowerThenMin',
    field: 'дата выдачи паспорта бенефициала',
    vModel: 'beneficial.passportDateOfIssue',
    message: 'В поле «Дата выдачи паспорта бенефициального владелеца» должна быть указана дата выдан не позже 25.12.1991 (дата образования РФ)'
  },
  'beneficial.passportDateOfIssueGreaterThenMax': {
    code: 'beneficial.passportDateOfIssueGreaterThenMax',
    field: 'дата выдачи паспорта бенефициала',
    vModel: 'beneficial.passportDateOfIssue',
    message: 'В поле «Паспорт бенефициального владелеца» не может быть выдан раньше настоящего времени'
  },
  'finalInvalidFormat': {
    code: 'finalInvalidFormat',
    message: 'Внутреняя ошибка. Поле «Final» содержит что-то кроме цифр. Проверьте все поля на заполненость'
  },
  'finalNotInAvailableRange': {
    code: 'finalNotInAvailableRange',
    message: 'Внутреняя ошибка. Поле «Final» содержит значение меньше 0 или больше 1'
  },
  'userFioAndBirthDayNotUnique': {
    code: 'userFioAndBirthDayNotUnique',
    message: 'Данный пользователь с такой же фамилией, именем, отчетством и датой рождения уже существует в личном кабинете'
  },
  'notAllRequiredFieldSet': {
    code: 'notAllRequiredFieldSet',
    message: 'Пропущенно обязательное поле. Пожалуйста, проверьте поля, заполните их и снова нажмите кнопку продолжить'
  },
  'serverSideError': {
    code: 'serverSideError'
  }
}
