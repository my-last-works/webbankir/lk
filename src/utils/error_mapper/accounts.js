export default {
  'description-add-validation-error': {
    code: 'description-add-validation-error',
    field: 'Номер карты',
    message: 'Некорректный номер карты'
  },
  'yandex-incorrect-error': {
    code: 'yandex-incorrect-error',
    field: 'для ввода номера яндекс кошелька',
    message: 'Некорректный номер кошелька Яндекс.Деньги'
  },
  'yandex-owner-error': {
    code: 'yandex-owner-error',
    field: 'для ввода номера яндекс кошелька',
    message: 'Анкетные данные не соответствуют владельцу Yandex кошелька'
  },
  'card-expired-error': {
    field: 'Номер карты',
    message: 'Срок действия этой карты истёк'
  }
}
