export default {
  passwordIsEmpty: 'Пароль не задан',
  passwordTooShort: 'Пароль должен содержать 7 и более символов',
  passwordInvalidFormat: 'Пароль должен содержать как минимум 1-у заглавную латинскую букву, 1-у прописную латинскую букву и 1-у цифру',
  passwordwrongSymbols: 'Пароль содержит недопустимые спец. символы',
  emailDoNotHaveRecoveryEmail: 'Неверная ссылка для восстановления пароля',
  emailHashInvalid: 'Неверная ссылка для восстановления пароля'
}
