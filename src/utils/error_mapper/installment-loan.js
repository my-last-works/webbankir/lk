export default {
  notValidData: 'Нет валидных данных',
  notHaveLoan: 'Внимание, у вас нет активного займа',
  haveNotActiveUnsignedSupplementary: 'Внимание, услуга по продлению займа уже подписана',
  exceedTheNumbetAttempt: 'Внимание, у вас превышено максимально доступного ввода смс-кода',
  userInvalid: 'Данный пользователь не существует или заблокирован',
  InvalidCode: 'Смс-код введен не корректно',
  userDoesNotHaveActiveLoan:  'У вас отстуствует активный займ',
  userDoesNotHaveUnsignActiveLoan:  'У вас имеется неподписанный договор по займу',
  accountError: 'Нет данных по данному пользователю',
  daysNotInAvailableRange: 'Срок продления займа не может быть больше указанной даты',
  daysDurationInvalidFormat: 'В поле срок продления займа указана не корректная дата'
}
