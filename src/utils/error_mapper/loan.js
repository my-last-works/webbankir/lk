export default {
  'invalidPromo': {
    code: 'invalidPromo',
    field: 'Промокод',
    message: 'Неверный промокод'
  },
  'tooManyTries': {
    code: 'tooManyTries',
    field: 'Промокод',
    message: 'Превышено максимальное количество попыток ввода промокода'
  },
  'codeIsNotCorrect': {
    field: 'код СМС',
    message: 'Вы ввели неверный СМС код'
  },
  'exceededLimitAttempts': {
    field: 'код СМС',
    message: 'Превышено максимальное число попыток, попробуйте позже'
  },
  'activeLoanError': {
    field: 'mainErrorBlock',
    message: 'У вас уже есть активный займ'
  },
  'smsCodeInvalid': {
    code: 'smsCodeInvalid',
    field: 'sms-код для подтверждения допсоглашения',
    message: 'Вы ввели неверный СМС код'
  },
  'mobilePhoneBlocked': {
    code: 'mobilePhoneBlocked',
    field: 'sms-код для подтверждения допсоглашения',
    message: 'Вы превысили количество попыток ввода смс-кода'
  },
    'Exceed the number  attempt': 'Превышено число попыток',
}
