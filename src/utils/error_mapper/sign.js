export default {
  'smsCodeInvalid': {
    code: 'smsCodeInvalid',
    field: 'sms-код для подтверждения допсоглашения',
    message: 'Вы ввели неверный СМС код'
  },
  'mobilePhoneBlocked': {
    code: 'mobilePhoneBlocked',
    field: 'sms-код для подтверждения допсоглашения',
    message: 'Вы превысили количество попыток ввода смс-кода'
  }
}
