export default {
  'wrong-promo-code': {
    code: 'wrong-promo-code',
    field: 'Промокод',
    message: 'Неверный промокод'
  }
}
