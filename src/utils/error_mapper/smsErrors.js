export default {
  doNotHaveActiveLoan: 'У вас нет активных займов',
  notAllowed: 'Вам недоступна амнистия',
  notHaveUnsignedSupplementary: 'У вас нет амнистии, ожидающей подписания',
  spamProtectionRestriction: 'Превышен лимит запросов',
  smsCodeIsEmpty: 'Не передан смс-код',
  smsCodeInvalidFormat: 'СМС-код должен содержать 4 цифры',
  mobilePhoneDoNotHaveActiveSmsCode: 'Срок действия смс-кода истек. Попробуйте запросить код еще раз',
  mobilePhoneBlocked: 'Вы превысили количество попыток ввода. Повторите попытку через 1 час',
  smsCodeInvalid: 'Введенный код не совпадает с высланным',
  notAvailableForSign: 'Нет доступных договоров для подписания',
  emptyCodeInDB: 'Код для подписания не найден. Запросите код повторно',
  mobilePhoneNotUnique: 'Данный телефон ранее зарегисрирован',
}
