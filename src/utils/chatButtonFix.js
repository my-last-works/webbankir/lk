// следим за домом, и если в него добавлен чат, то вешаем свой обработчик на него для изменения размеров
// стилями это сделать невозможно, потому что высота и ширина блока кнопка
// прописано строкой в аттрибуте style
const observer = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {
    mutation.addedNodes.forEach((node) => {
      if (node.classList && node.classList.contains('sender-connect')) {
        node.oldStyle = node.style.cssText
        node.style.cssText = node.style.cssText.replace('170px', '77px')
        node.style.setProperty("z-index", "1999999", "important")
		  observer.disconnect()
        clearTimeout(timeout)
      }
    })
  })
})
const config = {
  attributes: false,
  childList: true,
  characterData: false
}
observer.observe(document.querySelector('body'), config)
// если чат вообще не загрузится, то все равно очистим обсервер
const timeout = setTimeout(() => {
  observer.disconnect()
}, 60 * 1000)

// скрываем чат и кнопку погасить займ, если открыта клавиатура на мобильном телефоне
// если есть window.orientation, то это мобильное устройтсво.
// а значит по фокусу на инутпы, открывается клавиатура.
if (typeof window.orientation !== 'undefined') {
  document.addEventListener('focus', ({target}) => {
    const button = document.querySelector('.sender-connect')
    const block = document.querySelector('.main-button-block')
    if (target.tagName.toLowerCase() === 'input' && isInput(target.type)) {
      if (button) button.style.display = 'none'
      if (block) block.style.display = 'none'
    }
  }, true)

  document.addEventListener('blur', ({target}) => {
    const button = document.querySelector('.sender-connect')
    const block = document.querySelector('.main-button-block')
    if (target.tagName.toLowerCase() === 'input' && isInput(target.type)) {
      if (button) button.style.display = ''
      if (block) block.style.display = ''
    }
  }, true)

}

const isInput = (type) => {
  return ['email', 'tel', 'text', 'password', 'number'].includes(type.toLowerCase());
}
