/*
 *  ОТПРАВКА СОБЫТИЙ В АНАЛИТИКУ
 *
 *  Пример:
 *  WbEvent.send('EVENT_NAME')
 *
 */

/*eslint-disable */

export default function () {
  var GA = 'UA-45944839-1'
  var YM = '17582872'

  /**
     * Отправка события
     *
     * @param name
     * @param redirect
     */
  this.send = function (name, redirect) {
    if (!name) {
      return false
    }

    this.getCounters(name, redirect)

    return false
  }

  /**
     * Вызывает счетчики
     *
     * @param name
     * @param redirect
     */
  this.getCounters = function (name, redirect) {
    if (!name) {
      return false
    }

    var params = this.getEvent(name)

    var nameEvent = name || false
    var gaCategory = params['ga']['category'] || false
    var gaAction = params['ga']['action'] || false
    var gaLabel = params['ga']['label'] || false

    if (typeof ga !== 'undefined') {
      // var cookie_guid = $.cookie('google_uid') || false; // Вроде как уже не надо

      // Google Analytics
      ga('create', {
        trackingId: GA,
        name: 'b'
        // userId: cookie_guid // Вроде как уже на ндо
      })
      ga('b.send', 'event', gaCategory, gaAction, gaLabel, {
        'hitCallback': function () {
          // Yandex.Metrika
          if (typeof window['yaCounter' + YM] !== 'undefined') {
            window['yaCounter' + YM].reachGoal(nameEvent)
          }

          // Если передали ссылку для редиректа
          if (redirect) {
            window.location = redirect
          }
        }
      })
    }
  }

  /**
     * Возвращает массив параметров события (цели)
     *
     * @param name
     *
     * @returns {*}
     */
  this.getEvent = function (name) {
    if (!name) {
      return false
    }

    var getEventList = this.getEventList()

    if (!getEventList) {
      return false
    }

    return getEventList[name]
  }

  /**
     * Возвращает список событий (целей)
     *
     * @returns {*}
     */
  this.getEventList = function () {
    var list = {

      /* КЛИЕНТЫ */

      'CLIENT_NEXT_CONTACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Продолжить"',
          'label': 'Личная информация'
        }
      },

      'CLIENT_NEXT_ADDITIONAL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Продолжить"',
          'label': 'Дополнительная информация'
        }
      },

      'CLIENT_ADD_CARD_STEPS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Привязать карту"',
          'label': 'Добавление карты'
        }
      },

      'CLIENT_ADD_ANOTHER_CARD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Привязать другую банковскую карту"',
          'label': 'Добавление новой карты'
        }
      },

      'CLIENT_ADD_YANDEX_WALLET': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Привязать кошелек"',
          'label': 'Добавление кошелька Яндекс.Деньги'
        }
      },

      'CLIENT_ADD_QIWI_WALLET': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Привязать кошелек"',
          'label': 'Добавление кошелька QIWI.WALLET'
        }
      },

      'CLIENT_ADD_ANOTHER_YANDEX_WALLET': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Привязать другой кошелек"',
          'label': 'Добавление нового кошелька Яндекс.Деньги'
        }
      },

      'CLIENT_CHOICE_METHOD_CONTACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Выбор способа "CONTACT"',
          'label': 'Способ получения CONTACT'
        }
      },

      'CLIENT_TIE_SOCIAL_NETWORK_VK': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа ВК',
          'label': 'Привязка социальной сети ВК'
        }
      },

      'CLIENT_TIE_SOCIAL_NETWORK_OK': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа ОК',
          'label': 'Привязка социальной сети ОК'
        }
      },

      'CLIENT_TIE_SOCIAL_NETWORK_FB': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа FB',
          'label': 'Привязка социальной сети FB'
        }
      },

      'CLIENT_APPLY_LOAN_PRIMARY': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подать заявку на займ"',
          'label': 'Подача заявки первичная'
        }
      },

      'CLIENT_SIGN_CONTRACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подписать договор"',
          'label': 'Подписание договора'
        }
      },

      'CLIENT_SIGN_CONTRACT_RESEND_CODE': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Повторно получить код подтверждения"',
          'label': 'Запрос кода. Подписание договора'
        }
      },

      'CLIENT_VIEWING_CONTRACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Договор"',
          'label': 'Просмотр договора'
        }
      },

      'CLIENT_PRINT_CONTRACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Рапечатать договор"',
          'label': 'Печать договора'
        }
      },

      'CLIENT_SIGN_CONTRACT_OPEN_BLOCK': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Подписать договор" в графе "Статус займа"',
          'label': 'Переход к подписанию договора'
        }
      },

      'CLIENT_CONFIRM_EMAIL_SEND': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подтвердить"',
          'label': 'Попытка подтверждения e-mail'
        }
      },

      'CLIENT_REPAY_LOAN_OPEN_BLOCK': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Погасить займ"',
          'label': 'Просмотр способа погашения'
        }
      },

      'CLIENT_CLICK_PAY': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Оплатить" / "Квитанция" / "Погасить займ в 1 клик"',
          'label': 'Попытка оплаты'
        }
      },

      'CLIENT_EXTEND_LOAN_OPEN_BLOCK': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Продлить займ"',
          'label': 'Попытка продления займа'
        }
      },

      'CLIENT_BUTTON_FORMULATE_ADDITIONAL_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Сформировать допсоглашение"',
          'label': 'Сформировано доп. соглашение'
        }
      },

      'CLIENT_SIGN_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подписать допсоглашение"',
          'label': 'Подписание доп. соглашения'
        }
      },

      'CLIENT_CLICK_TERMS_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Условия допсоглашения"/"Допсоглашения"',
          'label': 'Ознакомление с условиями доп. соглашения'
        }
      },

      'CLIENT_RESEND_CODE_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Повторно получить код подтверждения"',
          'label': 'Повторное получение кода для подписи доп. соглашения'
        }
      },

      'CLIENT_PAY_BUTTON_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки кнопки/ссылки "Оплатить"',
          'label': 'Попытка оплаты доп. соглашения'
        }
      },

      'CLIENT_REPAY_LOAN_OPEN_BLOCK_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Погасить займ"',
          'label': 'Попытка погашения займа при сформированном доп. соглашении'
        }
      },

      'CLIENT_BUTTON_SIGN_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подписать"',
          'label': 'Попытка подписания доп. соглашения после того, как доп. соглашение было сформировано, был покинут раздел подписания'
        }
      },

      'CLIENT_BUTTON_VIEW_AGREEMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Просмотреть"',
          'label': 'Просмотр доп. соглашения после того, как доп. соглашение было сформировано, был покинут раздел подписания'
        }
      },

      'CLIENT_CHANGE_PHONE_OPEN_MODAL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Изменить"',
          'label': 'Попытка изменить телефон в ЛК'
        }
      },

      'CLIENT_CHANGE_PHONE_CONFIRM': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подтвердить" в модальном окне "Изменение номера мобильного телефона"',
          'label': 'Подтверждение изменения номера телефона в ЛК'
        }
      },

      'CLIENT_CHANGE_PHONE_CLICK_CHANGE': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Изменить номер телефона"',
          'label': 'Изменение номера телефона в ЛК, когда ожидается подтверждение ранее указанного номера'
        }
      },

      'CLIENT_CHANGE_PHONE_RESEND_CODE': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Запросить код повторно"',
          'label': 'Запрос кода. Подтверждение номера в ЛК'
        }
      },

      'CLIENT_CHANGE_PHONE_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подтвердить"',
          'label': 'Успешное изменение номера в ЛК'
        }
      },

      'CLIENT_CHANGE_EMAIL_OPEN_MODAL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Изменить"',
          'label': 'Попытка изменить e-mail в ЛК'
        }
      },

      'CLIENT_CHANGE_EMAIL_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Изменить"',
          'label': 'Изменение e-mail в ЛК'
        }
      },

      'CLIENT_SAVE_PROFILE_STEP': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Продолжить"/"Завершить заполнение"',
          'label': 'Изменение анкетных данных в ЛК'
        }
      },

      'CLIENT_COMPLAIN_MODAL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Пожаловаться на оператора"',
          'label': 'Попытка пожаловаться на оператора'
        }
      },

      'CLIENT_COMPLAIN_OPERATOR': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Отправить сообщение"',
          'label': 'Жалоба на оператора'
        }
      },

      'CLIENT_CHOOSE_CHANGE_PASSWORD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Выбор "Изменить пароль" в меню',
          'label': 'Попытка изменить пароль'
        }
      },

      'CLIENT_CHANGE_PASSWORD_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Сохранить"',
          'label': 'Изменение пароля'
        }
      },

      'CLIENT_LOGOUT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Выбор "Выход" в меню',
          'label': 'Выход из ЛК'
        }
      },

      'CLIENT_AUTH_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Войти"',
          'label': 'Авторизация'
        }
      },

      'CLIENT_TIE_VK_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа ВК',
          'label': 'Привязка ВК в ЛК'
        }
      },

      'CLIENT_TIE_OK_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа ОК',
          'label': 'Привязка ОК в ЛК'
        }
      },

      'CLIENT_TIE_FB_SUCCESS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие логотипа FB',
          'label': 'Привязка FB в ЛК'
        }
      },

      'CLIENT_ARCHIVE_PAGE': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "История займов"',
          'label': 'Просмотр истории займов'
        }
      },

      'CLIENT_ADD_CARD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Привязать карту"',
          'label': 'Попытка привязки карты в ЛК'
        }
      },

      'CLIENT_GET_CARD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Получить на карту"',
          'label': 'Привязка карты в ЛК'
        }
      },

      'CLIENT_TIE_NEW_YANDEX_WALLET': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Привязать новый яндекс кошелек"',
          'label': 'Попытка привязать яндекс кошелек в ЛК'
        }
      },

      'CLIENT_YANDEX_WALLET_SUCCESS_TIE': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Привязать"',
          'label': 'Привязка яндекс кошелька в ЛК'
        }
      },

      'CLIENT_MAKE_MAIN_CONTACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Сделать основным"',
          'label': 'Выбор CONTACT'
        }
      },

      'CLIENT_MAKE_MAIN_CARD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Сделать основной"',
          'label': 'Выбор Карты'
        }
      },

      'CLIENT_MAKE_MAIN_YANDEX_WALLET': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Сделать основным"',
          'label': 'Выбор яндекс кошелька'
        }
      },

      'CLIENT_INDENTIFICATION_YANDEX_WALLET_PA': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие ссылки "Привязать"',
          'label': 'Попытка идентификации яндекс кошелька в ЛК'
        }
      },

      'CLIENT_CLICK_PENCIL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие пиктограммы "карандаш"',
          'label': 'Редактирование имени способа получения'
        }
      },

      'CLIENT_YANDEX_WALLET_DELETED': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие удалить в модальном окне',
          'label': 'Удаление яндекс кошелька'
        }
      },

      'CLIENT_CARD_DELETED': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие удалить в модальном окне',
          'label': 'Удаление карты'
        }
      },

      'CLIENT_UNTIE_SOCIAL_NETWORK_PA': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Отвязать" в модальном окне',
          'label': 'Отвязка соц. сети в ЛК'
        }
      },

      'CLIENT_CANCEL_SOCIAL_NETWORK_PA': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Отмена" в модальном окне',
          'label': 'Попытка отвязать соц. сеть в ЛК'
        }
      },

      'CLIENT_UNTIE_SOCIAL_NETWORK_STEPS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подать заявку"',
          'label': 'Повторная подача заявки'
        }
      },

      'CLIENT_CANCEL_SOCIAL_NETWORK_STEPS': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Отмена" в модальном окне',
          'label': 'Попытка отвязать соц. сеть на этапе подачи заявки'
        }
      },

      'CLIENT_APPLY_LOAN_AGAIN': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие кнопки "Подать заявку"',
          'label': 'Повторная подача заявки'
        }
      },

      'CLIENT_ACTION_PAYMENT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Ответ от 1С о выдаче займа',
          'label': 'Выдача займа'
        }
      },

      'CLIENT_MAIL_CONFIRMATION_EMAIL': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Переход по ссылке из письма',
          'label': 'Подтверждение e-mail'
        }
      },

      'CLIENT_ACTION_PAYMENT_CONTACT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Фиксация выдачи на CONTACT',
          'label': 'Выдача на CONTACT'
        }
      },

      'CLIENT_ACTION_PAYMENT_CARD': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Фиксация выдачи на карту',
          'label': 'Выдача на карту'
        }
      },

      'CLIENT_ACTION_PAYMENT_YANDEX': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Фиксация выдачи на яндекс кошелек',
          'label': 'Выдача на яндекс кошелек'
        }
      },

      'CLIENT_CLICK_OPEN_CHAT': {
        'ga': {
          'category': 'Пользователи',
          'action': 'Нажатие "Чат"',
          'label': 'Онлайн помощь в ЛК'
        }
      },

      /* ПОСЕТИТЕЛИ */

      'USER_REGISTER_ATTEMPT': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие на кнопку "Продолжить"',
          'label': 'Попытка регистрации'
        }
      },

      'USER_REGISTER_NEXT': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие на кнопку "Продолжить"',
          'label': 'Регистрация ЛК'
        }
      },

      'USER_CHANGE_PHONE': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Изменить номер телефона"',
          'label': 'Изменение номера телефона'
        }
      },

      'USER_REQUEST_AGAIN_CODE': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Запросить код повторно"',
          'label': 'Запрос кода. Подтверждение номера'
        }
      },

      'USER_CLICK_PERSONAL_AREA': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие кнопки "Личный кабинет"',
          'label': 'Попытка авторизоваться'
        }
      },

      'USER_CLICK_RECEIVE_MONEY': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Получить деньги"',
          'label': 'Попытка подачи заявки через форму авторизации'
        }
      },

      'USER_CLICK_PASSRESTORE_PAGE_AUTH': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Восстановить"',
          'label': 'Попытка восстановить пароль на форме авторизации'
        }
      },

      'USER_SUCCESS_CHANGE_PASSWORD': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие кнопки "Восстановить пароль"',
          'label': 'Восстановление пароля'
        }
      },

      'USER_CLICK_PASSRESTORE': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Напомнить пароль"',
          'label': ''
        }
      },

      'USER_PASSRESTORE_EMAIL': {
        'ga': {
          'category': 'Посетители',
          'action': 'При выборе "По e-mail"',
          'label': 'Восстановление по e-mail'
        }
      },

      'USER_PASSRESTORE_LOGIN': {
        'ga': {
          'category': 'Посетители',
          'action': 'При выборе "По логину"',
          'label': 'Восстановление по логину'
        }
      },

      'USER_PASSRESTORE_PHONE': {
        'ga': {
          'category': 'Посетители',
          'action': 'При выборе "По телефону"',
          'label': 'Восстановление по моб. номеру'
        }
      },

      'USER_CLICK_GOOGLE_PLAY': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие на кнопку моб. прил. в верхней плашке',
          'label': 'Переход в GooglePlay'
        }
      },

      'USER_CLICK_APP_STORE': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие на кнопку моб. прил. в верхней плашке',
          'label': 'Переход в Appstore'
        }
      },

      'USER_CLICK_OPEN_CHAT': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие "Чат"',
          'label': 'Онлайн помощь'
        }
      },

      'USER_RECEIVE_MONEY_CALC': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие на кнопку "Оформить займ"/"Получить деньги"',
          'label': 'Переход с калькулятора'
        }
      },

      'USER_PERSONAL_AREA': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие ссылки "Личный кабинет"',
          'label': 'Переход в ЛК через калькулятор'
        }
      },

      'USER_INVESTOR_BUTTON': {
        'ga': {
          'category': 'Посетители',
          'action': 'Нажатие кнопки "Отправить Ваше сообщение"',
          'label': 'Инвестор. Отправка сообщения'
        }
      },

      'TAKE_LOAN': {
        'ga': {
          'category': '',
          'action': '',
          'label': ''
        }
      },

      'SUPATEST': {
        'ga': {
          'category': 'payture',
          'action': 'click',
          'label': 'take_loan'
        }
      }

    }

    return list
  }
}

/* eslint-enable */
