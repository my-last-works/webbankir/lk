export default {
	colors: {
		white: '#ffffff',
		
		statusGreen: '#51a351',
		statusRed: '#e63f4f',
		statusAwait: '#323c47',
		blue: '#4d92e3',
		gray: '#707070',
		yellow: '#ffd012',
		label: '#b7c6d0',
	},
	
	notifications: {
		timeout: 10000,
	},
};
