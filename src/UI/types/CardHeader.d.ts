import {VNode} from "vue";

export interface UiCardHeader_Slots {
    default: VNode[],
    header: VNode[]
    [key: string]: VNode[]
}

export declare class UiCardHeader {
    trigger: 'hover' | 'click'
    height: string
    disabled: boolean
    $slots: UiCardHeader_Slots
}
