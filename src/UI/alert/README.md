Props
====
| Название       | Тип     | Значение по умолчанию    | Описание   |
| :------------- | :----------: | -----------: |        --------:|
|  isExpanded    |  Boolean |  false  | Значение, может ли разворачиваться контент алерта
|  isSingleLine  |  Boolean  |   false  | Значение, может ли алерт быть в одну строку
|  type          |  String  |   ''  | Тип алерта, влияет на цвет фона и иконку в названии
|  alertTitle    |  String | '' | Текст верхнего титульного поля

Slots
===
| alert-body |
|:--

Содержимое контента
