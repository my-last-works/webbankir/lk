declare module "vue/types/vue" {
    interface Vue {
        $ui: {
            colors: {
                // #4d92e3
                info: string,
                // #51a351
                success: string,
                // #f74455
                error: string,
                // #ffd012
                warning: string,
                // #707070
                gray: string,
            },

            animations: {
                //animation time in ms
                duration: number,
            }
        }
    }
}
