export default {
    name: 'UiCol',
    
    props: {
        span: {
            type: Number,
            default: 24
        },
        tag: {
            type: String,
            default: 'div'
        },
        offset: Number,
        pull: Number,
        push: Number,
        xs: [Number, Object],
        sm: [Number, Object],
        md: [Number, Object],
        lg: [Number, Object],
        
        flex: {
            type: Boolean,
            default: false,
            validator: (v) => typeof v === "boolean"
        },
        
        alignItems: {
            type: String,
            default: '',
            validator: (v) => !v || ['center'].includes(v)
        },
        
        textRight: {
            type: Boolean,
            default: false,
        },
        
        wrap: {
            type: Boolean,
            default: false,
        }
    },
    
    computed: {
        gutter() {
            let parent = this.$parent;
            while (parent && parent.$options.componentName !== 'UiRow') {
                parent = parent.$parent;
            }
            return parent ? parent.gutter : 0;
        }
    },
    render(h) {
        let classList = [];
        let style = {};
        
        if (this.gutter) {
            style.paddingLeft = this.gutter / 2 + 'px';
            style.paddingRight = style.paddingLeft;
        }
        
        ['span', 'offset', 'pull', 'push'].forEach(prop => {
            if (this[prop] || this[prop] === 0) {
                classList.push(
                    prop !== 'span'
                        ? `el-col-${prop}-${this[prop]}`
                        : `el-col-${this[prop]}`
                );
            }
        });
        
        
        ['flex', 'alignItems', 'textRight', 'wrap'].forEach((prop) => {
            if(this[prop]) {
                if(typeof this[prop] === "string") {
                    classList.push(`display-flex-${prop}-${this[prop]}`);
                } else {
                    classList.push(`display-flex-${prop}`);
                }
            }
        });
        
        ['xs', 'sm', 'md', 'lg'].forEach(size => {
            if (typeof this[size] === 'number') {
                classList.push(`el-col-${size}-${this[size]}`);
            } else if (typeof this[size] === 'object') {
                let props = this[size];
                Object.keys(props).forEach(prop => {
                    classList.push(
                        prop !== 'span'
                            ? `el-col-${size}-${prop}-${props[prop]}`
                            : `el-col-${size}-${props[prop]}`
                    );
                });
            }
        });
        
        return h(this.tag, {
            class: ['el-col', classList],
            style
        }, this.$slots.default);
    }
};
