import Vue from 'vue';
import Checkbox from './checkbox';
import InlineHelp from './inline-help';
import Multiselect from './Multiselect/Multiselect';

import UiCard from './card';
import UiCardHeader from './card/card-header';
import UiCardBody from './card/card-body';
import UiCardFooter from './card/card-footer';
import UiCardExpand from './card/card-expand';
import UiIcon from './icon';
import UiButton from './button';
import UiInput from './input';
import UiRow from './row';
import UiCol from './col';
import UiDetailsCard from './details-card';
import UiHelpAlert from './help-alert';
import UiAlert from './alert';
import UiProgress from './progress';
import UiDialog from './dialog';
import UiRadioList from './radiolist';

Vue.prototype.$ui = {
    colors:  {
        info: '#4d92e3',
        success: '#51a351',
        error: '#f74455',
        warning: '#ffd012',
        gray: '#707070',
        primary: '#323c47'
    },
    
    animation: {
        duration: 300 //ms
    }
};

Vue.component('UiCheckbox', Checkbox);
Vue.component('UiInlineHelp', InlineHelp);
Vue.component('UiMultiselect', Multiselect);
Vue.component('UiCard', UiCard);

Vue.component('UiCardHeader', UiCardHeader);
Vue.component('UiCardBody', UiCardBody);
Vue.component('UiCardFooter', UiCardFooter);
Vue.component('UiCardExpand', UiCardExpand);
Vue.component('UiIcon', UiIcon);
Vue.component('UiButton', UiButton);
Vue.component('UiInput', UiInput);
Vue.component('UiRow', UiRow);
Vue.component('UiCol', UiCol);
Vue.component('UiDetailsCard', UiDetailsCard);
Vue.component('UiHelpAlert', UiHelpAlert);
Vue.component('UiAlert', UiAlert);
Vue.component('UiProgress', UiProgress);
Vue.component('UiDialog', UiDialog);
Vue.component('UiRadioList', UiRadioList);
