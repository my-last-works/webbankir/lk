export default {
    name: 'UiRow',
    
    componentName: 'UiRow',
    
    props: {
        tag: {
            type: String,
            default: 'div'
        },
        type: String,
        justify: {
            type: String,
            default: 'start'
        },
        align: {
            type: String,
            default: 'top'
        },
        block: {
            type: Boolean,
            default: false,
        }
    },
    
    computed: {
        style() {
            const ret = {};

            if (this.gutter) {
                ret.marginLeft = `-${this.gutter / 2}px`;
                ret.marginRight = ret.marginLeft;
            }

            return ret;
        },

        gutter () {
            return 0; //this.$interface.viewport === 'mobile' ? 0 : 0;
        }
    },
    
    render(h) {
        return h(this.tag, {
            class: [
                'el-row',
                this.justify !== 'start' ? `is-justify-${this.justify}` : '',
                this.align !== 'top' ? `is-align-${this.align}` : '',
                { 'el-row--flex': this.type === 'flex' },
                { 'el-row--block': this.block }
            ],
            style: this.style
        }, this.$slots.default);
    }
};
