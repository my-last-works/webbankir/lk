Props
====
| Название       | Тип     | Значение по умолчанию    | Описание   |
| :------------- | :----------: | -----------: |        --------:|
|  customClasses    |  [String, Function] |  ''  | Кастомные классы для кнопки
|  disabled  |  Boolean  |   false  | Значение, может ли алерт быть в одну строку
|  underline          |  Boolean  |   false  | Есть ли подчеркивание
|  center    |  Boolean | false | Текст по центру
|  link    |  Boolean | false | Стилизация под ссылку
|  bold    |  Boolean | false | Жирный текст шрифта
|  block    |  Boolean | false | Растягивание на всю ширину контента
|  color    |  String | '' | Цвет фона кнопки

Slots
====
| default |
|:--

Стандартный слот
