import cardValidator from 'card-validator';

export const cardCheckSum = (number) => {
    const check = cardValidator.number(number);
    
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(check.isValid);
        }, 1000)
    })
};

export const cardCheckDate = (date) => {
    const dateRegex = /^\d{2}\/\d{2}$/;
    return new Promise((resolve) => {
        setTimeout(() => {
            if(!dateRegex.test(date)) resolve(false);
            const [m, y] = date.split('/');
            resolve(new Date('20' + y, m-1) >= new Date());
        }, 1000)
    })
};

export const cardCheckCvv = (cvv) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(/^\d{3}$/.test(cvv));
        }, 1000)
    })
};
