const mapRU = {
    'q': 'й',
    'w': 'ц',
    'e': 'у',
    'r': 'к',
    't': 'е',
    'y': 'н',
    'u': 'г',
    'i': 'ш',
    'o': 'щ',
    'p': 'з',
    '[': 'х',
    '{': 'Х',
    ']': 'ъ',
    '}': 'Ъ',
    '|': '/',
    '`': 'ё',
    '~': 'Ё',
    'a': 'ф',
    's': 'ы',
    'd': 'в',
    'f': 'а',
    'g': 'п',
    'h': 'р',
    'j': 'о',
    'k': 'л',
    'l': 'д',
    ';': 'ж',
    ':': 'Ж',
    '\'': 'э',
    '"': 'Э',
    'z': 'я',
    'x': 'ч',
    'c': 'с',
    'v': 'м',
    'b': 'и',
    'n': 'т',
    'm': 'ь',
    ',': 'б',
    '<': 'Б',
    '.': 'ю',
    '>': 'Ю',
    '/': '.',
    '?': ',',
    '@': '"',
    '#': '№',
    '$': ';',
    '^': ':',
    '&': '?',
};

export function convert () {
    const reverse = { };
    const full = { };
    const values = Object.values(mapRU);
    const keys = Object.keys(mapRU);
  
    for (let i = keys.length; i--;) {
        full[keys[i].toUpperCase()] = values[i].toUpperCase()
        full[keys[i]] = values[i]
    }
    
    for(let i in full) {
        reverse[full[i]] = i
    }
    
    return {
        fromEn: function (str) {
            return str.replace(/./g, function (ch) {
                return full[ch] || ch
            })
        },
        toEn: function (str) {
            return str.replace(/./g, function (ch) {
                return reverse[ch] || ch
            })
        }
    }
};
