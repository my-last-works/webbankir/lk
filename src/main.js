// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// ...
require('./polyfill/closest');


import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import store from './store'
import 'normalize.css'
import VModal from 'vue-js-modal'
import VeeValidate from './helpers/vee-validate/dist/vee-validate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {
  mobilePhone,
  standartMobilePhone,
  cyrillic,
  latin,
  exceptLatinAndServiceChar,
  passportCode,
  serialAndNumber,
  snils,
  addressAndLocation,
  addressIndex,
  bankCard,
  maestroCard,
  requiredSelect,
  nonStandart,
  inn,
  holderNameValidate,
  cardNumberValidate,
  cvvValidate,
  cvv,
  date_between_bd,
  date_format_check,
  date_between_passport,
  date_passport_validate

} from '@/components/Validators/CustomVeeValidators'
import ru from 'vee-validate/dist/locale/ru';
import ValidateMessage from '@/components/Validators/ValidateMessage';
import ErrorBlock from '@/components/Validators/ErrorBlock';
import VuexErrorBlock from '@/components/Validators/VuexErrorBlock';
import AjaxLoader from '@/base/Loader';
import Multiselect from '@/UI/Multiselect';
import VueMask from 'v-mask';
import VTooltip from 'v-tooltip';
import WbEvent from './utils/WbEvent';

import * as cookie from 'tiny-cookie';
import {mapGetters, mapMutations} from 'vuex';
import dayjs from 'dayjs';
import theme from './theme.js';
import Notifications from '@/components/Notifications/main.js';
import UserClass from '@/classes/user.class';
import LoanClass from '@/classes/loan.class';
import ga from '@/classes/ga.class';
import'./UI';
import Vuelidate from 'vuelidate'
window.Analityc = new ga();


Vue.use(Vuelidate)

require('./utils/chatButtonFix');

// disable console.log for production
const regex = /webbankir\.com$/i;
if (regex.test(document.location.hostname) && !localStorage.getItem('enableConsoleLog')) {
  window.console.log = () => {};
  window.console.warn = () => {};
  window.console.error = () => {};
  window.console.info = () => {};
  window.console.vuex = () => {};
  window.console.group = () => {};
  window.console.groupEnd = () => {};
  window.console.groupCollapsed= () => {};
}

Vue.config.productionTip = !regex.test(document.location.hostname);

// Vue uses
Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(VModal, {
  dynamic: true
})
Vue.use(VueMask)
Vue.use(VTooltip)

// prototypes
Vue.prototype.$methods = {
	user: new UserClass(),
	loan: new LoanClass(),
};
Vue.prototype.$theme = theme;
Vue.prototype.$backend = require('./utils/backend.js').default;
Vue.prototype.$helpers = require('./helpers/index.js').default;
Vue.prototype.$date = require('./helpers/date.js');
Vue.prototype.$dayjs = dayjs;
Vue.prototype.$getErrorCode = (e) => {
  if (typeof e === 'string') {
    e = JSON.parse(e)
  }
  if(e && Array.isArray(e.errors) && typeof e.errors[0] === "object") {
    return e.errors[0].code
  } else {
    return false
  }
}

Vue.prototype.$cookie = cookie //https://github.com/Alex1990/tiny-cookie#apis
Vue.prototype.$events = new Vue();
Vue.prototype.$isMain = regex.test(document.location.host) || localStorage.getItem('enableConsoleLog');
Vue.prototype.$error = function (response, map, default_message  = 'Произошла ошибка') {
  if(Array.isArray(response.errors) && response.errors[0].code) {
    const message = map[response.errors[0].code];
    this.$toastr.error(message || default_message, 'Ошибка');
  } else {
    this.$toastr.error(default_message, 'Ошибка');
  }
}
Vue.prototype.$toastr = Notifications;
// if (document.location.hostname === 'localhost') {
//   Vue.config.errorHandler = (err, vm, info) => {
//     console.error(err)
//     debug.send({
//       ...debug.meta(),
//       err: {
//         name: err.name,
//         message: err.message,
//         stack: err.stack
//       }, info
//     })
//   }
// }


/* validators */
Vue.use(VeeValidate, {
  errorBagName: 'vErrors', // Изменено, так как уже есть свойсво 'error', используемое по умолчанию во VeeValidate
  locale: 'ru',
  events: 'input|blur',
  delay: 2000,
});

VeeValidate.Validator.extend('cyrillic', cyrillic);
VeeValidate.Validator.extend('latin', latin);
VeeValidate.Validator.extend('passportCode', passportCode);
VeeValidate.Validator.extend('serialAndNumber', serialAndNumber);
VeeValidate.Validator.extend('exceptLatinAndServiceChar', exceptLatinAndServiceChar);
VeeValidate.Validator.extend('snils', snils);
VeeValidate.Validator.extend('mobilePhone', mobilePhone);
VeeValidate.Validator.extend('standartMobilePhone', standartMobilePhone);
VeeValidate.Validator.extend('addressAndLocation', addressAndLocation);
VeeValidate.Validator.extend('addressIndex', addressIndex);
VeeValidate.Validator.extend('bankCard', bankCard);
VeeValidate.Validator.extend('maestroCard', maestroCard);
VeeValidate.Validator.extend('requiredSelect', requiredSelect);
VeeValidate.Validator.extend('nonStandart', nonStandart);
VeeValidate.Validator.extend('inn', inn);
VeeValidate.Validator.extend('holderNameValidate', holderNameValidate);
VeeValidate.Validator.extend('cardNumberValidate', cardNumberValidate);
VeeValidate.Validator.extend('cvvValidate', cvvValidate);
VeeValidate.Validator.extend('cvv', cvv);
VeeValidate.Validator.extend('date_between_bd', date_between_bd);
VeeValidate.Validator.extend('date_format_check', date_format_check);
VeeValidate.Validator.extend('date_between_passport', date_between_passport);
VeeValidate.Validator.extend('date_passport_validate', date_passport_validate);

ru.messages = { // Переопредение некоторых сообщений
  ...ru.messages,
  credit_card: 'Недействительный номер карты',
  required: 'Поле обязательно для заполнения',
  email: 'Поле email должно быть действительным электронным адресом',
  date_format: 'Дата указывается ДД.ММ.ГГГГ'
};
VeeValidate.Validator.localize('ru', ru);

// import css
require('../static/css/main.scss');
require('../static/css/skeleton-lk.scss');
require('../static/css/multiselect.css');
require('../static/css/slidermobiwidget.css');
require('../static/css/additional-style.css');
require('../static/css/footer.css');
require('../static/css/pos.css');
require('../static/css/owl.carousel.css');
require('../static/css/owl.theme.default.css');
require('../static/css/Animate.scss');

// WbEvents from old LK
window.wbEvent = new WbEvent();

// Global components register
Vue.component('validate-message', ValidateMessage);
Vue.component('ajax-loader', AjaxLoader);
Vue.component('error-block', ErrorBlock);
Vue.component('vuex-error-block', VuexErrorBlock);
Vue.component('multiselect', Multiselect);

// Global components register
Vue.mixin({
  methods: {
    openPopup (target) {
      window.open(target, '', 'width=600,height=800')
    },
    transitionEnter (el) {
      console.log('transitionEnter el', el);
      this.$helpers.scroll(el)
    },
    ...mapMutations([
      'AJAX_LOADING',
      'BUTTON_LOADING'
    ]),
    currencyFilter (n) {
      const sum = parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").replace(' ', ' ');
      const [rub, kop] = sum.split('.');
      if (kop === "00") {
        return rub
      } else {
        return sum;
      }
    }
  },

  filters: {
    currency(n)   {
      const sum = parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").replace(' ', ' ');
      const [rub, kop] = sum.split('.');
      if (kop === "00") {
        return rub
      } else {
        return sum;
      }
    }
  },

    // todo вывести из под комментариев $validate

  computed: {
    $validate() {
      return new Proxy({...this.$user.validate}, {
        get (obj, key) {
          if(!obj[key]) {
            // console.warn(`${obj[key]} (${key}) not found in APP.$user.validate`);
            return undefined;
          }
          return Object.keys(obj[key]).every(v => obj[key][v])
        }
      })

    },
    ...mapGetters([
      '$pdl',
      '$pos',
      '$posServices',
      '$calculate',
      '$history',
      '$prolongation',
      '$payment',
      '$user',
      '$auth',
      '$accounts',
      '$installment',
      '$social',
      '$interface',
      '$config',
      '$pay',
      '$errors',
      '$graphicInstallment',
      '$loanType',
      '$qiwi',
        '$service',
        '$notifications',
    ]),
  }
});

// Prototype redefines
Date.prototype.addHours = function (h) {
  this.setTime(this.getTime() + (h * 60 * 60 * 1000));
  return this
};


let domain;
const expires = 30;
if (regex.test(document.location.hostname)) {
    domain = '.webbankir.com';
} else {
    domain = document.location.hostname;
}

/**
 * при наличии utm[_]source, в любом случае нужно делать куки реферала на 30 дней.
 */
const expires_value =  parseInt(new Date().getTime() / 1000);

if (document.location.href.indexOf('utm_source') !== -1 || document.location.href.indexOf('utmsource') !== -1) {
  cookie.set('site_referer', btoa(document.location.href), null, {
      expires,
      domain
  });
  cookie.set('site_referer_expire', expires_value, null, {
      expires,
      domain
  })
} else {
  /**
     * если нет реферала, но есть кука , обновляем ее время жизни на 30 дней.
     */
    if(cookie.get('site_referer') && !cookie.get('site_referer_expire')) {
        const value = cookie.get('site_referer');
        cookie.set('site_referer', value, null, { expires, domain});
        cookie.set('site_referer_expire', expires_value, null,{ expires, domain});
    }

    /**
     * если нет ни реферала, ни куки, то ставим либо реферера, либо текущую страницу
     */
    else if(!cookie.get('site_referer')) {
        cookie.set('site_referer',
            btoa(document.referrer || document.location.toString()),
            null,
            { expires, domain });
        cookie.set('site_referer_expire', expires_value, null, { expires, domain});

    }
}

const app = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
});

/**
 * на тестах добавим APP в глобальную видимость
 */
if (!regex.test(document.location.host)) {
  window.APP = app
}
