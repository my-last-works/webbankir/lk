import backend from '@/utils/backend-api';

/**
 * @type iBackend.api
 */
const api = backend.api;
export default class CardPaymentProcess {
    
    constructor(vm, card = null, account = null) {
        this.vm = vm;
        this.card = card;
        this.account = account;
    }
    
    async pay () {
        const method = this.card ? 'sendCardData' : 'sendAccountData';
        return this[method]().then(() => {
            this.success();
        }).catch(() => {
            this.error();
        })
       
    }
    
    success () {
        this.vm.success();
    }
    
    error () {
        this.vm.error();
    }
    
    async sendCardData () {
        return await new Promise((resolve, reject) => {
            api.payment.pay.card({
                card: this.card,
                ...this.vm.$store.getters.paymentJSON
            }).then(async ({data}) => {
                if(data?.data?.amount) {
                    resolve(true);
                    this.vm.success();
                } else if (data?.data?.type === 'tds') {
                    this.vm.AJAX_LOADING(false);
                    await this.vm.tdsConfirm(data.data).then(() => {
                        resolve(true);
                    }).catch((e) => {
                        reject(false);
                    }).finally(() => {
                        this.vm.loading = false;
                    })
                } else {
                    reject(false);
                }
            }).catch((e) => {
                console.warn(e);
                this.vm.error(e);
                reject(false);
            });
        })
    }
    
    async sendAccountData () {
        return api.payment.pay.token({
            accountId: this.account,
            ...this.vm.$store.getters.paymentJSON
        }).then(({data}) => {
            console.log(data);
            this.vm.success();
        }).catch((e) => {
            console.warn(e);
            this.vm.error(e);
        })
    }
    
}
