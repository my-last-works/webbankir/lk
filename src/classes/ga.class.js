/**
 * @type iGaClass
 */
import config from '@/config'
export default class ga {
	constructor () {
	
	}
	
	
	reg_step1 () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/step1'
			});
			console.log('GA::reg_step1');
		}
	}
	reg_step1a () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/step1a'
			});
			console.log('GA::reg_step2');
			
		}
	}
	reg_step2 () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/step2'
			});
		}
	}
	reg_step3 () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/step3'
			});
		}
	}
	reg_step4 () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/step4'
			});
		}
	}
	reg_success () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/registration/success'
			});
		}
	}
	
	second_calc () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/my-loan/step1'
			});
		}
	}
	second_success () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': 'v-url/lk/my-loan/success'
			});
		}
	}
	
	prolongation_info () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': '/v-url/lk/my-loan/prolongation/step1'
			});
		}
	}
	prolongaction_create () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': '/v-url/lk/my-loan/prolongation/step2'
			});
		}
	}
	prolongation_sign () {
		if(typeof window.ga === "function") {
			window.ga('config', config.GA, {
				'page_path': '/v-url/lk/my-loan/prolongation/step3'
			});
		}
	}
}
