import store from '@/store';
export default class Loan {
	constructor () {
	
	}
	
	
	getLoans () {
		return new Promise((resolve, reject) => {
			
			const promises = [];
			promises.push(store.dispatch('GET_ALL_LOANS').catch((e) => {
				console.warn('Loan::getLoans.GET_ALL_LOANS', e);
			}));
			
			promises.push(store.dispatch('POS_CHECK_USER').then((result) => {
				store.commit('INTERFACE_SHOW_INSTALLMENT', result);
				return Promise.resolve();
			}));
			
			
			return Promise.all(promises).then(() => {
				resolve();
			}).catch((e) => {
				console.warn('Loan::getLoans', e);
				reject('load-data-error');
			})
		})
	}
}
