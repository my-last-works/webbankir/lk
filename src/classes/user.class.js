import store from '@/store';
import router from '@/router';
import backend from '@/utils/backend';
import Vue from 'vue';

/**
 * @type iBackend.api
 */
const api = backend.api;

export default class User {
	constructor() {
	
	}
	
	
	login({login, password}) {
		
		const data = {
			login,
			password,
			site_referer: Vue.prototype.$cookie.get('site_referer')
		}
		
		return new Promise((resolve, reject) => {
			api.user.login(data).then(({data}) => {
				console.log('User::login', data);
				if(typeof data?.data === "object") {
					this.setToken(data.data);
					this.getUserData().then(() => {
						resolve();
					}).catch((e) => {
						console.warn(e);
						reject(e);
					})
				}
			}).catch((e) => {
				console.warn('User::login', e);
				
				if(Array.isArray(e.errors)) {
					reject(e.errors[0]?.code);
				} else {
					reject('server-error');
				}
			})
		})
	}
	
	
	setToken(data) {
		console.groupCollapsed('User::setToken');
		console.table(data);
		console.groupEnd();
		
		localStorage.setItem('token', data.token);
		localStorage.setItem('token-expired', data.tokenExpired);
		localStorage.setItem('token-userid', data.userId);
		
		store.commit('AUTH_SUCCESS', {
			data,
			authState: 'authorized'
		})
	}
	
	getUserData () {
		return new Promise((resolve, reject) => {
			api.user.get().then(({data}) => {
				console.log('User::getUserData', data);
				store.commit('USER_SET_DATA', data);
				resolve();
			}).catch((e) => {
				console.warn('User::getUserData', e);
				if(Array.isArray(e.errors)) {
					reject(e.errors[0]?.code);
				} else {
					reject('server-error');
				}
			})
		})
	}
	
	/**
	 *
	 * После авторизации метод направляет на целевую для пользователя страницу.
	 *
	 * Если есть pdl или installment, то на страницу my-loan
	 * Если есть pos займ (целевое кредитование), то на страницу целевого кредитования.
	 *
	 * Если у пользователя нет займов в истории, и отсутствует pos, то мы кидаем его на страницу регистрации (клиент не может войти в лк, если у него нет и не было ни одной услуги).
	 *
	 * @warning Для корректной работы этот метод должен вызываться исключительно после загрузки всех данных о пользователе (займы, данные пользователя, истории займов) из-за сложной логики обработки входа в ЛК.
	 */
    redirect (name = '', query = {}) {
        console.log('User::redirect');
        const {$posServices, $pdl, $installment, $history} = store.getters;

        /**
         * если есть займ пдл или история займов пдл. по старому апи, займы доп.услуг, тоже относятся к пдл.
         */
        if($posServices?.length || $pdl?.id) {
            console.log('User::redirect pdl.active');
            return router.push(name ? {name, query} : {name: 'my-loan', query});
        }

        if($installment.payment.id) {
            console.log('User::redirect pos', name);
            return router.push(name ? {name, query} : {name: 'installment', query});
        }

        if($history?.length) {
            console.log('User::redirect pdl.new');
            return router.push(name ? {name, query} : {name: 'my-loan', query});
        }


        console.log('User:redirect registration')
        if(name !== 'registration') {
            this.logout();
        }
        router.push({name: 'registration'});
    }
	
	/**
	 * Очистка vuex-state
	 */
	logout () {
		this.clearTokenData();
		store.dispatch('AUTH_LOGOUT');
		store.commit('CLEAR_PAYMENT_DATA');
		store.commit('USER_RESET');
		store.commit('CLEAR_STATE');
	}
	
	/**
	 * Очистка всех данных токена из localStorage
	 */
	clearTokenData () {
		['token', 'token-exipred', 'token-userid'].forEach((key) => {
			localStorage.removeItem(key);
		})
	}
	
	
	tokenExists () {
		const token = localStorage.getItem('token');
		const tokenExpired = localStorage.getItem('token-expired');
		
		if(!token || !tokenExpired) {
			console.log('%cAuth data not found', 'color: orange');
			this.clearTokenData();
			return false;
		}
		
		const now = Math.round(new Date().getTime() / 1000);
		
		console.log(now, Number(tokenExpired));
		if(now > Number(tokenExpired)) {
			this.clearTokenData();
			return false;
		}
		
		return true;
	}
}
