export const REQUIRED_FIELD = {
  ru: {
    messages: {
      required: () => `Обязательное поле`
    }
  }
}

export const MIN_LENGHT = {
  ru: {
    messages: {
      min: (field, params) => (params[0] >= '2' && params[0] <= '4') ? `В поле должно быть минимум ${params[0]} символа` : `В поле должно быть минимум ${params[0]} символов`
    }
  }
}

export const LENGHT = {
  ru: {
    messages: {
      length: (field, params) => `Поле ${field} должно состоять ${params[0]} символов`
    }
  }
}

export const MIN_NUMBER_BC = {
  ru: {
    messages: {
      min: (field, params) => `Номер карты состоит как мининмум из ${params[0] - 3} символов`
    }
  }
}

export const MAX_LENGHT = {
  ru: {
    messages: {
      max: (field, params) => `Значение не может быть больше ${params[0]} символов`
    }
  }
}

export const MAX_LENGHT_MESSAGE = {
  ru: {
    messages: {
      max: (field, params) => `Количество символов в поле ${field} не должно привышать ${params[0]}`
    }
  }
}

export const ONLY_NUMERIC = {
  ru: {
    messages: {
      numeric: (field, params) => `Значение должно собдержать только цифры`
    }
  }
}
