import cv from 'card-validator';
import dayjs from 'dayjs';
import store from '../../store';
/* eslint-disable */
export const cyrillic = {
  getMessage: field => 'Необходимо использовать русские буквы',
  validate: value => /^[а-яА-Я\sёЁ\.,-]*$/.test(value)
}

export const latin = {
  getMessage: field => 'Необходимо использовать английские буквы',
  validate: value => /^[A-Za-z0-9\s@\.-]*$/.test(value)
}

export const exceptLatinAndServiceChar = {
  getMessage: () => 'Разрешено вводить только русские буквы, цифры, дефисы, точки и пробелы',
  validate: value => /^[\\\\/а-яё\d \.\№-]+$/iu.test(value)
}

export const addressAndLocation = {
  getMessage: () => 'Разрешено вводить только русские буквы, цифры, дефисы, точки, запятые, пробелы и скобки',
  validate: value => /^[а-яё\d \.\,\;\(\)\:-]+$/iu.test(value)
}

export const addressIndex = {
  getMessage (field) {
    return 'Индекс должен состоять из 6 цифр'
  },
  validate: value => {
    return value.match(/\d/g).join('').length === 6
  }
}

export const passportCode = {
  getMessage: () => 'Необходимо ввести 6 цифр',
  validate: value => /\d{3}-\d{3}/iu.test(value)
}

export const serialAndNumber = {
  getMessage: () => 'Необходимо ввести 10 цифр',
  validate: value => /\d{4} \d{6}/iu.test(value)
}

export const snils = {
  getMessage (field) {
    return 'Вы ввели некорректный снилс'
  },
  validate: snils => {
    snils = snils.replace(/[^\d]/g, '')
    let error = {}
    var result = false;
    if (typeof snils === 'number') {
      snils = snils.toString();
    } else if (typeof snils !== 'string') {
      snils = '';
    }
    if (!snils.length) {
      error.code = 1;
      error.message = 'СНИЛС пуст';
    } else if (/[^0-9]/.test(snils)) {
      error.code = 2;
      error.message = 'СНИЛС может состоять только из цифр';
    } else if (snils.length !== 11) {
      error.code = 3;
      error.message = 'СНИЛС может состоять только из 11 цифр';
    } else {
      var sum = 0;
      for (var i = 0; i < 9; i++) {
        sum += parseInt(snils[i]) * (9 - i);
      }
      var checkDigit = 0;
      if (sum < 100) {
        checkDigit = sum;
      } else if (sum > 101) {
        checkDigit = parseInt(sum % 101);
        if (checkDigit === 100) {
          checkDigit = 0;
        }
      }
      if (checkDigit === parseInt(snils.slice(-2))) {
        result = true;
      } else {
        error.code = 4;
        error.message = 'Неправильное контрольное число';
      }
    }
    console.log(error)
    return result;
  }
}

export const inn = {
  getMessage (field) {
    return 'Вы ввели некорректный ИНН'
  },
  validate: inn => {
    let error = {}
    inn = inn.replace(/[^\d]/g, '')

    console.log('validate inn', inn)
    var result = false;
    if (typeof inn === 'number') {
      inn = inn.toString();
    } else if (typeof inn !== 'string') {
      inn = '';
    }
    if (!inn.length) {
      error.code = 1;
      error.message = 'ИНН пуст';
    } else if (/[^0-9]/.test(inn)) {
      error.code = 2;
      error.message = 'ИНН может состоять только из цифр';
    } else if ([10, 12].indexOf(inn.length) === -1) {
      error.code = 3;
      error.message = 'ИНН может состоять только из 10 или 12 цифр';
    } else {
      var checkDigit = function (inn, coefficients) {
        var n = 0;
        for (var i in coefficients) {
          n += coefficients[i] * inn[i];
        }
        return parseInt(n % 11 % 10);
      };
      switch (inn.length) {
        case 10:
          var n10 = checkDigit(inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
          if (n10 === parseInt(inn[9])) {
            result = true;
          }
          break;
        case 12:
          var n11 = checkDigit(inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
          var n12 = checkDigit(inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
          if ((n11 === parseInt(inn[10])) && (n12 === parseInt(inn[11]))) {
            result = true;
          }
          break;
      }
      if (!result) {
        error.code = 4;
        error.message = 'Неправильное контрольное число';
      }
    }
    return result;
  }
}

export const bankCard = {
  getMessage (field) {
    return 'Номер должен состоять из 16 символов'
  },
  validate: (value) => {
    return value.match(/\d/g).join('').length === 16
  }
}

export const maestroCard = {
  getMessage (field) {
    return 'Номер должен состоять из 19 символов'
  },
  validate: value => {
    return value.match(/\d/g).join('').length === 19
  }
}

export const mobilePhone = {
  getMessage (field) {
    return 'Вы ввели некорректный телефон'
  },
  validate: value => {
    value = value.toString()
    let phone = value.match(/\d/g);
    if(phone)
      phone = phone.join('').replace('7', '')

    if (!phone) {
      return false
    }
    // console.log(phone.length, phone.length)
    return phone.length === 10 // && phone.startsWith('9')
  }
}

export const standartMobilePhone = {
  getMessage (field) {
    return 'Номер телефона введен не полностью'
  },
  validate: value => {
    value = value.toString()
    let phone = value.replace(/[^\d]/g, '').replace(/^8/, '')
    let regex = /^9[\d+]{9,}$/
    return regex.test(phone)
  }
}

export const requiredSelect = {
  getMessage (field) {
    return 'Выберите ' + field
  },
  validate: value => {
    return !!value
  }
}

export const nonStandart = {
  getMessage: field => `Поле ${field} не соответствует ни одному из указанных типов`,
  validate: value => /^[а-яё\d \.\№-]+$/iu.test(value)
}


export const holderNameValidate =  {
  getMessage: (field, args) => 'Имя держателя карты введено не верно',
  validate: (value, args) => {
    if (!value.trim()) return true;
    const regex = /^[a-zA-Z\-\s]*$/;
    return regex.test(value);
  }
}


export const cardNumberValidate = {
  getMessage: field => `Неверно введен номер карты`,
  validate: (value, args) => {
    return cv.number(
      value.replace(/[^\d]/g, '')
    ).isValid;
  }
}

/**
 * Правило описывающее валидацию CVV/CVC кодов банковских карт
 * @type {{ getMessage: (function(): string), validate: (function(string): boolean) }}
 */
export const cvvValidate = {
  getMessage: () => 'CVV/CVC должен состоять из трех цифр',
  validate: (v) => /[0-9]{3}/i.test(v)
};

/**
 * Упрощенный вариант валидации CVV/CVC кодов
 * @type {{ getMessage: (function(): string), validate: (function(string): boolean) }}
 */
export const cvv = Object.assign({}, cvvValidate, {
  validate: (v) => /(.){3}/i.test(v)
});




export const date_between_bd = {
  getMessage: (field, args) => 'Вам должно быть больше 19 и меньше 100 лет',
  validate: (dateCheck, args) => {
    const dateFrom = dayjs().subtract(100, 'year').toDate();
    const dateTo = dayjs().subtract(19, 'year').toDate();

    const [d, m, y] = dateCheck.split('.');

    const check = new Date(y, m-1, d);
    return (check <= dateTo && check >= dateFrom);
  }
}

export const date_between_passport = {
  getMessage: (field, args) => 'Дата выдачи паспорта должна быть с 25.12.1991 до ' + dayjs().format('DD.MM.YYYY'),
  validate: (dateCheck, args) => {
    const dateFrom = new Date(1991, 11, 25);
    const dateTo = new Date();
    const [d, m, y] = dateCheck.split('.');
    const check = new Date(y, m-1, d);
    return (check <= dateTo && check >= dateFrom);
  }
}


export const date_format_check = {
  getMessage: (field, args) => 'Дата указывается в формате ДД.ММ.ГГГГ',
  validate: (date) => {
    const test_regex = /^[\d]{2}\.[\d]{2}\.[\d]{4}$/
    if (!test_regex.test(date)) {
      return false;
    }

    const [d, m, y] = date.split('.');
    if (d > 31) return false;
    if (m > 12) return false;

    return true;
  }
}

export const date_passport_validate = {
  getMessage: (field, args) => 'На момент выдачи паспорта вам должно было быть не менее 14 лет',
  validate: (date) => {
    console.log('validate date', date);
    const test_regex = /^[\d]{2}\.[\d]{2}\.[\d]{4}$/;
    const $user = store.getters.$user;
    if (!test_regex.test(date) || !test_regex.test($user.bDay)) {
      console.log('date_passport_validate', date, $user.bDay)
      return true;
    }
    const [d, m, y] = date.split('.');
    const [bd, bm, by] = $user.bDay.split('.');

    const passportDate = new Date(y, m - 1, d);
    const bDate = new Date(by, bm - 1, bd);

    const diff = dayjs(passportDate).diff(dayjs(bDate), 'year');
    console.log('diff', diff);
    return diff >= 14;
  }

}
