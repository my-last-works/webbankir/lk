const helper = require('../../helpers/index.js').default;
export const focus = () => {
  try {
    const errors = [].slice.call(document.querySelectorAll('p.red')).filter((item) => item.style.display !== 'none')
    if (errors.length === 0) {
      console.info('no errors found')
      return
    }
    const block = errors[0].closest('.columns')
    if (block.querySelector('input.error')) {
      block.querySelector('input.error').focus()
    } else {
      helper.scroll(block.querySelector('input.error'));
    }
  } catch (e) {
    console.warn('Field focus error: ' + e.message)
  }
}
