import store from '@/store';
export default {
    success: function (text, title = 'Успешно') {
        store.commit('NOTIFICATION_MESSAGE_TEXT', text);
        store.commit('NOTIFICATION_MESSAGE_TITLE', title);
        store.commit('NOTIFICATION_MESSAGE_TYPE', 'success');
        store.commit('NOTIFICATION_SHOW');
    },

    error: function (text, title = 'Ошибка', options = {}) {
        store.commit('NOTIFICATION_MESSAGE_TEXT', text);
        store.commit('NOTIFICATION_MESSAGE_TITLE', title);
        store.commit('NOTIFICATION_MESSAGE_TYPE', 'error');
        store.commit('NOTIFICATION_SHOW');
        
        if(typeof options === "object") {
            if(options.buttons) {
                store.commit('NOTIFICATIONS_BUTTONS', options.buttons);
            }
        }
    },

    warning: function (text, title = 'Предупреждение') {
        store.commit('NOTIFICATION_MESSAGE_TEXT', text);
        store.commit('NOTIFICATION_MESSAGE_TITLE', title);
        store.commit('NOTIFICATION_MESSAGE_TYPE', 'warning');
        store.commit('NOTIFICATION_SHOW');
    },

    message: function (text, title = 'Информация') {
        store.commit('NOTIFICATION_MESSAGE_TEXT', text);
        store.commit('NOTIFICATION_MESSAGE_TITLE', title);
        store.commit('NOTIFICATION_MESSAGE_TYPE', 'message');
        store.commit('NOTIFICATION_SHOW');
    },


}
