export const icon = {
    card: 'pay-method__card-pymt-opt-50',
    qiwi: 'pay-method__qiwi-pymt-opt-50',
    sberbank: 'pay-method__sber-pymt-opt-50',
    yandex: 'pay-method__yamoney-pymt-opt-50',
    ruru: 'pay-method__mobile-pymt-opt-50',
    russian_post: 'pay-method__pochta-pymt-opt-50',
    cyberplat: 'pay-method-cyberplat',
    elecsnet: 'pay-method__eleksnet-pymt-opt-50',
    gold_crown: 'pay-method__zkorona-pymt-opt-50',
    mkb: 'pay-method__icon-mkb-pymt-opt-50',
    bank_transfer:  'pay-method__transfer-pymt-opt-50',
    contact: 'pay-method__contact-pymt-opt-50',
    svyaznoy: 'pay-method__svyaznoy-pymt-opt-50'
};
export const title = {
    card: 'Банковская карта',
    qiwi: 'QIWI Кошелек',
    sberbank: 'Сбербанк Онлайн',
    yandex: 'Яндекс.деньги',
    ruru: 'Мобильный телефон',
    mkb: 'Московский кредитный банк',
    russian_post: 'Почта России',
    elecsnet: 'Элекснет',
    bank_transfer: 'Банковский перевод',
    gold_crown: 'Золотая Корона',
    contact: 'Контакт',
    svyaznoy: 'Связной',
};

export const  methods = {
    card: 'Картой',
    yandex: 'Яндекс.Деньгами',
    qiwi: ' из QIWI Кошелька',
    bank_transfer: ' банковским переводом',
    ruru: ' со счета мобильного'
}


