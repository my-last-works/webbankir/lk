export default {
  computed: {
    insuranceIsAllowed () {
      return this.$store.getters.insuranceIsAllowed
    },
    insuranceParameters () {
      switch (true) {
        case this.sum < 10001:
          return {
            sum: 10000,
            price: 200
          }
        case this.sum < 15001:
          return {
            sum: 15000,
            price: 250
          }
        default:
          return {
            sum: 20000,
            price: 300
          }
      }
    }
  },
  methods: {
    async checkForInsurance () {
      if (!await this.$refs.snils.$validator.validateAll()) {
        this.$toastr.error('Заполните поле СНИЛС или ИНН', 'Ошибка')
        this.$refs.controlQuestion.$validator.validateAll()
        this.$helpers.focus()
        return
      }
      if (!await this.$refs.controlQuestion.$validator.validateAll()) {
        this.$toastr.error('Заполните поле контрольный ответ', 'Ошибка')
        this.$helpers.focus()
        return
      }


      if (this.$store.getters.getConfig.additionalServiceEnabled) {
        if (!this.isInsured && this.$store.getters.insuranceIsAllowed) { // Если страховка не разрешена то просто создаем займ
          this.$modal.show('modal-insurance', {
            price: this.insuranceParameters.price,
            sum: this.insuranceParameters.sum
          })
          if (!this.$refs.ModalInsurance._events.select) {
            this.$refs.ModalInsurance.$on('select', (answer) => {
              if (answer === 'yes') {
                this.isInsured = true
              }
              this.$modal.hide('modal-insurance')
              window.wbEvent.send('CLIENT_SIGN_CONTRACT')
              this.createLoan()
            })
          }
        } else {
          window.wbEvent.send('CLIENT_SIGN_CONTRACT')
          this.createLoan()
        }
      } else {
        window.wbEvent.send('CLIENT_SIGN_CONTRACT')
        this.createLoan()
      }
    }
  }
}
