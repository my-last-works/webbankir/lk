import env from '@/utils/methods/env';

export default {
    POS_BASE_URL: env().env === 'dev' ?  'https://demo.webbankir.partners/api-lk-client/' : 'https://webbankir.partners/api-lk-client/',
    GA: 'UA-45944839-1',
    YM: '17582872',
}
