export default function () {
	/*Если уже существуют значения записанные в хранилище пользователя - подставить их в текущее состояние*/
	Object.keys(this.formWithUserDataFields).forEach(fieldKey => {
		const setUserValue = this.$user[fieldKey];
		if(!setUserValue) {
			return;
		}
		this.formWithUserDataFields[fieldKey] = setUserValue;
	});
}
