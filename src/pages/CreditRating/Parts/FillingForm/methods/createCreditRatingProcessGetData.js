export default async function() {
	return await this.$backend.api.services.cr.create(this.storeCreditRatingState.dataForRequestBuildCreditRatingGuid)
		.then(({data}) => {
			if(!data) {
				return;
			}
			this.$toastr.success('Сбор информации для вашего кредитного рейтинга успешно начат');
			return data;
		})
		.catch(() => {
			this.isRequestCreditRatingDataError = true;
			this.$toastr.error('Не удалось начать сбор информации для составления кредитного рейтинга. Попробуйте повторить попытку.');
		});
};
