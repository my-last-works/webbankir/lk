export default async function(postData) {
	return await this.$backend.api.payment.card.pay(postData)
		.then(({data}) => {
			const requestData = data.data;
			if(!requestData) {
				return;
			}
			// this.$toastr.success('Сбор информации для вашего кредитного рейтинга успешно начат');
			return requestData;
		})
		.catch(() => {
			this.isRequestCreditRatingDataError = true;
			this.$toastr.error('Не удалось начать сбор информации для составления кредитного рейтинга. Попробуйте повторить попытку.');
		});
};
