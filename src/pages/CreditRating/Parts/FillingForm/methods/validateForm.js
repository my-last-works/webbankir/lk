export default async function () {
	/*установка состояния ошибок по умолчанию*/
	this.isServerValidationError = false;
	this.isClientValidationError = false;
	/*валидация на клиенте*/
	const validate = await this.$validator.validateAll();

	if(validate) {
		const {formWithUserDataFields, $user, userLocationData} = this;
		const userPassportSplit = formWithUserDataFields.passport.split(' ');
		/*записать актуальные данные для получения кредитного рейтинга в store*/
		this.$store.commit('setdataForRequestBuildCreditRatingGuid', {
			first_name: formWithUserDataFields.firstName,
			middle_name: formWithUserDataFields.middleName,
			last_name: formWithUserDataFields.lastName,
			semej: 0,

			male: formWithUserDataFields.gender === 'male' ? 1 : 2,
			p_serie: userPassportSplit[0],
			p_number: userPassportSplit[1],
			p_date: formWithUserDataFields.passportDateOfIssue,
			p_code: formWithUserDataFields.passportDivisionCode,
			birth_date: formWithUserDataFields.bDay,
			obr: formWithUserDataFields.educationType.id,
			work_vid: formWithUserDataFields.workType.id,
			profit_month: formWithUserDataFields.workSalary,

			mobile_phone: $user.mobilePhone.replace(/[^\d]/g, ''),
			email: $user.email,
			birth_city: $user.bPlace,

			a_reg_subject: userLocationData.settlement || '',
			a_reg_city: userLocationData.city || '',
			a_reg_street: userLocationData.street || '',
			a_reg_index: userLocationData.postalCode || '',
			a_reg_house: userLocationData.house || '',
			a_reg_corpus: userLocationData.housing || '',
			a_reg_stroenie: userLocationData.building || '',
			a_reg_flatnumber: userLocationData.flat || '',
		});

		// /*валидация на сервере*/
		// await this.$backend.postData('creditrating/validate', this.storeCreditRatingState.dataForRequestBuildCreditRatingGuid)
		// 	.then(() => {
		// 		/*перейти к этапу оплаты*/
		// 		this.$emit('nextStep', 'Payment');
		// 	})
		// 	.catch(error => {
		// 		const errorStatus = error.response.status;
		// 		/*если ошибка не критичная, связанная с не прохождением валидации данных*/
		// 		if(errorStatus === 400) {
		// 			this.isServerValidationError = true;
		// 			this.$error(error, creditRatingErrors, 'Заполненные данные не прошли валидацию. Пожалуйста, проверьте правильность заполнения.');
		// 			this.$nextTick(() => {
		// 				this.$helpers.scroll(this.$refs.serverValidationError);
		// 			});
		// 			return;
		// 		}
		// 		this.$error(error, creditRatingErrors, 'Сервер вернул ошибку на ваш запрос. Попробуйте повторить позже.');
		// 	});

	} else {
		this.$helpers.focus();
		this.isClientValidationError = true;
		this.$toastr.error('Заполните все поля', 'Ошибка');
	}
	return validate;
}
