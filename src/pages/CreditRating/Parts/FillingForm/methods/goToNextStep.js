const process = async function () {
	const isValidForm = await this.validateForm();
	/*если форма не прошла валидацию*/
	if(!isValidForm) {
		/*переместить экран к невалидным полям*/
		this.$nextTick(() => {
			this.$helpers.focus();
		});
		return;
	}
	const creditRatingProcessGetData = await this.createCreditRatingProcessGetData();
	/*если не были получены данные означающие начало процесса подготовки кредитного рейтинга*/
	if(!creditRatingProcessGetData?.data) {
		return;
	}
	this.$store.commit('setLatestResponseGuid', creditRatingProcessGetData.data.guid);
	this.$store.commit('setLatestResponsePrice', creditRatingProcessGetData.data.price);
	this.$store.commit('setServiceGuid', [creditRatingProcessGetData.data.guid]);
	this.$store.commit('SET_PAYMENT', {
		type: 'credit_rating',
		amount: creditRatingProcessGetData.data.price
	});
	this.$store.commit('setServicePrice', creditRatingProcessGetData.data.price);
	/*перейти к этапу оплаты*/
	this.$emit('nextStep', 'Payment');
};
export default async function() {
	this.AJAX_LOADING({isShow: true, waitingTitle: 'Подождите...', caption: 'Загрузка формы оплаты', name: 'rootFixed'});
	await process.call(this);
	this.AJAX_LOADING(false);
};
