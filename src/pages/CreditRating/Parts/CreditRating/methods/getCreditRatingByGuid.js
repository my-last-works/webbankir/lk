export default function(guid) {
	return this.$backend.api.services.cr.guid(guid)
		.then(({data}) => {
			return data.data;
		})
		.catch(error => {
			this.$toastr.error('Ошибка при запросе кредитного рейтинга');
			this.isRequestCreditRatingDataError = true;
		});
};
