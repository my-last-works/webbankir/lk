export default async function() {
	const storeCreditRatingState = this.storeCreditRatingState;
	/*результат попытки получения хеша*/
	let userCreditRatingGuid = storeCreditRatingState.latestResponseGuid;
	/*если не был получен хеш обработки запроса кредитного рейтинга*/

	if(!userCreditRatingGuid) {
		return;
	}

	let userCreditRatingData = null;
	storeCreditRatingState.firstRequestCreditRatingTimestamp = + new Date;
	/*выполнять шагами с паузами пока не будет превышен лимит ожидания*/
	do /*await */{
		userCreditRatingData = await this.getCreditRatingByGuid(userCreditRatingGuid);
		/*если не были получены данные кредитного рейтинга, или статус из полученных данных означает неготовность скредитного рейтинга*/
		if(!userCreditRatingData || userCreditRatingData.status !== 2) {
			/*выдерживание указанной паузы перед повторным запросом*/
			await new Promise(async(resolve) => {
				setTimeout(() => {
					resolve();
				}, storeCreditRatingState.resendRequestToGetLatestCreditRatingIntervalMs);
			});
		} else {
			/*обновление данных кредитного рейтинга*/
			Object.assign(this.ratingData, userCreditRatingData);
			/*прекратить повторную отправку запросов*/
			break;
		}
	} while (storeCreditRatingState.firstRequestCreditRatingTimestamp >  + new Date - storeCreditRatingState.getCreditRatingByGuidTimeoutMs);

	/*если получены подготовленные данные кредитного рейтинга*/
	if(this.isCreditRatingReady) {
		return;
	}

	/*установка состояния превышенного времени ожидания статуса готовности от сервера*/
	this.isCreditRatingLoadOvertime = true;
};
