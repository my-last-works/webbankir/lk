export default function(propName) {
	const ratingDataPropValue = this.ratingData[propName];
	let decorateMod = '';
	Object.entries(this.valuesDecorateModsRanges[propName]).forEach(propEntry => {
		const propKey = propEntry[0];
		const propValue = propEntry[1];
		if(+propKey || +propKey === 0) {
			/*для диапозонов чисел*/
			if(propKey <= ratingDataPropValue) {
				decorateMod = propValue;
			}
		} else {
			/*для конкретных значений*/
			if(propKey === ratingDataPropValue) {
				decorateMod = propValue;
			}
		}
	});
	return decorateMod;
};
