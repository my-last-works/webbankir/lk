import {mapGetters} from 'vuex';

/**
 * @var iScheduleMixin
 */
export default {
	computed: {
		...mapGetters(['$POS']),
		currentPay () {
			return this.$POS.schedule.find((s) => !s.isPaid && !s.isOverdue)
		},
		
		currentPayIndex () {
			return this.$POS.schedule.findIndex((s) => !s.isPaid && !s.isOverdue)
		},
		
		haveOverdue () {
			return this.$POS.schedule.find((s) => s.isOverdue)
		},
		
		overdueAmount () {
			const {percent, penalties, body} = this.debtSum;
			
			return  percent + penalties + body;
		},
		nextPaymentDate () {
			return new Date()
		},
		
		nextPaymentSum () {
			return 1000;
		},
		
		debtSum () {
			
			let percent = 0;
			let penalties = 0;
			
			this.$POS.schedule.filter(schedule => schedule.isOverdue).forEach((schedule) => {
				schedule.sales.forEach((/**iPosSale**/sale) => {
					percent += sale.percents;
					penalties += sale.penalties;
				})
			});
			
			const body = this.$POS.schedule.filter(schedule => schedule.isOverdue).reduce((a, /**iPosSchedule**/schedule) => {
				return a + schedule.sales.reduce((a, /**iPosSale**/sale) => {
					return a + sale.amount - sale.paid_amount
				}, 0)
			}, 0);
			
			return {
				percent,
				penalties,
				body
			};
		}
	},
}
