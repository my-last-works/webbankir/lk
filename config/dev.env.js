'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  ROUTER_BASE: '"/lk/"',
  API_URL: '"http://localhost:5354/"',
  watchOptions: {
    poll: 1000
  }
})
